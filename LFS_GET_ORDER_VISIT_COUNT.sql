USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_GET_ORDER_VISIT_COUNT]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_GET_ORDER_VISIT_COUNT]
GO

/*  pass it a, order numner and it will the visit count for that particular order.  Based on sales, not scanned attendance.   */

CREATE FUNCTION [dbo].[LFS_GET_ORDER_VISIT_COUNT] (@order_no int) Returns int
AS BEGIN

    /*  Function Parameters  */

        DECLARE @return_count INT = 0
        DECLARE @mos_buyout_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'Buyouts')
        DECLARE @visit_count_raw_data TABLE ([attend_type] varchar(30), [order_no] int, [mode_of_sale] INT, [perf_no] int, [zone_no] int, 
                                             [prod_name] varchar(30), [sale_total] int, [scan_admission_total] int)

    /*  Check Parameters  */

        IF ISNULL(@order_no,0) = 0 GOTO FINISHED
    
    /*  Retrieve the Visit Count data for ticketed events from the database  */
            
        INSERT INTO @visit_count_raw_data
        SELECT 'ticketed', tik.[order_no], ord.[MOS], tik.[perf_no], tik.[zone_no], tik.[production_name],  sum(tik.[sale_total]), sum(tik.[scan_admission_total]) 
        FROM [dbo].[LT_HISTORY_TICKET] AS tik (NOLOCK)
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = tik.[order_no]
        WHERE tik.[order_no] = @order_no 
          AND (tik.[title_name] in (SELECT [title_name] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [visit_count_title] = 'Y')
               OR tik.[production_name] IN (SELECT [production_name] FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] WHERE [visit_count_production] = 'Y'))
        GROUP BY tik.[order_no], ord.[mos], tik.[perf_no], tik.[zone_no], tik.[production_name]

        DELETE FROM @visit_count_raw_data WHERE [prod_name] like '%Buyout%' OR [mode_of_sale] = @mos_buyout_no

       
        DELETE FROM @visit_count_raw_data WHERE [prod_name] = 'Exhibit Halls Special'

        
        SELECT @return_count =  max([sale_total]) FROM @visit_count_raw_data
    
    FINISHED:

    RETURN IsNull(@return_count, 0)

END
GO

GRANT EXECUTE ON [dbo].[LFS_GET_ORDER_VISIT_COUNT] TO [ImpUsers] AS [dbo]
GO





