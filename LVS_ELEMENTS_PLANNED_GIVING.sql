USE [Impresario]
GO

IF EXISTS(SELECT 1 FROM sys.views WHERE name = 'LVS_ELEMENTS_PLANNED_GIVING' AND type = 'v')
	DROP VIEW LVS_ELEMENTS_PLANNED_GIVING
go

/****** Object:  View [dbo].[LVS_ELEMENTS_PLANNED_GIVING]    Script Date: 5/13/2021 7:07:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LVS_ELEMENTS_PLANNED_GIVING]
AS

/************************************************************************
CREATED CMC	2016-09-09 OSB Element View
*************************************************************************/

SELECT
	PG.PG_no, 
	PG.customer_no, 
	PG.first_dt, 
	PG.source_no, 
	PS.description AS source_desc,
	PG.PG_code, 
	PC.description AS PG_code_desc,
	PG.gift_type, 
	GT.description AS gift_type_desc,
	PG.on_file, 
	PO.description AS on_file_desc,
	PG.last_contact_dt, 
	PG.status, 
	PGS.description AS status_desc,
	PG.irrev_ind, 
	PG.restr_ind, 
	PG.fund_no, 
--	F.description AS fund_desc,
	PGP.description AS fund_desc,
	PG.notify_dt, 
	PG.confirm_dt, 
	PG.disc_dt, 
	PG.encore_dt, 
	PG.inst_dt, 
	PG.expt_rec_dt, 
	PG.expt_amt, 
	PG.funding,
	PF.description AS funding_desc, 
	PG.low_amt, 
	PG.high_amt, 
	PG.nom_val, 
	PG.actl_val, 
	PG.notes, 
	PG.val_dt, 
	PG.disc_rate, 
	PG.create_loc, 
	PG.created_by, 
	PG.create_dt, 
	PG.last_updated_by, 
	PG.last_update_dt, 
	PG.worker_customer_no,
	CDN.display_name AS worker_display_name,
	CDN.sort_name AS worker_sort_name
FROM dbo.T_PLANNED_GIVING PG
	INNER JOIN dbo.VRS_GIFT_TYPE GT ON PG.gift_type = GT.id
	LEFT JOIN dbo.TR_PG_CODE PC ON PG.PG_code = PC.id
	LEFT JOIN dbo.TR_PG_FUNDING PF ON PG.funding = PF.id
	LEFT JOIN dbo.TR_PG_ONFILE PO ON PG.on_file = PO.id
--	LEFT JOIN dbo.T_FUND F ON PG.fund_no = F.fund_no
	LEFT JOIN DBO.TR_PG_PURPOSE PGP on PG.fund_no = PGP.id
	LEFT JOIN dbo.TR_PG_SOURCE PS ON PG.source_no = PS.id
	LEFT JOIN dbo.TR_PG_STATUS PGS ON PG.status = PGS.id
	LEFT JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() CDN ON PG.worker_customer_no = CDN.customer_no

GO

IF (EXISTS (SELECT id FROM [dbo].TR_QUERY_ELEMENT_PARAMETER
				WHERE group_id in (SELECT id FROM [dbo].TR_QUERY_ELEMENT_GROUP WHERE data_from = 'LVS_ELEMENTS_PLANNED_GIVING')))
	DELETE FROM [dbo].TR_QUERY_ELEMENT_PARAMETER 
		WHERE group_id in (SELECT id FROM [dbo].TR_QUERY_ELEMENT_GROUP WHERE data_from = 'LVS_ELEMENTS_PLANNED_GIVING')
go

IF (EXISTS (SELECT id FROM [dbo].TR_QUERY_ELEMENT
				WHERE group_id in (SELECT id FROM [dbo].TR_QUERY_ELEMENT_GROUP WHERE data_from = 'LVS_ELEMENTS_PLANNED_GIVING')))
	DELETE FROM [dbo].TR_QUERY_ELEMENT
		WHERE group_id in (SELECT id FROM [dbo].TR_QUERY_ELEMENT_GROUP WHERE data_from = 'LVS_ELEMENTS_PLANNED_GIVING')
go

IF (EXISTS (SELECT id FROM [dbo].TR_QUERY_ELEMENT_GROUP WHERE data_from = 'LVS_ELEMENTS_PLANNED_GIVING'))
	DELETE FROM [dbo].TR_QUERY_ELEMENT_GROUP WHERE data_from = 'LVS_ELEMENTS_PLANNED_GIVING'
go

INSERT INTO [dbo].TR_QUERY_ELEMENT_GROUP (description, data_from, data_where, virtual_ind, category)
	SELECT 'Planned Giving (custom)', 'LVS_ELEMENTS_PLANNED_GIVING', data_where, virtual_ind, category
		FROM [dbo].TR_QUERY_ELEMENT_GROUP
		WHERE data_from = 'VS_ELEMENTS_PLANNED_GIVING'
go

declare @id int
SELECT @id = -500

WHILE (exists (SELECT a.id 
				FROM [dbo].TR_QUERY_ELEMENT a
					JOIN [dbo].TR_QUERY_ELEMENT_GROUP b on a.group_id = b.id
				WHERE b.data_from = 'VS_ELEMENTS_PLANNED_GIVING'
					AND a.id > @id))
BEGIN
	SELECT TOP 1 @id = a.id 
		FROM [dbo].TR_QUERY_ELEMENT a
			JOIN [dbo].TR_QUERY_ELEMENT_GROUP b on a.group_id = b.id
		WHERE b.data_from = 'VS_ELEMENTS_PLANNED_GIVING'
			AND a.id > @id

	INSERT INTO [dbo].TR_QUERY_ELEMENT 
		(id, description, category, data_select, control_group, single_row, primary_group_default, inactive
			,group_id, deprecated_where, deprecated_from, keyword_no)
		SELECT (SELECT MAX(id) + 1 FROM [dbo].TR_QUERY_ELEMENT) as id
				,a.description
				,a.category
				,a.data_select
				,a.control_group
				,a.single_row
				,a.primary_group_default
				,a.inactive
				,(SELECT id FROM [dbo].TR_QUERY_ELEMENT_GROUP WHERE data_from = 'LVS_ELEMENTS_PLANNED_GIVING') as group_id
				,a.deprecated_where
				,a.deprecated_from
				,a.keyword_no
			FROM [dbo].TR_QUERY_ELEMENT a
			WHERE a.id = @id
END
go

declare @id2 int
SELECT @id2 = -500

WHILE (exists (SELECT a.id 
				FROM [dbo].TR_QUERY_ELEMENT_PARAMETER a
					JOIN [dbo].TR_QUERY_ELEMENT_GROUP b on a.group_id = b.id
				WHERE b.data_from = 'VS_ELEMENTS_PLANNED_GIVING'
					AND a.id > @id2))
BEGIN
	SELECT TOP 1 @id2 = a.id
		FROM [dbo].TR_QUERY_ELEMENT_PARAMETER a
			JOIN [dbo].TR_QUERY_ELEMENT_GROUP b on a.group_id = b.id
		WHERE b.data_from = 'VS_ELEMENTS_PLANNED_GIVING'
			AND a.id > @id2

	INSERT INTO [dbo].TR_QUERY_ELEMENT_PARAMETER
		(id, description, data_type, end_of_day, multi_select, ref_tbl, ref_id, ref_desc, ref_where, ref_sort
			,filter_element, group_id)
		SELECT (SELECT MAX(id) + 1 FROM [dbo].TR_QUERY_ELEMENT_PARAMETER) as id
				,a.description
				,a.data_type
				,a.end_of_day
				,a.multi_select
				,a.ref_tbl
				,a.ref_id
				,a.ref_desc
				,a.ref_where
				,a.ref_sort
				,a.filter_element
				,(SELECT id FROM [dbo].TR_QUERY_ELEMENT_GROUP WHERE data_from = 'LVS_ELEMENTS_PLANNED_GIVING') as group_id
			FROM[dbo].TR_QUERY_ELEMENT_PARAMETER a
			WHERE a.id = @id2
END
go




