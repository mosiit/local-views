USE [impresario]
GO

/****** Object:  View [dbo].[LVS_EMP_PROSPECT_MANAGER]    Script Date: 10/24/2017 1:37:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LVS_EMP_PROSPECT_MANAGER]
AS

SELECT  
	a.individual_customer_no,
	SUBSTRING(key_value, 1,3) AS pm_initial,
	SUBSTRING(key_value, CHARINDEX(' ', key_value)+1, LEN(key_value)) AS pm_name,
	c.customer_no AS pm_id,
	d2.description AS Dept
FROM dbo.TX_CUST_KEYWORD ky (NOLOCK) 
INNER JOIN dbo.T_CUSTOMER c (NOLOCK) 
	ON ky.key_value = c.lname + ' ' + c.fname
INNER JOIN T_AFFILIATION AS a (NOLOCK) 
	ON ky.customer_no = a.group_customer_no
	AND a.affiliation_type_id = 10007 -- Employee_Main
LEFT JOIN LT_ATTRIBUTE_DETAIL AS ad (NOLOCK) 
	ON c.customer_no = ad.customer_no
	AND ad.type_no = 15  
LEFT OUTER JOIN LTR_ATTDET_DESCR2 AS d2 (NOLOCK) 
	ON ad.descr2_no =  d2.id
WHERE ky.keyword_no = 555

GO


