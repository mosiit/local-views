USE [impresario];
GO

/****** Object:  View [dbo].[LVXS_CUST_MEMBERSHIP]    Script Date: 4/30/2021 10:48:17 AM ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO


ALTER VIEW [dbo].[LVXS_CUST_MEMBERSHIP]
AS
SELECT a.*,
       b.edit_ind,
       org_rank = CASE
                      WHEN b.memb_org_no IN (5, 24) THEN 1
                      WHEN b.memb_org_no = '26' THEN 2
                      WHEN b.memb_org_no = '4' THEN 3
                      WHEN b.memb_org_no = '10' THEN 4
                  END
FROM [dbo].TX_CUST_MEMBERSHIP a
JOIN [dbo].VS_MEMB_ORG b ON a.memb_org_no = b.memb_org_no;




GO


