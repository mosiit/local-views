USE [impresario]
GO

/****** Object:  View [dbo].[LVX_BOARD_AFFIL]    Script Date: 9/26/2019 10:37:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LVX_BOARD_AFFIL]
AS
-- ====================================================================================================
-- Modified for SCTASK0000333 - [TESS] The Board of Overseers has been renamed Board of Museum Advisors
-- Modified for SCTASK0002320 - Former Advisor status codes not showing up in Constituent Header
--
--	LF_BOARD_STATUS_STRING - for the flex header Board Status: 
--		1. Add affiliation types 10239 (Former Museum Advisor) and 10240 (Former Museum Advisor Emeriti) 
--		2. Update abbreviations: AO becomes AMA; OE becomes MAE
--		3. Create new abbreviations for new types: FMA and FMAE
-- ====================================================================================================

SELECT	a.customer_no,
		b.relationship_type_id,
		b.end_dt,
		CASE WHEN isnull(end_dt, '2099-12-31') >= getdate() AND relationship_type_id = 10086 THEN 'AT' 
			-- 20190926, H. Sheridan - change affiliation code
			--WHEN isnull(end_dt, '2099-12-31') >= getdate() AND relationship_type_id = 10087 THEN 'OE' 
			WHEN isnull(end_dt, '2099-12-31') >= getdate() AND relationship_type_id = 10087 THEN 'MAE'
			--WHEN isnull(end_dt, '2099-12-31') >= getdate() AND relationship_type_id = 10088 THEN 'AO' 
			WHEN isnull(end_dt, '2099-12-31') >= getdate() AND relationship_type_id = 10088 THEN 'AMA' 
			WHEN isnull(end_dt, '2099-12-31') >= getdate() AND relationship_type_id = 10089 THEN 'LT' 
			WHEN isnull(end_dt, '2099-12-31') >= getdate() AND relationship_type_id = 10090 THEN 'TE' 
			
            -- 20190926, H. Sheridan - Added two new affiliations
			--WHEN isnull(end_dt, '2099-12-31') >= getdate() AND relationship_type_id = 10239 THEN 'FMA'
			--WHEN isnull(end_dt, '2099-12-31') >= getdate() AND relationship_type_id = 10240 THEN 'FMAE'
            
            -- 20210618 - Above two lines are using logic for current instead of former.  
            --Two lines below replace them, using logic for former advisor.
            WHEN (end_dt < getdate() AND relationship_type_id = 10239) THEN 'FMA'
			WHEN (end_dt < getdate() AND relationship_type_id = 10240) THEN 'FMAE'
			WHEN (end_dt < getdate() AND relationship_type_id = 10091) THEN 'FT' 
			WHEN (end_dt < getdate() AND relationship_type_id = 10092) THEN 'FO' 
			WHEN (end_dt < getdate() AND relationship_type_id = 10094) THEN 'FOE' 
			WHEN (end_dt < getdate() AND relationship_type_id = 10095) THEN 'FTE' 
			WHEN (end_dt < getdate() AND relationship_type_id = 10093) THEN 'FLT' 
		END AS board_affil
FROM	dbo.T_CUSTOMER AS a 
INNER JOIN	dbo.VS_RELATIONSHIP AS b ON a.customer_no = b.customer_no
WHERE	(b.relationship_category_id = 17)
GO


--SELECT [relationship_type_id], [relationship_type_description], [end_dt]  FROM [dbo].[VS_RELATIONSHIP] WHERE [customer_no] = 2876676 ORDER BY [end_dt] DESC

SELECT * FROM [dbo].[LVX_BOARD_AFFIL] WHERE [customer_no] IN (2673233,2876676) ORDER BY [customer_no], [end_dt] DESC

SELECT * FROM [dbo].[LVX_BOARD_AFFIL] WHERE [board_affil] IN ('FMA','FMAE') AND [customer_no] <> 5976

--SELECT * FROM [dbo].[VS_RELATIONSHIP] WHERE [customer_no] = 2876676 AND [end_dt] = '2021-06-14 00:00:00.000'
