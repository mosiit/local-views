USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_A1_AND_INDIVIDUAL]
AS

--CREATE VIEW WITH 1) A1 MEMBER OF HOUSEHOLD AND JUST 2) INDIVIDUAL CONSTITUENTS 

SELECT customer_no AS search_customer_no, expanded_customer_no AS join_customer_no, 'A1' [type]
FROM V_CUSTOMER_WITH_PRIMARY_AFFILIATES
WHERE cust_type = 7 -- Household
	AND name_ind = -1 -- Primary Adult Member

UNION ALL 

SELECT c.customer_no AS search_customer_no, c.customer_no AS join_customer_no, 'Individual' [type]
FROM V_CUSTOMER_WITH_PRIMARY_AFFILIATES c
LEFT JOIN
(	-- all primary members of a household 
	SELECT expanded_customer_no
	FROM V_CUSTOMER_WITH_PRIMARY_AFFILIATES
	WHERE cust_type = 7 -- Household
) X
ON c.customer_no = X.expanded_customer_no
WHERE c.cust_type = 1 -- Individual
	AND c.name_ind = 0 -- Self 
	AND X.expanded_customer_no IS NULL 
-- 844,516
GO

GRANT SELECT ON  [dbo].[LV_A1_AND_INDIVIDUAL] to impusers
GO