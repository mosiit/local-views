


        ALTER VIEW [dbo].[LV_ACTIVE_CORPORATE_MEMBERS] AS
        WITH [CTE_ACTIVE_MEMBERSHIPS] ([row_num], [customer_no], [cust_memb_no], [memb_level], [memb_level_name], [memb_init_dt], [memb_expr_dt])
                                       AS (SELECT ROW_NUMBER() OVER(PARTITION BY mem.[customer_no] ORDER BY mem.[expr_dt] DESC) AS [row_num],
                                                  mem.[customer_no],   
                                                  mem.[cust_memb_no],
                                                  mem.[memb_level],
                                                  lev.[description] AS [memb_level_name],
                                                  mem.[init_dt] AS [memb_init_dt],
                                                  mem.[expr_dt] AS [memb_expr_dt]
                                           FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem
                                                LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev ON lev.[memb_level] = mem.[memb_level]
                                           WHERE mem.[memb_org_no] = 7 
                                             AND mem.[current_status] IN (2, 3))
        SELECT kw1.[keyword_no],
               ISNULL(med.[appeal_no], 0) AS [source_appeal],
               ISNULL(apl.[description], '') AS [source_appeal_name],
               ISNULL(med.[source_group], 0) AS [source_group],
               ISNULL(sgp.[description],'') AS [source_group_name],
               kw1.[key_value] AS [source_no],
               ISNULL(med.[source_name], '') AS [source_name],
               kw1.[customer_no],
               nam.[display_name] AS [customer_name],
               ISNULL(adr.[street1], '') AS [street1],
               ISNULL(adr.[street2], '') AS [street2],
               ISNULL(adr.[street3], '') AS [street3],
               ISNULL(adr.[city], '') AS [city],
               ISNULL(adr.[state], '') AS [state],
               CASE WHEN LEN(ISNULL(adr.[postal_code], '')) = 9 THEN
                         LEFT(ISNULL(adr.[postal_code], ''),5) + '-' + RIGHT(ISNULL(adr.[postal_code], ''),4)
                    ELSE ISNULL(adr.[postal_code], '') END AS [postal_code],
               ISNULL(mem.[cust_memb_no], 0) AS [cust_memb_no],
               ISNULL(mem.[memb_level],'') AS [memb_level],
               ISNULL(mem.[memb_level_name], '') AS [memb_level_name],
               mem.[memb_init_dt],
               mem.[memb_expr_dt],
               kw1.[key_value] AS [Pass Source No],
               ISNULL(kw2.[key_value],'') AS [Pass Promo Code]
        FROM [dbo].[TX_CUST_KEYWORD] as kw1
             LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = kw1.[customer_no]
             LEFT OUTER JOIN [dbo].[FT_GET_PRIMARY_ADDRESS]() AS adr ON adr.[customer_no] = kw1.[customer_no]
             LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] as kw2 ON kw2.[customer_no] = kw1.[customer_no] AND kw2.[keyword_no] = 672
             LEFT OUTER JOIN [CTE_ACTIVE_MEMBERSHIPS] AS mem ON mem.[customer_no] = kw1.[customer_no]-- AND mem.[row_num] = 1
             LEFT OUTER JOIN [dbo].[TX_APPEAL_MEDIA_TYPE] AS med ON med.[source_no] = kw1.[key_value]
             LEFT OUTER JOIN [dbo].[T_APPEAL] AS apl ON apl.[appeal_no] = med.[appeal_no]
             LEFT OUTER JOIN [dbo].[TR_SOURCE_GROUP] AS sgp ON sgp.[id] = med.[source_group]
        WHERE kw1.[keyword_no] = 673
GO

    GRANT SELECT ON [dbo].[LV_ACTIVE_CORPORATE_MEMBERS] TO [ImpUsers], [tessitura_app]
GO


ALTER VIEW [dbo].[LV_ACTIVE_CORPORATE_SOURCE_GROUPS] AS
SELECT DISTINCT [source_group], 
                [source_group_name] 
FROM [dbo].[LV_ACTIVE_CORPORATE_MEMBERS] 
WHERE [source_group] > 0
GO

GRANT SELECT ON [dbo].[LV_ACTIVE_CORPORATE_SOURCE_GROUPS] TO [ImpUsers], [tessitura_app]
GO


SELECT * FROM [dbo].[LV_ACTIVE_CORPORATE_MEMBERS] ORDER BY [source_appeal], [source_name]
--SELECT * FROM [dbo].[LV_ACTIVE_CORPORATE_SOURCE_GROUPS]

--SELECT * FROM [dbo].[T_KEYWORD] WHERE [keyword_no] IN (672,673)
--SELECT * FROM [dbo].[TX_CUST_KEYWORD] WHERE [keyword_no] IN (672,673) ORDER BY [customer_no], [keyword_no]


