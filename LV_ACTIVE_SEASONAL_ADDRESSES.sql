USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_ACTIVE_SEASONAL_ADDRESSES]'))
    DROP VIEW [dbo].[LV_ACTIVE_SEASONAL_ADDRESSES]
GO

/*  Created to support the LV_ADV_BESTMAILING views  */

CREATE VIEW [dbo].[LV_ACTIVE_SEASONAL_ADDRESSES] AS
SELECT   [address_no]
       , SUBSTRING([months], MONTH(GETDATE()), 1) AS 'this_month'
       , [customer_no]
       , [address_type]
       , ISNULL([street1],'') AS 'street1'
       , ISNULL([street2],'') AS 'street2'
       , ISNULL([street3],'') AS 'street3'
       , [city]
       , [state]
       , [postal_code]
       , [country]
       , [inactive]
  FROM   [dbo].[T_ADDRESS] (NOLOCK)
 WHERE   [address_type] IN (14,15) and [inactive] = 'N'
GO

GRANT SELECT ON [dbo].[LV_ACTIVE_SEASONAL_ADDRESSES] TO ImpUsers
GO 

--SELECT * FROM [dbo].[LV_ACTIVE_SEASONAL_ADDRESSES]