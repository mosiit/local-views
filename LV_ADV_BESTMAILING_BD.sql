USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_ADV_BESTMAILING_BD]'))
    DROP VIEW [dbo].[LV_ADV_BESTMAILING_BD]
GO

CREATE VIEW [dbo].[LV_ADV_BESTMAILING_BD] AS
        WITH [business_addresses] ([address_no], [customer_no])
             AS (SELECT [address_no], 
                        [customer_no] 
                 FROM [dbo].[T_ADDRESS] 
                 WHERE [address_type] = 2 AND [inactive] = 'N')
        SELECT  adr.[address_no],
                adr.[customer_no],
                adr.[address_type],
                ISNULL(aty.[description],'') as [address_type_name],
                ISNULL(adr.[street1],'') AS [street1],
                ISNULL(adr.[street2],'') AS [street2],
                ISNULL(adr.[street3],'') AS [street3],
                adr.[city],
                adr.[state],
                CASE WHEN LEN(adr.[postal_code]) = 9 THEN 
                        LEFT(adr.[postal_code],5) + '-' + RIGHT(adr.[postal_code],4) 
                     ELSE 
                        adr.[postal_code] END AS [postal_code],
                adr.[country],
                adr.[inactive],
                ISNULL(cou.[description],'') AS [country_name],
                adr.[months],
                adr.[primary_ind]
FROM [dbo].[T_ADDRESS] AS adr (NOLOCK)
         LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS aty (NOLOCK) ON aty.[id] = adr.[address_type]
         LEFT OUTER JOIN [dbo].[TR_COUNTRY] AS cou (NOLOCK) ON cou.[id] = adr.[country]
WHERE adr.[address_type] <> 20 
  AND adr.[inactive] = 'N' AND
      adr.[last_updated_by] <> 'NCOA$DNM' AND
      (
           adr.[address_no] IN (SELECT [address_no] FROM [business_addresses])
        OR (adr.[primary_ind] = 'Y' AND adr.[customer_no] NOT IN  (SELECT [customer_no] FROM [business_addresses]))
      )      
  
GO

GRANT SELECT ON [dbo].[LV_ADV_BESTMAILING_BD] TO ImpUsers
GO

--SELECT * FROM [dbo].[LV_ADV_BESTMAILING_BD] WHERE postal_code like '02129%'
SELECT * FROM [dbo].[LV_ADV_BESTMAILING_BD] WHERE customer_no = 67226

/*

OLD CODE


WITH BusinessAddress ([address_no], [customer_no], [address_type], [street1], [street2], [street3], [city], [state], [postal_code], [country], [inactive])
AS (SELECT   [address_no]
           , [customer_no]
           , [address_type]
           , [street1]
           , ISNULL([street2],'')
           , ISNULL([street3],'')
           , [city]
           , [state]
           , [postal_code]
           , [country]
           , [inactive]
	FROM   [dbo].[T_ADDRESS] (NOLOCK)
	WHERE  [address_type] = 2 AND  [inactive] = 'N' AND  [last_updated_by] <> 'NCOA$DNM')
SELECT    [address_no]
        , [customer_no]
        , [address_type]
        , [street1]
        , [street2]
        , [street3]
        , [city]
        , [state]
        , [postal_code]
        , [country]
        , [BusinessAddress].[inactive]
FROM [BusinessAddress] 

UNION SELECT  [address_no]
            , [customer_no]
            , [address_type]
            , [street1]
            , ISNULL([street2],'') AS 'street2'
            , ISNULL([street3],'') AS 'street3'
            , [city]
            , [state]
            , [postal_code]
            , [country]
            , [inactive]
FROM [dbo].[T_ADDRESS] (NOLOCK)
WHERE  [inactive] = 'N' 
  AND  [primary_ind] = 'Y'
  AND  [address_type] <> 20
  AND  [last_updated_by] <> 'NCOA$DNM' 
  AND  [customer_no] NOT IN (SELECT [customer_no] FROM [BusinessAddress])

*/



