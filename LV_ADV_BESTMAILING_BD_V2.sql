USE [impresario]
GO

/****** Object:  View [dbo].[LV_ADV_BESTMAILING_BD_V2]    Script Date: 2/26/2018 2:32:36 PM ******/
DROP VIEW [dbo].[LV_ADV_BESTMAILING_BD_V2]
GO

/****** Object:  View [dbo].[LV_ADV_BESTMAILING_BD_V2]    Script Date: 2/26/2018 2:32:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[LV_ADV_BESTMAILING_BD_V2] AS
WITH    [business_addresses]([address_no], [customer_no])
          AS (
              SELECT    [address_no],
                        [customer_no]
              FROM      [dbo].[T_ADDRESS] (NOLOCK)
              WHERE     [address_type] = 2
                        AND [inactive] = 'N'
             )
     SELECT adr.[address_no],
            adr.[customer_no],
            adr.[address_type],
            ISNULL(aty.[description], '') AS 'address_type_name',
            ISNULL(adr.[street1], '') AS 'street1',
            ISNULL(adr.[street2], '') AS 'street2',
            ISNULL(adr.[street3], '') AS 'street3',
            adr.[city],
            adr.[state],
            adr.[postal_code],
            adr.[country],
            adr.[inactive],
            ISNULL(cou.[description], '') AS 'country_name',
            adr.[months],
            adr.[primary_ind]
     FROM   [dbo].[T_ADDRESS] AS adr (NOLOCK)
     LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS aty (NOLOCK)
     ON     aty.[id] = adr.[address_type]
     LEFT OUTER JOIN [dbo].[TR_COUNTRY] AS cou (NOLOCK)
     ON     cou.[id] = adr.[country]
     WHERE  adr.[address_type] <> 20
            AND adr.[inactive] = 'N'
            AND adr.[last_updated_by] <> 'NCOA$DNM'
            AND EXISTS ( SELECT 1
                         FROM   [business_addresses] ba
                         WHERE  ba.[address_no] = adr.[address_no] )
            OR adr.[primary_ind] = 'Y'
            AND NOT EXISTS ( SELECT 1
                             FROM   [business_addresses] ba
                             WHERE  adr.[customer_no] = ba.[customer_no] )

GO