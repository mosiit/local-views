USE [impresario]
GO

/****** Object:  View [dbo].[LV_ADV_BESTMAILING_BD]    Script Date: 3/5/2021 10:48:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
    ALTER VIEW [dbo].[LV_ADV_BESTMAILING_HD] AS
        WITH [home_addresses] ([row_no], [address_no], [customer_no])
             AS (SELECT ROW_NUMBER() OVER (PARTITION BY [customer_no] ORDER BY [primary_ind] DESC, [last_update_dt] DESC) AS [row_no],
                        [address_no], 
                        [customer_no] 
                 FROM [dbo].[T_ADDRESS] 
                 WHERE [address_type] = 3 AND [inactive] = 'N')
        SELECT  adr.[address_no],
                adr.[customer_no],
                adr.[address_type],
                ISNULL(aty.[description],'') AS [address_type_name],
                ISNULL(adr.[street1],'') AS [street1],
                ISNULL(adr.[street2],'') AS [street2],
                ISNULL(adr.[street3],'') AS [street3],
                adr.[city],
                adr.[state],
                CASE WHEN LEN(adr.[postal_code]) = 9 THEN 
                        LEFT(adr.[postal_code],5) + '-' + RIGHT(adr.[postal_code],4) 
                     ELSE 
                        adr.[postal_code] END AS [postal_code],
                adr.[country],
                adr.[inactive],
                ISNULL(cou.[description],'') AS [country_name],
                adr.[months],
                adr.[primary_ind]
        FROM [dbo].[T_ADDRESS] AS adr (NOLOCK)
             LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS aty (NOLOCK) ON aty.[id] = adr.[address_type]
             LEFT OUTER JOIN [dbo].[TR_COUNTRY] AS cou (NOLOCK) ON cou.[id] = adr.[country]
        WHERE adr.[address_type] NOT IN (1,20)
          AND adr.[inactive] = 'N' 
          AND adr.[last_updated_by] <> 'NCOA$DNM' 
          AND (adr.[address_no] IN (SELECT [address_no] FROM [home_addresses])
               OR (adr.[primary_ind] = 'Y' AND adr.[address_no] NOT IN (SELECT ISNULL([address_no], 0) FROM [home_addresses] WHERE [row_no] = 1)))
GO


SELECT * FROM [dbo].[LV_ADV_BESTMAILING_HD] WHERE [customer_no] = 67226
--SELECT * FROM [dbo].[LV_ADV_BESTMAILING_HD] WHERE [address_type] <> 3
--SELECT customer_no, COUNT(*) FROM [dbo].[LV_ADV_BESTMAILING_HD] WHERE [state] = 'MA' GROUP BY [customer_no] HAVING COUNT(*) > 1


--SELECT cus.customer_no, mai.*
--FROM t_CUSTOMER AS cus
--     INNER JOIN [dbo].[LV_ADV_BESTMAILING_HD] AS mai ON mai.[customer_no] = cus.[customer_no]
--WHERE 

--SELECT * FROM [dbo].[TR_ADDRESS_TYPE]
--SELECT * FROM [dbo].[T_ADDRESS] WHERE [address_type] = 1



                
                -- WITH CTE_HOME_ADDRESSES
                -- AS (SELECT ROW_NUMBER() OVER (PARTITION BY [customer_no] ORDER BY [primary_ind] DESC, [last_update_dt] DESC) AS [row_no],
                --            [address_no], 
                --            [customer_no] 
                --     FROM [dbo].[T_ADDRESS] 
                --     WHERE [address_type] = 3
                --       AND [inactive] = 'N')
                -- SELECT [customer_no],
                --        [address_no],
                --        [inactive],
                --        [primary_ind],
                --        [address_type],
                --        [street1],
                --        [city] + ', ' + [state] + ' ' + [postal_code],
                --        [country]
                -- FROM [dbo].[T_ADDRESS]
                -- WHERE [customer_no] IN (SELECT [customer_no] 
                --                         FROM [CTE_HOME_ADDRESSES]
                --                         WHERE [CTE_HOME_ADDRESSES].[row_no] > 1)
                --  AND [address_type] = 3
                --ORDER BY [customer_no], [primary_ind] DESC, [last_update_dt] DESC

