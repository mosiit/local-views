USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_ADV_BESTMAILING_SD]'))
    DROP VIEW [dbo].[LV_ADV_BESTMAILING_SD]
GO
/*  Assuming the data is correct (i.e. One primary address for each customer and only one 
    seasonal address for any given month per customer) this view should return one row 
    per customer, or one row at the most.  It is possible for this view to return no rows 
    for a particular customer if they have no active primary and no seasonal address. */
CREATE VIEW [dbo].[LV_ADV_BESTMAILING_SD] AS
WITH    [SeasonalAddress]([address_no], [customer_no])
          AS (
              SELECT    [address_no],
                        [customer_no]
              FROM      [dbo].[T_ADDRESS]
              WHERE     [address_type] IN (14, 15)
                        AND [inactive] = 'N'
                        AND SUBSTRING([months], MONTH(GETDATE()), 1) = 'Y'
             )
       SELECT   adr.[address_no],
                adr.[customer_no],
                adr.[address_type],
                ISNULL(aty.[description], '') AS [address_type_name],
                ISNULL(adr.[street1], '') AS [street1],
                ISNULL(adr.[street2], '') AS [street2],
                ISNULL(adr.[street3], '') AS [street3],
                adr.[city],
                adr.[state],
                CASE WHEN LEN(adr.[postal_code]) = 9 THEN 
                        LEFT(adr.[postal_code],5) + '-' + RIGHT(adr.[postal_code],4) 
                     ELSE 
                        adr.[postal_code] END AS [postal_code],
                adr.[country],
                ISNULL(cou.[description], '') AS [country_name],
                adr.[months],
                adr.[primary_ind],
                adr.[inactive]
       FROM     [dbo].[T_ADDRESS] AS adr (NOLOCK)
                LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS aty (NOLOCK) ON aty.[id] = adr.[address_type]
                LEFT OUTER JOIN [dbo].[TR_COUNTRY] AS cou (NOLOCK) ON cou.[id] = adr.[country]
       WHERE    adr.[inactive] = 'N'
                AND adr.[address_type] NOT IN (1, 20)
                AND adr.[last_updated_by] <> 'NCOA$DNM'
        AND     (
                    EXISTS	(SELECT	1 
                             FROM [SeasonalAddress] sadr1 
                             WHERE	sadr1.address_no = adr.address_no)      --Either the address exists in the seasonal address CTE
                                                                            --Or use the primary address for any customer not in 
                    OR                                                      --the seasonal address CTE.
                   
                    (
                        NOT EXISTS (SELECT 1 
                                    FROM [SeasonalAddress] sadr2 
                                    WHERE sadr2.customer_no = adr.customer_no) 
                        AND adr.primary_ind = 'Y'
                    ) 
                 )
GO

GRANT SELECT ON [dbo].[LV_ADV_BESTMAILING_SD] TO ImpUsers
GO

--SELECT * FROM LV_ADV_BESTMAILING_SD WHERE customer_no = 67226

/*

    OLD CODE 

CREATE VIEW [dbo].[LV_ADV_BESTMAILING_SD] AS
WITH    SeasonalAddress ([address_no], [customer_no], [address_type], [street1], [street2], [street3], [city], [state], [postal_code], [country], [inactive])
          AS (SELECT    [address_no],
                        [customer_no],
                        [address_type],
                        [street1],
                        ISNULL([street2],''),
                        ISNULL([street3],''),
                        [city],
                        [state],
                        [postal_code],
                        [country],
                        [inactive]
              FROM      [dbo].[T_ADDRESS] (NOLOCK)
              WHERE     [inactive] = 'N'
                        AND [last_updated_by] <> 'NCOA$DNM'
                        AND SUBSTRING([months], MONTH(GETDATE()), 1) = 'Y')
     SELECT [address_no],
            [customer_no],
            [address_type],
            [street1],
            [street2],
            [street3],
            [city],
            [state],
            [postal_code],
            [country],
            [inactive]
     FROM   [SeasonalAddress]

     UNION SELECT [address_no],
            [customer_no],
            [address_type],
            [street1],
            ISNULL([street2],'') AS 'street2',
            ISNULL([street3],'') AS 'street3',
            [city],
            [state],
            [postal_code],
            [country],
            [inactive]
     FROM   [dbo].[T_ADDRESS] (NOLOCK)
     WHERE  [primary_ind] = 'Y'
            AND [address_type] <> 20
            AND [customer_no] NOT IN (SELECT  [customer_no]
                                      FROM    SeasonalAddress
                                      WHERE   [address_type] IN (14, 15))
	
	UNION SELECT [address_no],
            [customer_no],
            [address_type],
            [street1],
            ISNULL([street2],'') AS 'street2',
            ISNULL([street3],'') AS 'street3',
            [city],
            [state],
            [postal_code],
            [country],
            [inactive]
     FROM   [dbo].[T_ADDRESS] (NOLOCK)
     WHERE  [primary_ind] = 'Y'
       AND [address_type] <> 20
       AND [customer_no] NOT IN (SELECT [customer_no]
                                 FROM SeasonalAddress
                                 WHERE [address_type] IN (14, 15)
                                   AND SUBSTRING([months], MONTH(GETDATE()), 1) = 'N')

GO


*/

/*

Old Code # 2

CREATE VIEW [dbo].[LV_ADV_BESTMAILING_SD] AS
/*  Get a CTE containing all the current month's seasonal addresses  */
WITH [SeasonalAddress] ([address_no], [customer_no])
             AS (SELECT [address_no], [customer_no] FROM [dbo].[T_ADDRESS] 
                  WHERE [address_type] IN (14,15) AND SUBSTRING([months], MONTH(GETDATE()), 1) = 'Y')
SELECT   adr.[address_no]
       , adr.[customer_no]
       , adr.[address_type]
       , ISNULL(aty.[description],'') as 'address_type_name'
       , ISNULL(adr.[street1],'') AS 'street1'
       , ISNULL(adr.[street2],'') AS 'street2'
       , ISNULL(adr.[street3],'') AS 'street3'
       , adr.[city]
       , adr.[state]
       , adr.[postal_code]
       , adr.[country]
       , ISNULL(cou.[description],'') AS 'country_name'
       , adr.[months]
       , adr.[primary_ind]
       , adr.[inactive]
  FROM   [dbo].[T_ADDRESS] AS adr (NOLOCK)
         LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS aty (NOLOCK) ON aty.[id] = adr.[address_type]
         LEFT OUTER JOIN [dbo].[TR_COUNTRY] AS cou (NOLOCK) ON cou.[id] = adr.[country]
 WHERE   adr.[inactive] = 'N'
   AND   adr.[address_type] <> 20
   AND   adr.[last_updated_by] <> 'NCOA$DNM' 
   AND   (
             (adr.[address_no] IN (SELECT [address_no] FROM [SeasonalAddress]))
          OR (adr.[primary_ind] = 'Y'  AND adr.[customer_no] NOT IN  (SELECT [customer_no] FROM [dbo].[T_ADDRESS] WHERE [address_type] IN (14,15))) 
          OR (adr.[primary_ind] = 'Y' AND adr.[customer_no] NOT IN (SELECT [customer_no] FROM [SeasonalAddress]))
         )

*/