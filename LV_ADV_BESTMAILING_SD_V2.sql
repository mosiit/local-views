USE [impresario]
GO

/****** Object:  View [dbo].[LV_ADV_BESTMAILING_SD_V2]    Script Date: 2/23/2018 11:58:26 AM ******/
DROP VIEW [dbo].[LV_ADV_BESTMAILING_SD_V2]
GO

/****** Object:  View [dbo].[LV_ADV_BESTMAILING_SD_V2]    Script Date: 2/23/2018 11:58:26 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[LV_ADV_BESTMAILING_SD_V2]
AS
WITH    [SeasonalAddress]([address_no], [customer_no])
          AS (
              SELECT    [address_no],
                        [customer_no]
              FROM      [dbo].[T_ADDRESS]
              WHERE     [address_type] IN (14, 15)
                        AND SUBSTRING([months], MONTH(GETDATE()), 1) = 'Y'
             )
       SELECT   adr.[address_no],
                adr.[customer_no],
                adr.[address_type],
                ISNULL(aty.[description], '') AS 'address_type_name',
                ISNULL(adr.[street1], '') AS 'street1',
                ISNULL(adr.[street2], '') AS 'street2',
                ISNULL(adr.[street3], '') AS 'street3',
                adr.[city],
                adr.[state],
                adr.[postal_code],
                adr.[country],
                ISNULL(cou.[description], '') AS 'country_name',
                adr.[months],
                adr.[primary_ind],
                adr.[inactive]
       FROM     [dbo].[T_ADDRESS] AS adr (NOLOCK)
       LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS aty (NOLOCK)
       ON       aty.[id] = adr.[address_type]
       LEFT OUTER JOIN [dbo].[TR_COUNTRY] AS cou (NOLOCK)
       ON       cou.[id] = adr.[country]
       WHERE    adr.[inactive] = 'N'
                AND adr.[address_type] NOT IN (1, 20)
                AND adr.[last_updated_by] <> 'NCOA$DNM'
		AND		(EXISTS	(SELECT	1
						 FROM	[SeasonalAddress] sadr1
						 WHERE	sadr1.address_no = adr.address_no)
		OR		NOT EXISTS (SELECT	1
							FROM	[dbo].[T_ADDRESS] AS adr2 (NOLOCK)
							WHERE   adr2.[address_type] IN (14, 15)
							AND		adr2.[customer_no] = adr.[customer_no]
							AND		adr.[primary_ind] = 'Y'
							AND		adr.[inactive] = 'N')
		OR		NOT EXISTS (SELECT	1
							FROM	[SeasonalAddress] sadr2
							WHERE   sadr2.[customer_no] = adr.[customer_no]
							AND		adr.[primary_ind] = 'Y'))


GO


