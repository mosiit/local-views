USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_ADV_BESTMAILING_SGD]'))
    DROP VIEW [dbo].[LV_ADV_BESTMAILING_SGD]
GO

CREATE VIEW [dbo].[LV_ADV_BESTMAILING_SGD] AS
SELECT   adr.[address_no]
       , adr.[customer_no]
       , [address_type]
       , ISNULL(aty.[description],'') as 'address_type_name'
       , ISNULL(adr.[street1],'') AS 'street1'
       , ISNULL(adr.[street2],'') AS 'street2'
       , ISNULL(adr.[street3],'') AS 'street3'
       , adr.[city]
       , adr.[state]
       , adr.[postal_code]
       , adr.[country]
       , ISNULL(cou.[description],'') AS 'country_name'
       , adr.[months]
       , adr.[primary_ind]
       , adr.[inactive]
FROM [dbo].[T_ADDRESS] AS adr (NOLOCK)
     LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS aty (NOLOCK) ON aty.[id] = adr.[address_type]
     LEFT OUTER JOIN [dbo].[TR_COUNTRY] AS cou (NOLOCK) ON cou.[id] = adr.[country]
WHERE
adr.[inactive] = 'N' AND
adr.[last_updated_by] <> 'NCOA$DNM' AND
(
(adr.[address_type] IN (14,15) AND SUBSTRING(adr.[months], MONTH(GETDATE()), 1) = 'Y') 
OR
(adr.[address_no] IN (SELECT [contact_point_id] FROM [dbo].[TX_CONTACT_POINT_PURPOSE] (NOLOCK) WHERE [contact_point_category] = -1 AND [purpose_id] = 12) AND [customer_no] NOT IN 
	(SELECT [customer_no] FROM [dbo].[T_ADDRESS] (NOLOCK) WHERE address_type IN (14,15))) 
OR
(adr.[address_no] IN (SELECT [contact_point_id] FROM [dbo].[TX_CONTACT_POINT_PURPOSE] (NOLOCK) WHERE [contact_point_category] = -1 AND [purpose_id] = 12) AND [customer_no] IN
	(SELECT [customer_no] FROM [dbo].[T_ADDRESS] (NOLOCK) WHERE [address_type] IN (14,15) AND SUBSTRING([months], MONTH(GETDATE()), 1) = 'N')) 
OR
(adr.[primary_ind] = 'Y' AND adr.[address_type] <> 20 AND adr.[customer_no] NOT IN 
	(SELECT [customer_no] FROM [dbo].[T_ADDRESS] (NOLOCK) WHERE [address_type] IN (14,15)) AND [customer_no] NOT IN 
	(SELECT [customer_no] FROM [dbo].[T_ADDRESS] (NOLOCK) WHERE address_no IN (SELECT [contact_point_id] FROM [dbo].[TX_CONTACT_POINT_PURPOSE] (NOLOCK) WHERE [contact_point_category] = -1 AND [purpose_id] = 12))) 
OR
(adr.[primary_ind] = 'Y' AND adr.[address_type] <> 20 AND adr.[customer_no] IN 
	(SELECT [customer_no] FROM [dbo].[T_ADDRESS] (NOLOCK) WHERE [address_type] IN (14,15) AND SUBSTRING([months], MONTH(GETDATE()), 1) = 'N') AND [customer_no] NOT IN 
	(SELECT [customer_no] FROM [dbo].[T_ADDRESS] (NOLOCK) WHERE [address_no] IN (SELECT [contact_point_id] FROM [dbo].[TX_CONTACT_POINT_PURPOSE] (NOLOCK) WHERE [contact_point_category] = -1 AND [purpose_id] = 12)))
)
GO

GRANT SELECT ON [dbo].[LV_ADV_BESTMAILING_SGD] TO ImpUsers
GO

/*


OLD CODE

CREATE VIEW [dbo].[LV_ADV_BESTMAILING_SGD] AS
WITH    [SeasonalAddress] ([address_no], [customer_no], [address_type], [street1], [street2], [street3], [city], [state], [postal_code], [country], [inactive], [last_updated_by])
          AS (SELECT    [address_no]
                      , [customer_no]
                      , [address_type]
                      , [street1]
                      , ISNULL([street2],'') AS 'street2'
                      , ISNULL([street3],'') AS 'street3'
                      , [city]
                      , [state]
                      , [postal_code]
                      , [country]
                      , [inactive]
                      , [last_updated_by]
              FROM  [dbo].[T_ADDRESS] (NOLOCK)
              WHERE SUBSTRING([months], MONTH(GETDATE()), 1) = 'Y' AND [inactive] = 'N' AND [last_updated_by] <> 'NCOA$DNM')
     SELECT   [address_no]
            , [customer_no]
            , [address_type]
            , [street1]
            , [street2]
            , [street3]
            , [city]
            , [state]
            , [postal_code]
            , [country]
            , [inactive]
     FROM   [SeasonalAddress]

UNION SELECT   [address_no]
             , [customer_no]
             , [address_type]
             , [street1]
             , ISNULL([street2],'') AS 'street2'
             , ISNULL([street3],'') AS 'street3'
             , [city]
             , [state]
             , [postal_code]
             , [country]
             , [inactive]
FROM [dbo].[T_ADDRESS] 
WHERE [address_no] IN (SELECT [contact_point_id] FROM [TX_CONTACT_POINT_PURPOSE] 
                       WHERE [contact_point_category] = -1 AND [purpose_id] = 12) 
                         AND [customer_no] NOT IN (SELECT [customer_no] FROM [dbo].[T_ADDRESS] WHERE [address_type] IN (14,15))

UNION SELECT   [address_no]
             , [customer_no]
             , [address_type]
             , [street1]
             , ISNULL([street2],'') AS 'street2'
             , ISNULL([street3],'') AS 'street3'
             , [city]
             , [state]
             , [postal_code]
             , [country]
             , [inactive]
FROM [dbo].[T_ADDRESS] 
WHERE [address_no] IN (SELECT [contact_point_id] FROM [dbo].[TX_CONTACT_POINT_PURPOSE] 
                       WHERE [contact_point_category] = -1 
                         AND [purpose_id] = 12) 
                         AND [customer_no] IN (SELECT [customer_no] FROM [dbo].[T_ADDRESS]
                                               WHERE address_type IN (14,15) 
                                                 AND SUBSTRING([months], MONTH(GETDATE()), 1) = 'N')

UNION SELECT   [address_no]
             , [customer_no]
             , [address_type]
             , [street1]
             , ISNULL([street2],'')
             , ISNULL([street3],'')
             , [city]
             , [state]
             , [postal_code]
             , [country]
             , [inactive]
FROM [dbo].[T_ADDRESS] 
WHERE [primary_ind] = 'Y' 
  AND [address_type] <> 20 
  AND [customer_no] NOT IN (SELECT [customer_no] FROM [dbo].[T_ADDRESS] WHERE [address_type] IN (14,15)) 
  AND [customer_no] NOT IN (SELECT [customer_no] FROM [dbo].[T_ADDRESS] 
                            WHERE [address_no] IN (SELECT [contact_point_id] FROM [dbo].[TX_CONTACT_POINT_PURPOSE]
                                                   WHERE [contact_point_category] = -1 AND [purpose_id] = 12))

UNION SELECT   [address_no]
             , [customer_no]
             , [address_type]
             , [street1]
             , ISNULL([street2],'')
             , ISNULL([street3],'')
             , [city]
             , [state]
             , [postal_code]
             , [country]
             , [inactive]
FROM [dbo].[T_ADDRESS] 
WHERE [primary_ind] = 'Y' 
  AND [address_type] <> 20 
  AND [customer_no] IN (SELECT [customer_no] FROM [dbo].[T_ADDRESS]
                        WHERE [address_type] IN (14,15) AND SUBSTRING([months], MONTH(GETDATE()), 1) = 'N') 
                          AND [customer_no] NOT IN (SELECT [customer_no] FROM [dbo].[T_ADDRESS] 
                                                    WHERE [address_no] IN (SELECT [contact_point_id] FROM [dbo].[TX_CONTACT_POINT_PURPOSE]
                                                                           WHERE [contact_point_category] = -1 AND [purpose_id] = 12))
GO
*/
