USE [impresario]
GO

/****** Object:  View [dbo].[LV_ADV_BESTMAILING_SGD_V2]    Script Date: 2/26/2018 2:09:46 PM ******/
DROP VIEW [dbo].[LV_ADV_BESTMAILING_SGD_V2]
GO

/****** Object:  View [dbo].[LV_ADV_BESTMAILING_SGD_V2]    Script Date: 2/26/2018 2:09:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[LV_ADV_BESTMAILING_SGD_V2] AS
WITH    [ContactPointPurpose]([contact_point_id])
          AS (
              SELECT    [contact_point_id]
              FROM      [dbo].[TX_CONTACT_POINT_PURPOSE] (NOLOCK)
              WHERE     [contact_point_category] = -1
                        AND [purpose_id] = 12
             ),
        [SeasonalAddress]([customer_no], [address_no], [months])
          AS (
              SELECT    [customer_no],
                        [address_no],
                        [months]
              FROM      [dbo].[T_ADDRESS] (NOLOCK)
              WHERE     address_type IN (14, 15)
             )
     SELECT adr.[address_no],
            adr.[customer_no],
            [address_type],
            ISNULL(aty.[description], '') AS 'address_type_name',
            ISNULL(adr.[street1], '') AS 'street1',
            ISNULL(adr.[street2], '') AS 'street2',
            ISNULL(adr.[street3], '') AS 'street3',
            adr.[city],
            adr.[state],
            adr.[postal_code],
            adr.[country],
            ISNULL(cou.[description], '') AS 'country_name',
            adr.[months],
            adr.[primary_ind],
            adr.[inactive]
     FROM   [dbo].[T_ADDRESS] AS adr (NOLOCK)
     LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS aty (NOLOCK)
     ON     aty.[id] = adr.[address_type]
     LEFT OUTER JOIN [dbo].[TR_COUNTRY] AS cou (NOLOCK)
     ON     cou.[id] = adr.[country]
     WHERE  adr.[inactive] = 'N'
            AND adr.[last_updated_by] <> 'NCOA$DNM'
            AND (
                 (
                  adr.[address_type] IN (14, 15)
                  AND SUBSTRING(adr.[months], MONTH(GETDATE()), 1) = 'Y'
                 )
                 OR (
                     EXISTS ( SELECT    1
                              FROM      [ContactPointPurpose] cpp
                              WHERE     adr.[address_no] = cpp.[contact_point_id] )
                     AND NOT EXISTS ( SELECT    1
                                      FROM      [SeasonalAddress] saddr
                                      WHERE     adr.[customer_no] = saddr.[customer_no] )
                    )
                 OR (
                     EXISTS ( SELECT    1
                              FROM      [ContactPointPurpose] cpp
                              WHERE     adr.[address_no] = cpp.[contact_point_id] )
                     AND EXISTS ( SELECT    1
                                  FROM      [SeasonalAddress] saddr
                                  WHERE     SUBSTRING(saddr.[months], MONTH(GETDATE()), 1) = 'N'
                                            AND adr.[customer_no] = saddr.[customer_no] )
                    )
                 OR (
                     adr.[primary_ind] = 'Y'
                     AND adr.[address_type] <> 20
                     AND NOT EXISTS ( SELECT    1
                                      FROM      [SeasonalAddress] saddr
                                      WHERE     adr.[customer_no] = saddr.[customer_no] )
                     AND NOT EXISTS ( SELECT    1
                                      FROM      [SeasonalAddress] saddr
                                      WHERE     saddr.[address_no] IN (SELECT   1
                                                                       FROM     [ContactPointPurpose] cpp
                                                                       WHERE    adr.[customer_no] = cpp.[contact_point_id]) )
                    )
                 OR (
                     adr.[primary_ind] = 'Y'
                     AND adr.[address_type] <> 20
                     AND EXISTS ( SELECT    1
                                  FROM      [SeasonalAddress] saddr
                                  WHERE     SUBSTRING([months], MONTH(GETDATE()), 1) = 'N'
                                            AND adr.[customer_no] = saddr.[customer_no] )
                     AND NOT EXISTS ( SELECT    1
                                      FROM      [dbo].[T_ADDRESS] adr2 (NOLOCK)
                                      WHERE     adr2.[address_no] IN (SELECT    [contact_point_id]
                                                                      FROM      [ContactPointPurpose] cpp)
                                                AND adr.[customer_no] = adr2.[customer_no] )
                    )
                )
GO


