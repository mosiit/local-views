USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_ADV_BEST_LAST_MEMBERSHIP]
AS

---- WO 83717 - Need SQL View for Tessitura - LT_CurrentMem_AdvView
---- SCTASK0002574 - Upated to take the new Membership Hierarchy into account

WITH [CTE_BEST_LAST_MEMBERSHIP] AS (SELECT  cm.[customer_no], 
                                            ROW_NUMBER() OVER (PARTITION BY cm.[customer_no] ORDER BY hie.[memb_org_sort], cm.[expr_dt] DESC) AS [row_num], 
                                            cm.[cust_memb_no], 
                                            cm.[memb_org_no]
                                     FROM [dbo].[TX_CUST_MEMBERSHIP] AS cm
                                          INNER JOIN T_MEMB_LEVEL AS ml ON cm.memb_level = ml.memb_level
                                          LEFT OUTER JOIN [dbo].[LV_MEMBERSHIP_HIERARCHY] AS hie ON hie.[memb_org_no] = cm.[memb_org_no]
                                     WHERE cm.[current_status] IN (2, 3)
                                       AND ml.[start_amt] = (SELECT MAX(ml2.[start_amt]) AS Exp1
                                                             FROM [dbo].[TX_CUST_MEMBERSHIP] AS cm2
                                                                  INNER JOIN [dbo].[T_MEMB_LEVEL] AS ml2 ON cm2.[memb_level] = ml2.[memb_level]
                                                             WHERE cm.[customer_no] = cm2.[customer_no]
                                                               AND cm2.[current_status] IN (2, 3)
                                                             GROUP BY cm2.[customer_no])
                                       AND cm.[expr_dt] = (SELECT MAX(cm3.[expr_dt]) AS Exp1
                                                           FROM [dbo].[TX_CUST_MEMBERSHIP] AS cm3
                                                                INNER JOIN [dbo].[T_MEMB_LEVEL] AS ml3 ON cm3.[memb_level] = ml3.[memb_level]
                                                           WHERE cm.[customer_no] = cm3.[customer_no]
                                                             AND ml3.[start_amt] = ml.[start_amt]
                                                             AND cm3.[current_status] IN (2, 3)
                                                           GROUP BY cm3.[customer_no]))
SELECT  mem.[cust_memb_no],
        mem.[parent_no],
        mem.[customer_no],
        mem.[campaign_no],
        mem.[memb_org_no],
        mem.[memb_level],
        mem.[memb_amt],
        mem.[ben_provider],
        mem.[init_dt],
        mem.[susp_dt],
        mem.[cancel_dt],
        mem.[renew_dt],
        mem.[expr_dt],
        mem.[lapse_dt],
        mem.[rein_dt],
        mem.[current_status],
        mem.[NRR_status],
        mem.[memb_trend],
        mem.[first_issue_id],
        mem.[last_issue_sent_id],
        mem.[final_issue_id],
        mem.[declined_ind],
        mem.[num_copies],
        mem.[AVC_amt],
        mem.[orig_expiry_dt],
        mem.[orig_memb_level],
        mem.[ship_method],
        mem.[inception_dt],
        mem.[mir_lock],
        mem.[create_loc],
        mem.[created_by],
        mem.[create_dt],
        mem.[last_updated_by],
        mem.[last_update_dt],
        mem.[last_issue_status],
        mem.[cur_record],
        mem.[ben_holder_ind],
        mem.[recog_amt],
        mem.[category_trend],
        mem.[notes]
FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem
     INNER JOIN [CTE_BEST_LAST_MEMBERSHIP] AS cte ON cte.[cust_memb_no] = mem.[cust_memb_no] AND cte.[row_num] = 1
GO

GRANT SELECT ON  [dbo].[LV_ADV_BEST_LAST_MEMBERSHIP] to impusers
GO

SELECT COUNT(*) FROM [dbo].[LV_ADV_BEST_LAST_MEMBERSHIP]
SELECT * FROM [dbo].[LV_ADV_BEST_LAST_MEMBERSHIP]
