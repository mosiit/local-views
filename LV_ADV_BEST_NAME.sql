USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_ADV_BEST_NAME]'))
    DROP VIEW [dbo].[LV_ADV_BEST_NAME]
GO

CREATE VIEW [dbo].[LV_ADV_BEST_NAME] AS
SELECT cus.customer_no
     , CASE WHEN cus.cust_type = 7 THEN LTRIM(ISNULL(cus.fname,'') + ' ' + ISNULL(cus.lname,''))
            WHEN cus.cust_type <> 7 and sal.[esal1_desc] IS NULL THEN LTRIM(ISNULL(cus.fname,'') + ' ' + ISNULL(cus.lname,''))
            ELSE sal.esal1_desc END AS 'best_name'
FROM T_CUSTOMER AS cus
     LEFT OUTER JOIN dbo.TX_CUST_SAL AS sal ON sal.customer_no = cus.customer_no AND sal.default_ind = 'Y'
WHERE [cus].[inactive] = 1
GO

GRANT SELECT ON [dbo].[LV_ADV_BEST_NAME] TO ImpUsers
GO

SELECT best_name FROM [dbo].[LV_ADV_BEST_NAME] WHERE customer_no = 718872



SELECT * FROM T_CUSTOMER WHERE lname = 'des'

