USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_ADV_LAST_INACTIVE_MEM]'))
    DROP VIEW [dbo].[LV_ADV_LAST_INACTIVE_MEM]
GO

CREATE VIEW [dbo].[LV_ADV_LAST_INACTIVE_MEM] AS
SELECT    cm.[cust_memb_no]
        , cm.[parent_no]
        , cm.[customer_no]
        , cm.[campaign_no]
        , cm.[memb_org_no]
        , cm.[memb_level]
        , cm.[memb_amt]
        , cm.[ben_provider]
        , cm.[init_dt]
        , cm.[expr_dt]
        , cm.[current_status]
        , cm.[NRR_status]
        , cm.[memb_trend]
        , cm.[declined_ind]
        , cm.[AVC_amt]
        , cm.[orig_expiry_dt]
        , cm.[orig_memb_level]
        , cm.[inception_dt]
        , cm.[create_loc]
        , cm.[created_by]
        , cm.[create_dt]
        , cm.[last_updated_by]
        , cm.[last_update_dt]
        , cm.[cur_record]
        , cm.[ben_holder_ind]
        , cm.[recog_amt]
        , cm.[category_trend]
        , cm.[notes]
FROM [dbo].[TX_CUST_MEMBERSHIP] AS cm 
WHERE cm.[current_status] IN (1, 9) 
  AND cm.[customer_no] NOT IN (SELECT [customer_no] FROM [dbo].[TX_CUST_MEMBERSHIP] WHERE current_status = 2) 
  AND cm.[expr_dt] = (SELECT MAX (cm2.[expr_dt]) AS Exp1 
                      FROM TX_CUST_MEMBERSHIP AS cm2
                      WHERE cm.[customer_no] = cm2.[customer_no] AND cm2.[current_status] IN (1,9)
                      GROUP BY cm2.[customer_no]) 
GO

GRANT SELECT ON [dbo].[LV_ADV_LAST_INACTIVE_MEM] TO ImpUsers
GO


--SELECT * FROM [dbo].[LV_ADV_LAST_INACTIVE_MEM]

