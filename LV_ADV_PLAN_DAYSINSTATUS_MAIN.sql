USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_ADV_PLAN_DAYSINSTATUS_MAIN]
AS

--When a plan is first set up, a Status Change step is not created.  
--So, a plan has to first be evaluated to see if there are any Status Change steps.  
--IF not, today�s date needs to be referenced against the plan�s create date.  
--IF there are Status Change steps, the most recent status change step date needs to be evaluated.

WITH primaryWorker
AS 
(
	-- CTE code taken from VS_PLAN_WITH_PRIMARY_WORKER
	SELECT plan_no, 
		dn.display_name as primaryWorker, 
		dn.customer_no as primaryWorkerNo, 
		dn.display_name_tiny as primaryWorkerTiny
	FROM TX_CUST_PLAN cp
	JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() dn 
		ON cp.customer_no = dn.customer_no
	WHERE cp.primary_ind = 'Y'
)
SELECT p.customer_no, 
	cust.display_name,
	p.plan_no, 
	c.description AS campaignDesc,
	cdes.description AS designationDesc, 
	p.start_dt AS askDate, 
	p.complete_by_dt AS expectedDate, 
	p.last_update_dt AS plan_last_update_dt, 
	sta.description AS planStatus,
	wkr.primaryWorker, 
	MIN( CASE 
		WHEN s.step_no IS NULL THEN DATEDIFF(d,p.create_dt,GETDATE())
		ELSE DATEDIFF(d,s.step_dt,GETDATE())
	END 
	) AS daysInStatus
FROM dbo.T_PLAN p
INNER JOIN dbo.TR_PLAN_STATUS sta
	ON sta.id = p.status
INNER JOIN dbo.T_CAMPAIGN c
	ON c.campaign_no = p.campaign_no
INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS cust 
	ON p.customer_no = cust.customer_no
LEFT JOIN primaryWorker wkr
	ON wkr.plan_no = p.plan_no
LEFT JOIN dbo.TR_CONT_DESIGNATION cdes
	ON cdes.id = p.cont_designation
LEFT JOIN dbo.T_STEP s
	ON s.plan_no = p.plan_no
	AND s.step_type = -1
GROUP BY p.customer_no, cust.display_name, p.plan_no, c.description, cdes.description, p.start_dt, p.complete_by_dt, p.last_update_dt, sta.description, wkr.primaryWorker
GO

GRANT SELECT ON  [dbo].[LV_ADV_PLAN_DAYSINSTATUS_MAIN] to impusers
GO


