USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_BATCH_INFORMATON]'))
    DROP VIEW [dbo].[LV_BATCH_INFORMATON]
GO

CREATE VIEW [dbo].[LV_BATCH_INFORMATON] AS
SELECT   bat.[batch_No] AS 'batch_no'
        ,bat.[batch_type] AS 'batch_type'
        ,bty.[description] AS 'batch_type_name'   
        ,bat.[cntl_ind] AS 'batch_is_controlled'
        ,bat.[status] AS 'batch_status'
        ,CASE WHEN bat.[status] = 'C' THEN 'Closed' WHEN bat.[status] = 'H' THEN 'Held'
              WHEN bat.[status] = 'O' THEN 'Open'   WHEN bat.[status] = 'P' THEN 'Posted' ELSE bat.[status] END AS 'batch_status_name'
        ,bat.[owner] AS 'batch_owner'
        ,(own.[fname] + ' ' + own.[lname]) AS 'batch_owner_name'
        ,bat.[created_by] AS 'batch_created_by'
        ,(cby.[fname] + ' ' + cby.[lname]) AS 'batch_created_by_name'
        ,bat.[create_dt] AS 'batch_create_dt'
        ,CONVERT(CHAR(10),bat.[create_dt],111) AS 'batch_create_date'
        ,CONVERT(CHAR(8),bat.[create_dt],108) AS 'batch_create_time'
        ,ISNULL(bat.[open_loc],'') AS 'batch_open_location'
        ,ISNULL(bat.[closed_by],'') AS 'batch_closed_by'
        ,clo.[fname] + ' ' + clo.[lname] AS 'batch_closed_by_name'
        ,bat.[close_dt] AS 'batch_closed_dt'
        ,CONVERT(CHAR(10),bat.[close_dt],111) AS 'batch_close_date'
        ,CONVERT(CHAR(8),bat.[close_dt],108) AS 'batch_close_time'
        ,bat.[post_no] AS 'batch_post_no'
        ,ISNULL(bat.[posted_by],'') AS 'batch_posted_by'
        ,pos.[fname] + ' ' + pos.[lname] AS 'batch_posted_by_name'
        ,bat.[posted_dt] AS 'batch_posted_dt'
        ,CONVERT(CHAR(10),bat.[posted_dt],111) AS 'batch_posted_date'
        ,CONVERT(CHAR(8), bat.[posted_dt],108) AS 'batch_posted_time'
        ,ISNULL(bat.[amt_posted],0.0) AS 'batch_amount_posted'
        ,ISNULL(tot.[batch_total_payments],0.0) AS 'batch_total_payments'
        ,bat.[tally_PL_amt] AS 'batch_tally_PL_amount'
        ,bat.[tally_RE_amt] AS 'batch_tally_RE_amount'
        ,ISNULL(bat.[unique_tag],'') AS 'batch_unique_tag'
        ,ISNULL(bat.[num_acc_cc],0) AS 'batch_num_acc_cc'
        ,ISNULL(bat.[num_rej_cc],0) AS 'batch_num_rej_cc'
        ,ISNULL(bat.[notes],'') AS 'batch_notes'
FROM [dbo].[T_BATCH] AS bat (NOLOCK)
     INNER JOIN [dbo].[TR_BATCH_TYPE] AS bty (NOLOCK) ON bty.[id] = bat.[batch_type]
     INNER JOIN [dbo].[T_METUSER] AS cby (NOLOCK) ON cby.[userid] = bat.[created_by]
     INNER JOIN [dbo].[T_METUSER] AS own (NOLOCK) ON own.[userid] = bat.[owner]
     INNER JOIN [dbo].[T_METUSER] AS clo (NOLOCK) ON clo.[userid] = bat.[closed_by]
     INNER JOIN [dbo].[T_METUSER] AS pos (NOLOCK) ON pos.[userid] = bat.[posted_by]
     LEFT OUTER JOIN [dbo].[LV_BATCH_TOTALS] AS tot (NOLOCK) ON tot.[batch_no] = bat.[batch_no]
GO

GRANT SELECT ON [dbo].[LV_BATCH_INFORMATON] TO ImpUsers
GO


--SELECT * FROM dbo.LV_BATCH_INFORMATON
--SELECT COUNT(DISTINCT batch_no) FROM dbo.LV_BATCH_TOTALS
--SELECT DISTINCT [status] FROM T_BATCH


     