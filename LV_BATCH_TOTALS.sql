USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_BATCH_TOTALS]'))
    DROP VIEW [dbo].[LV_BATCH_TOTALS]
GO

/*  LV_BATCH_INFORMATION IS DEPENDENT ON THIS VIEW
    DO NOT DELETE AND DO NOT CHANGE NAME OF BATCH_TOTAL_PAYMENTS COLUMN  */

CREATE VIEW [dbo].[LV_BATCH_TOTALS] AS
SELECT   pay.[batch_no] AS 'batch_no'
        ,SUM(pay.[pmt_amt]) AS 'batch_total_payments'
FROM [dbo].[T_PAYMENT] AS pay (NOLOCK)
GROUP BY pay.[batch_no]
GO

GRANT SELECT ON [dbo].[LV_BATCH_TOTALS] TO ImpUsers
GO