USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_BATCH_TOTALS_BY_METHOD]'))
    DROP VIEW [dbo].[LV_BATCH_TOTALS_BY_METHOD]
GO


CREATE VIEW [dbo].[LV_BATCH_TOTALS_BY_METHOD] AS
SELECT   pay.[batch_no] AS 'batch_no'
        ,pmt_method AS 'batch_payment_method'
        ,mth.[description] AS 'batch_payment_method_name'
        ,SUM(pay.[pmt_amt]) AS 'batch_total_payments'
FROM [dbo].[T_PAYMENT] AS pay (NOLOCK)
     INNER JOIN [dbo].[TR_PAYMENT_METHOD] AS mth (NOLOCK) ON mth.[id] = pay.[pmt_method]
GROUP BY pay.[batch_no], pay.[pmt_method], mth.[description]
GO

GRANT SELECT ON [dbo].[LV_BATCH_TOTALS_BY_METHOD] TO ImpUsers
GO
