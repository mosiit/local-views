USE [impresario]
GO

/****** Object:  View [dbo].[LV_BOARD_MEMBERS]    Script Date: 9/26/2019 10:47:48 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[LV_BOARD_MEMBERS]
AS
-- ====================================================================================================
-- Modified for SCTASK0000333 - [TESS] The Board of Overseers has been renamed Board of Museum Advisors
--
--	Add affiliation types 10239 (Former Museum Advisor) and 10240 (Former Museum Advisor Emeriti) to
--	the following views: LV_BOARD_STATUS_A1, LV_BOARD_STATUS_A2, LV_BOARD_STATUS_MAIN, LV_BOARD_MEMBERS
-- ====================================================================================================
WITH BoardMemCTE
AS 
(
	SELECT at.description,
		a.*
	FROM    T_AFFILIATION AS a
		INNER JOIN TR_AFFILIATION_TYPE AS at
			ON a.affiliation_type_id = at.id
	-- 20190926, H. Sheridan - include two new affiliation types
	WHERE   a.affiliation_type_id IN (10086, 10088, 10087, 10090, 10089, 10091, 10095, 10092, 10094, 10239, 10240)
	--WHERE   a.affiliation_type_id IN (10086, 10088, 10087, 10090, 10089, 10091, 10095, 10092, 10094)
)
SELECT  * 
FROM BoardMemCTE
WHERE   affiliation_type_id IN (10086, 10088, 10087, 10090, 10089) -- Active Trustee,Overseer Emeriti,Active Overseer,Life Trustee,Trustee Emeriti
UNION
SELECT  * 
FROM BoardMemCTE
WHERE affiliation_type_id IN (10091, 10095) -- Former Trustee,Former Trustee Emeriti
	AND individual_customer_no NOT IN 
		(
		SELECT  individual_customer_no
		FROM    BoardMemCTE
		WHERE   affiliation_type_id IN (10086, 10088, 10087, 10090, 10089)
		)
UNION
SELECT  * 
FROM BoardMemCTE
WHERE affiliation_type_id IN (10092, 10094) -- Former Overseer, Former Overseer Emeriti
	AND individual_customer_no NOT IN 
	(
		SELECT  individual_customer_no
		FROM    BoardMemCTE
		WHERE   affiliation_type_id IN (10086, 10088, 10087, 10090, 10089, 10091, 10095)
	)
-- 20190926, H. Sheridan - add code for two new affiliation types
UNION
SELECT  * 
FROM BoardMemCTE
WHERE affiliation_type_id IN (10239, 10240) -- Former Museum Advisor, Former Museum Advisor Emeriti
	AND individual_customer_no NOT IN 
	(
		SELECT  individual_customer_no
		FROM    BoardMemCTE
		WHERE   affiliation_type_id IN (10086, 10088, 10087, 10090, 10089, 10091, 10095, 10092, 10094)
	)
GO


