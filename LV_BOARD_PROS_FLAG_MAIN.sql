USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_BOARD_PROS_FLAG_MAIN]
AS

--using DISTINCT because sometimes a person can have both association_type_ids
SELECT DISTINCT 'Ever_Yes' AS board_prospect, search_customer_no AS customer_no
FROM LV_A1_AND_INDIVIDUAL a
INNER JOIN dbo.T_ASSOCIATION ass
	ON ass.customer_no = a.join_customer_no
	AND ass.association_type_id IN (86,89) -- ONom Suggester, TNom Suggester
WHERE a.join_customer_no NOT IN (SELECT customer_no from TX_CONST_CUST WHERE constituency IN (1,23)) -- Board Member, Former Board
GO

GRANT SELECT ON  [dbo].[LV_BOARD_PROS_FLAG_MAIN] to impusers
GO