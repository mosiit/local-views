USE [impresario]
GO

/****** Object:  View [dbo].[LV_BOARD_STATUS_A1]    Script Date: 9/26/2019 10:47:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[LV_BOARD_STATUS_A1]
AS
-- ====================================================================================================
-- Modified for SCTASK0000333 - [TESS] The Board of Overseers has been renamed Board of Museum Advisors
--
--	Add affiliation types 10239 (Former Museum Advisor) and 10240 (Former Museum Advisor Emeriti) to
--	the following views: LV_BOARD_STATUS_A1, LV_BOARD_STATUS_A2, LV_BOARD_STATUS_MAIN, LV_BOARD_MEMBERS
-- ====================================================================================================
SELECT  at.description,
        a2.group_customer_no AS main_customer_no,
        a.*
FROM    T_AFFILIATION AS a
        INNER JOIN TR_AFFILIATION_TYPE AS at
            ON a.affiliation_type_id = at.id
        INNER JOIN T_AFFILIATION AS a2
            ON a.individual_customer_no = a2.individual_customer_no
		INNER JOIN LV_BOARD_MEMBERS mem
			ON mem.individual_customer_no = a.individual_customer_no
			AND mem.affiliation_no = a.affiliation_no
-- 20190926, H. Sheridan - include two new affiliation types
WHERE   a2.affiliation_type_id IN (10002, 10239, 10240)
--WHERE   a2.affiliation_type_id = 10002
        AND a2.name_ind = -1

GO


