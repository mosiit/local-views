USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_BOARD_STATUS_MAIN]
AS

SELECT a.search_customer_no AS customer_no, a.type, b.description AS board_status, b.title, b.start_dt, b.end_dt, b.note
FROM LV_A1_AND_INDIVIDUAL a
INNER JOIN dbo.LV_BOARD_MEMBERS b
	ON a.join_customer_no = b.individual_customer_no
GO

GRANT SELECT ON  [dbo].[LV_BOARD_STATUS_MAIN] to impusers
GO
