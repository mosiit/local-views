USE [impresario]
GO

/****** Object:  View [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]    Script Date: 9/19/2018 2:26:01 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]'))
DROP VIEW [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS] AS
    SELECT	DISTINCT c.ref_no AS [reference_no],
			c.[customer_no] AS [customer_no],
			CASE WHEN cust.[cust_type] = 7 THEN cust.[lname] ELSE cs.[esal1_desc] END AS [constituent_name],
			c.[cont_type] AS [contribution_type],
			'' AS [creditee_type],
			CONVERT(DATE,c.[cont_dt]) AS [contribution_date],
			bp.[fyear] AS [fiscal_year],
			bp.[quarter] AS [fiscal_quarter],
			bp.[period] AS [fiscal_period],
			c.[cont_amt] AS [contribution_amount],
			c.recd_amt AS [received_amount],
			kv.key_value AS [is_anonymous],
			c.[custom_1] AS [soft_credit_type],
			coa.[fund_description] AS [fund_description],
			coa.[printable_fund] AS [printable_fund],
			coa.[designation] AS [designation],
			coa.[overall_cm] AS [overall_campaign],
            c.[campaign_no] AS [campaign_no],
			cm.[description] AS [campaign],
			coa.[cm_category] AS [campaign_category],
			coa.[acct_goal] AS [account_goal],
			coa.[acct_grp] AS [account_group],
			a.[description] AS [appeal],
			mt.[description] AS [media],
			amt.[source_name] AS [source_name],
			REPLACE(REPLACE(c.[notes],CHAR(13),'/'),CHAR(10),'/') AS [notes],
			c.[cancel] AS [cancel],
			ps.[description] AS [pledge_status_desc],
			bt.[description] AS [billing_type],
			c.[batch_no] AS [batch],     --**
			c.[KG_xfer_dt] AS [KG_xfer_dt],
			c.[KGift_desc] AS KGift_desc,
			c.[custom_2] AS [stock_ticker],
			kv3.[key_value] AS [contribution_detail1],
			kv4.[key_value] AS [contribution_detail2],
			c.[custom_6] AS [agreement_date],
			c.[custom_7] AS [challenge_earned],
			kv5.[key_value] AS [pg_instrument],
			c.[custom_0] AS [solicitation],
			c.[worker_customer_no] AS [solicitor_number],
			WCS.[esal1_desc] AS [solicitor_name],
			coa.[cpf_flag] AS [cpf_flag],
			coa.[exh_prog_cat] AS [exh_prog_cat],
			coa.[full_fund] AS [full_fund_name],
			coa.[cm_category1] AS [cm_category1],
			coa.[cm1_intermediatelevel] AS [cm_intermediate1],
			coa.[cm1_main_level] AS [cm_pillar1],
			coa.[cm_category2] AS [cm_category2],
			cm_intermediate2 = coa.cm2_intermediate_level,
			cm_category3 = coa.cm_category3,
			cm_intermediate3 = coa.cm3_intermediate_level,
			c.[custom_2] AS [custom_2],
			c.[custom_9] AS [custom_9],
			ct.[description] AS [main_customer_type],
			ct.[description] AS [original_customer_type],
			cust.[sort_name] AS [sort_name],
			chan.[description] AS [channel],
            ISNULL(cbu.[Business_Unit],'Unknown') AS [Business_Unit]
	FROM [dbo].[T_CONTRIBUTION] AS c (NOLOCK)
          INNER JOIN [dbo].[T_CUSTOMER] AS cust (NOLOCK) ON c.[customer_no] = cust.[customer_no]
          INNER JOIN [dbo].[TR_CUST_TYPE] AS ct (NOLOCK) ON cust.[cust_type] = ct.[id]
          INNER JOIN [dbo].[TX_CUST_SAL] AS cs (NOLOCK) ON c.[customer_no] = cs.[customer_no] AND cs.[default_ind] = 'Y'
          INNER JOIN [dbo].[TR_SALES_CHANNEL] AS chan (NOLOCK) ON chan.[id] = c.[channel]
          LEFT OUTER JOIN [dbo].[TX_CUST_SAL] WCS (NOLOCK) ON c.[worker_customer_no] = WCS.[customer_no] AND WCS.[default_ind] = 'Y' 
          LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv (NOLOCK) ON c.[custom_5] = kv.[key_value] AND kv.[keyword_no] = 460
          LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv2 (NOLOCK) ON c.[custom_1] = kv2.[key_value] AND kv2.[keyword_no] = 456
          INNER JOIN [dbo].[TR_Batch_Period] AS bp (NOLOCK) ON c.[cont_dt] BETWEEN bp.[start_dt] AND bp.[end_dt]
          INNER JOIN [dbo].[T_CAMPAIGN] AS cm (NOLOCK) ON c.[campaign_no] = cm.[campaign_no]
          INNER JOIN [dbo].[LV_MOS_CHART_OF_ACCOUNTS] AS coa (NOLOCK) ON c.[fund_no] = coa.[fund_no] AND cm.[campaign_no] = coa.[campaign_no]
          INNER JOIN [dbo].[T_APPEAL] AS a (NOLOCK) ON c.[appeal_no] = a.[appeal_no]
          INNER JOIN [dbo].[TR_MEDIA_TYPE] AS mt (NOLOCK) ON c.[media_type] = mt.[id]
          INNER JOIN [dbo].[TX_APPEAL_MEDIA_TYPE] AS amt (NOLOCK) ON c.[source_no] = amt.[source_no]
          LEFT OUTER JOIN [dbo].[TR_BILLING_TYPE] AS bt (NOLOCK) ON c.[billing_type] = bt.[id]
          LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv3 (NOLOCK) ON c.[custom_3] = kv3.[key_value] AND kv3.[keyword_no] = 458
          LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv4 (NOLOCK) ON c.[custom_4] = kv4.[key_value] AND kv4.[keyword_no] = 459
          LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv5 (NOLOCK) ON c.[custom_8] = kv5.[key_value] AND kv5.[keyword_no] = 463
          LEFT OUTER JOIN [dbo].[TX_CUST_SAL] AS cs2 (NOLOCK) ON c.worker_customer_no = cs2.customer_no AND cs2.default_ind = 'Y'
          LEFT OUTER JOIN [dbo].[TR_PLEDGE_STATUS] PS (NOLOCK) ON c.[pledge_status] = PS.[id]
          LEFT OUTER JOIN [dbo].[LTR_CAMPAIGN_BUSINESS_UNIT] AS cbu (NOLOCK) ON cbu.[campaign_no] = c.[campaign_no]
	WHERE ISNULL(c.[custom_1],'(none)') IN ('(none)','Matching Gift Credit')
	  AND c.[cont_amt] > 0 
    UNION ALL
    SELECT	DISTINCT c.[ref_no] AS [reference_no],
			cr.[creditee_no] AS [customer_no],
			CASE WHEN cust2.[cust_type] = 7 THEN cust2.[lname] ELSE cs.[esal1_desc] END AS [constituent_name],
			c.[cont_type] AS [contribution_type],
			crt.[descriptiuon] AS [creditee_type],
			CONVERT(DATE,c.[cont_dt]) AS [contribution_date],
			bp.[fyear] AS [fiscal_year],
			bp.[quarter] AS [fiscal_quarter],
			bp.[period] AS [fiscal_period],
			c.[cont_amt] AS [contribution_amount],
			c.[recd_amt] AS [received_amount],
			kv.[key_value] AS [is_anonymous],
			c.[custom_1] AS [soft_credit_type],
			coa.[fund_description] AS [fund_description],
			coa.[printable_fund] AS [printable_fund],
			coa.[designation] AS [designation],
			coa.[overall_cm] AS [overall_campaign],
            c.[campaign_no] AS [campaign_no],
			cm.[description] AS [campaign],
			coa.[cm_category] AS [campaign_category],
			coa.[acct_goal] AS [account_goal],
			coa.[acct_grp] AS [account_group],
			a.[description] AS [appeal],
			mt.[description] AS [media],
			amt.[source_name] AS [source_name],
			REPLACE(REPLACE(c.[notes],CHAR(13),'/'),CHAR(10),'/') AS [notes],
			c.cancel AS [cancel],
			ps.[description] AS [pledge_status_desc],
			bt.[description] AS [billing_type],
			c.[batch_no] AS [batch],
			c.[KG_xfer_dt] AS [KG_xfer_dt],
			c.[KGift_desc] AS [KGift_desc],
			c.[custom_2] AS [stock_ticker],
			kv3.[key_value] AS [contribution_detail1],
			kv4.[key_value] AS [contribution_detail2],
			c.[custom_6] AS [agreement_date],
			c.[custom_7] AS [challenge_earned],
			kv5.[key_value] AS [pg_instrument],
			c.[custom_0] AS [solicitation],
			c.[worker_customer_no] AS [solicitor_number],
			WCS.[esal1_desc] AS [solicitor_name],
			coa.[cpf_flag] AS [cpf_flag],
			coa.[exh_prog_cat] AS [exh_prog_cat],
			coa.[full_fund] AS [full_fund_name],
			coa.[cm_category1] AS [cm_category1],
			coa.[cm1_intermediatelevel] AS [cm_intermediate1],
			coa.[cm1_main_level] AS [cm_pillar1],
			coa.[cm_category2] AS [cm_category2],
			coa.[cm2_intermediate_level] AS [cm_intermediate2],
			coa.[cm_category3] AS [cm_category3],
			coa.[cm3_intermediate_level] AS [cm_intermediate3],
			c.[custom_2] AS [custom_2],
			c.[custom_9] AS [custom_8],
			ct2.[description] AS [main_customer_type],
			ct.[description] AS [original_customer_type],
			cust2.[sort_name] AS [sort_name],
			chan.[description] AS [channel],
            ISNULL(cbu.[Business_Unit],'Unknown') AS [Business_Unit]
	FROM [dbo].[T_CONTRIBUTION] AS c (NOLOCK)
          INNER JOIN [dbo].[T_CUSTOMER] AS cust (NOLOCK) ON c.[customer_no] = cust.[customer_no]
          INNER JOIN [dbo].[TR_CUST_TYPE] AS ct (NOLOCK) ON cust.[cust_type] = ct.[id]
          INNER JOIN [dbo].[TR_SALES_CHANNEL] AS chan (NOLOCK) ON chan.[id] = c.[channel]
          LEFT OUTER JOIN [dbo].[TX_CUST_SAL] WCS (NOLOCK) ON C.[worker_customer_no] = WCS.[customer_no] AND WCS.[default_ind] = 'Y' 
          INNER JOIN [dbo].[T_CREDITEE] AS cr (NOLOCK) ON c.[ref_no] = cr.[ref_no]
          INNER JOIN [dbo].[TR_CREDITEE_TYPE] AS crt (NOLOCK) ON cr.[creditee_type] = crt.[id]
          INNER JOIN [dbo].[T_CUSTOMER] AS cust2 (NOLOCK) ON cr.[creditee_no] = cust2.[customer_no]
          INNER JOIN [dbo].[TR_CUST_TYPE] AS ct2 (NOLOCK) ON cust2.[cust_type] = ct2.[id]
          INNER JOIN [dbo].[TX_CUST_SAL] AS cs (NOLOCK) ON cr.[creditee_no] = cs.[customer_no] AND cs.[default_ind] = 'Y'
          LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv (NOLOCK) ON c.[custom_5] = kv.[key_value] AND kv.[keyword_no] = 460
          LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv2 (NOLOCK) ON c.[custom_1] = kv2.[key_value] AND kv2.[keyword_no] = 456
          INNER JOIN [dbo].[TR_Batch_Period] AS bp (NOLOCK) ON c.[cont_dt] BETWEEN bp.[start_dt] AND bp.[end_dt]
          INNER JOIN [dbo].[T_CAMPAIGN] AS cm (NOLOCK) ON c.[campaign_no] = cm.[campaign_no]
          INNER JOIN [dbo].[LV_MOS_CHART_OF_ACCOUNTS] AS coa (NOLOCK) ON c.[fund_no] = coa.[fund_no] AND cm.[campaign_no] = coa.[campaign_no] 
          INNER JOIN [dbo].[T_APPEAL] AS a (NOLOCK) ON c.[appeal_no] = a.[appeal_no]
          INNER JOIN [dbo].[TR_MEDIA_TYPE] AS mt (NOLOCK) ON c.[media_type] = mt.[id]
          INNER JOIN [dbo].[TX_APPEAL_MEDIA_TYPE] AS amt (NOLOCK) ON c.[source_no] = amt.[source_no]
          LEFT OUTER JOIN [dbo].[TR_BILLING_TYPE] AS bt (NOLOCK) ON c.[billing_type] = bt.[id]
          LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv3 (NOLOCK) ON c.[custom_3] = kv3.[key_value] AND kv3.[keyword_no] = 458
          LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv4 (NOLOCK) ON c.[custom_4] = kv4.[key_value] AND kv4.[keyword_no] = 459
          LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv5 (NOLOCK) ON c.[custom_8] = kv5.[key_value] AND kv5.[keyword_no] = 463
          LEFT OUTER JOIN [dbo].[TX_CUST_SAL] AS cs2 (NOLOCK) ON c.[worker_customer_no] = cs2.[customer_no] AND cs2.[default_ind] = 'Y'
          LEFT OUTER JOIN [dbo].[TR_PLEDGE_STATUS] PS (NOLOCK) ON c.[pledge_status] = PS.[id]
          LEFT OUTER JOIN [dbo].[LTR_CAMPAIGN_BUSINESS_UNIT] AS cbu (NOLOCK) ON cbu.[campaign_no] = c.[campaign_no]
	WHERE c.[custom_1] IN ('Primary Soft Credit')
      AND cr.[creditee_type] IN (5,12,15)
      AND c.[cont_amt] > 0 
GO

GRANT SELECT ON [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS] TO ImpUsers
GO


--SELECT * FROM [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS] WHERE [contribution_date] BETWEEN '7-1-2018' AND '8-31-2018'

