USE [impresario]
GO

/****** Object:  View [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]    Script Date: 9/19/2018 2:26:01 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS]'))
DROP VIEW [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

    CREATE VIEW [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS
        SELECT wty.[worker_customer_no],
               cus.[fname] AS [worker_name],
               cus.[lname] AS [worker_initials],
               cus.[sort_name] AS [worker_sort],
               wty.[inactive],
               MIN(ISNULL(usr.[inactive],'?')) AS [login_inactive],
               MIN(ISNULL(usr.[userid],'NO LOGIN')) AS [tessitura_login_id], 
               MIN(ISNULL(usr.[location],'No Login Found')) AS [tessitura_location], 
               MAX(CAST (ISNULL(kw1.[key_value],'0') AS INT)) AS [goal_visits],
               MAX(CAST (ISNULL(kw2.[key_value],'0') AS INT)) AS [goal_hard_contacts],
               MAX(CAST (ISNULL(kw3.[key_value],'0') AS INT)) AS [goal_soft_contacts],
               MAX(CAST (ISNULL(kw4.[key_value],'0.0') AS DECIMAL(18,2))) AS [goal_ask_total],
               MAX(CAST (ISNULL(kw5.[key_value],'0.0') AS DECIMAL(18,2))) AS [goal_expected_total],
               MAX(CAST (ISNULL(kw6.[key_value],'0.0') AS DECIMAL(18,2))) AS [goal_prim_sol_rev_total],
               MAX(CAST (ISNULL(kw7.[key_value],'0') AS INT)) AS [goal_solicitations],
               MAX(CAST (ISNULL(kw8.[key_value],'0') AS INT)) AS [goal_qualifications]
        FROM [dbo].[TX_CUST_WORKER_TYPE] AS wty (NOLOCK)
             INNER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = wty.[worker_customer_no]
             LEFT OUTER JOIN [dbo].[T_METUSER] AS usr (NOLOCK) ON usr.worker_customer_no = wty.worker_customer_no
             LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS kw1 (NOLOCK) ON kw1.customer_no = wty.worker_customer_no AND kw1.keyword_no = 613            --613 = Goal_Visits
             LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS kw2 (NOLOCK) ON kw2.customer_no = wty.worker_customer_no AND kw2.keyword_no = 614            --614 = Goal_Hard_Contacts
             LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS kw3 (NOLOCK) ON kw3.customer_no = wty.worker_customer_no AND kw3.keyword_no = 615            --615 = Goal_Soft_Contacts
             LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS kw4 (NOLOCK) ON kw4.customer_no = wty.worker_customer_no AND kw4.keyword_no = 616            --616 = Goal_Ask_Total
             LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS kw5 (NOLOCK) ON kw5.customer_no = wty.worker_customer_no AND kw5.keyword_no = 617            --617 = Goal_Expected_Total
             LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS kw6 (NOLOCK) ON kw6.customer_no = wty.worker_customer_no AND kw6.keyword_no = 618            --618 = Goal_Prim_Sol_Rev_Total
             LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS kw7 (NOLOCK) ON kw7.customer_no = wty.worker_customer_no AND kw7.keyword_no = 659            --659 = Goal_Solicitations
             LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS kw8 (NOLOCK) ON kw8.customer_no = wty.worker_customer_no AND kw8.keyword_no = 660            --660 = Goal_Qualifications
        WHERE wty.worker_type = 7  --  7 = Current Adv Solicitor
        GROUP BY wty.[worker_customer_no], wty.[inactive], cus.[fname], cus.[lname], cus.[sort_name]
GO

GRANT SELECT ON [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] TO ImpUsers, tessitura_app
GO

--SELECT * FROM LV_CAMPAIGN_ACTIVITY_WORKERS WHERE [worker_customer_no] = 4016769
--SELECT * FROM [dbo].[TX_CUST_KEYWORD] WHERE [customer_no] = 4016769 AND [keyword_no] = 618
--SELECT * FROM [dbo].[TX_CUST_WORKER_TYPE] WHERE [worker_type] = 7
--SELECT [worker_customer_no], COUNT(*) FROM LV_CAMPAIGN_ACTIVITY_WORKERS GROUP BY [worker_customer_no] HAVING COUNT(*) > 1
