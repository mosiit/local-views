USE [impresario];
GO

SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

CREATE VIEW [dbo].[LV_CAMPAIGN_CONT_DESIGNATION]
AS
SELECT  c.campaign_no,
        c.description AS campaignName,
        c.fyear AS campaignFY,
        c.inactive AS campaignInactive,
        c.start_dt AS campaignStartDt,
        c.end_dt AS campaignEndDt,
        ccat.id AS campaignCategoryID,
        ccat.description AS campaignCategoryDesc,
        ccd.id AS campContDesigID,
        ccd.cont_designation AS contDesignationID,
        vcd.description AS contDesignationDesc,
        ccd.default_ind AS contDesginationDefaultInd
FROM    TX_CAMPAIGN_CONT_DESIGNATION AS ccd
        INNER JOIN T_CAMPAIGN AS c
            ON ccd.campaign_no = c.campaign_no
        INNER JOIN VRS_CONT_DESIGNATION AS vcd
            ON ccd.cont_designation = vcd.id
        INNER JOIN dbo.TR_CAMPAIGN_CATEGORY ccat
            ON ccat.id = c.category;

GO

GRANT SELECT ON  [dbo].[LV_CAMPAIGN_CONT_DESIGNATION] TO ImpUsers;
GO