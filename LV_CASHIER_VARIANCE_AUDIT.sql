
--DROP VIEW [dbo].[LV_CASHIER_VARIANCE_AUDIT] 
--GO

CREATE VIEW [dbo].[LV_CASHIER_VARIANCE_AUDIT] AS
SELECT 0 AS [audit_id],
       v.[id],
       v.[variance_unique_identifier] AS [unique_id],
       v.[operator_id],
       v.[operator_first_name],
       v.[operator_last_name],
       v.[operator_first_name] + ' ' + v.[operator_last_name] AS [operator_full_name],
       v.[operator_last_name] + ', ' + v.[operator_first_name] AS [operator_sort_name],
       v.[variance_date],
       v.[variance_type],
       v.[variance_amount],
       v.[variance_reason],
       ISNULL(r.[description],'') AS [variance_reason_str],
       v.[variance_status],
       v.[variance_notes],
       v.[inactive],
       CONVERT(VARCHAR(50),v.[created_by]) AS [audit_user],
       v.[create_dt] AS [audit_timestamp],
       'Insert' AS [audit_type],
       CONVERT(VARCHAR(100),'record added to table') AS [audit_info]
FROM [dbo].[LTR_CASHIER_VARIANCES] AS v
     LEFT OUTER JOIN [dbo].[LTR_CASHIER_VARIANCE_REASONS] AS r ON r.[id] = v.[variance_reason]
UNION ALL
SELECT a.[audit_id],
       v.[id],
       v.[variance_unique_identifier] AS [unique_id],
       v.[operator_id],
       v.[operator_first_name],
       v.[operator_last_name],
       v.[operator_first_name] + ' ' + v.[operator_last_name] AS [operator_full_name],
       v.[operator_last_name] + ', ' + v.[operator_first_name] AS [operator_sort_name],
       v.[variance_date],
       v.[variance_type],
       v.[variance_amount],
       v.[variance_reason],
       ISNULL(r.[description],'') AS [variance_reason_str],
       v.[variance_status],
       v.[variance_notes],
       v.[inactive],
       a.[audit_user],
       a.[audit_timestamp],
       'Update' AS [audit_type],
       CASE WHEN a.[column_changed] = 'variance_notes'  THEN 'notes updated by '
                                                        ELSE a.[column_changed] + ' changed from ' + a.[original_value] + ' to ' + a.[changed_value] END AS [audit_info]
FROM [dbo].[LTR_CASHIER_VARIANCES] AS v
     LEFT OUTER JOIN [dbo].[LTA_CASHIER_VARIANCES_AUDIT] AS a ON a.variance_id = v.id
     LEFT OUTER JOIN [dbo].[LTR_CASHIER_VARIANCE_REASONS] AS r ON r.[id] = v.[variance_reason]
WHERE ISNULL(a.[audit_user],'') <> ''
GO

GRANT SELECT ON [dbo].[LV_CASHIER_VARIANCE_AUDIT] TO ImpUsers
GO

SELECT * FROM [dbo].[LV_CASHIER_VARIANCE_AUDIT] WHERE variance_date = '2018/01/27'





