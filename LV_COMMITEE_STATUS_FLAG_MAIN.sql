USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_COMMITEE_STATUS_FLAG_MAIN]
AS

--ONLY SHOW "FORMER COMMITTEE", IF MEMBER DOES NOT ALSO HAVE AN "ACTIVE COMMITTEE"
SELECT DISTINCT 'Active Committee' AS committee_status, a.search_customer_no AS customer_no
FROM LV_A1_AND_INDIVIDUAL a
INNER JOIN dbo.T_AFFILIATION aff
	ON aff.individual_customer_no = a.join_customer_no
	AND aff.affiliation_type_id = 10096 -- Active Committee

UNION

SELECT DISTINCT 'Former Committee' AS committee_status, a.search_customer_no AS customer_no
FROM LV_A1_AND_INDIVIDUAL a
INNER JOIN dbo.T_AFFILIATION aff
	ON aff.individual_customer_no = a.join_customer_no
	AND aff.affiliation_type_id = 10097 -- Former Committee
LEFT JOIN 
(
	SELECT individual_customer_no 
	FROM dbo.T_AFFILIATION 
	WHERE affiliation_type_id = 10096 -- Active Committee
) X
ON a.join_customer_no = X.individual_customer_no
WHERE X.individual_customer_no IS NULL 
GO

GRANT SELECT ON  [dbo].[LV_COMMITEE_STATUS_FLAG_MAIN] to impusers
GO