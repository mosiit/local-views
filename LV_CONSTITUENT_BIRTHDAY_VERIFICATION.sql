USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_CONSTITUENT_BIRTHDAY_VERIFICATION]
AS

-- Do not include DECEASED individuals

SELECT bday.customer_no, CAST(bday.key_value AS DATE) AS birthDate, ver.key_value verifiedStatus
FROM dbo.TX_CUST_KEYWORD bday
INNER JOIN dbo.T_CUSTOMER cust
	ON cust.customer_no = bday.customer_no
	AND cust.name_status <> 2 -- Deceased
LEFT JOIN dbo.TX_CUST_KEYWORD ver
	ON ver.customer_no = bday.customer_no
	AND ver.keyword_no = 422
WHERE bday.keyword_no IN (1,2)
