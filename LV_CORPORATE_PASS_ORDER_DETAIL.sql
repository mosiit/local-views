

CREATE VIEW [dbo].[LV_CORPORATE_PASS_ORDER_DETAIL] AS

    WITH [CTE_RULES] ([rule_no], [rule_name]) AS (SELECT [id], 
                                                         [description] 
                                                  FROM [dbo].[T_PRICING_RULE]
                                                  WHERE [appeals] = '1760' 
                                                     OR [appeals] LIKE '%1760,%'),
         [CTE_SOURCES] ([appeal_no], [media_type], [source_no], [source_name], [source_group], [source_group_name], [customer_no])
                        AS (SELECT med.[appeal_no],
                                   med.[media_type],
                                   med.[source_no],
                                   med.[source_name], 
                                   med.[source_group],
                                   sgp.[description] AS [source_group_name],
                                   CASE WHEN med.[source_name] LIKE '%(%)%' THEN
                                        SUBSTRING (med.[source_name], CHARINDEX('(', med.[source_name]) + 1, 
                                                                      CHARINDEX(')', med.[source_name]) - CHARINDEX('(', med.[source_name]) -1)
                                        ELSE '0' END AS [source_customer]
                            FROM [dbo].[TX_APPEAL_MEDIA_TYPE] AS med
                                 INNER JOIN [dbo].[TR_SOURCE_GROUP] AS sgp ON sgp.[id] = med.[source_group]
                            WHERE med.[appeal_no] = 1760)
    SELECT sli.[sli_no],
           sli.[order_no],
           sou.[source_group],
           sou.[source_group_name],
           ord.[source_no],
           sou.[source_name],
           sou.[customer_no] AS [source_customer],
           ISNULL(nam.[display_name],sou.[source_name]) AS [member_name],
           ISNULL(adr.[street1],'') AS [member_address_1],
           ISNULL(adr.[street2],'') as [member_address_2],
           ISNULL(adr.[street3],'') AS [member_address_3],
           ISNULL(adr.[city],'') AS [member_city],
           ISNULL(adr.[state],'') AS [member_state],
           CASE WHEN LEN(ISNULL(adr.[postal_code],'')) = 9 THEN LEFT(adr.[postal_code],5) + '-' + RIGHT(adr.[postal_code],4) ELSE ISNULL(adr.[postal_code],'') END AS [member_postal_code],
           sli.[price_type],
           ptp.[description] AS [price_type_name],
           sli.[due_amt],
           sli.[paid_amt],
           sli.[ticket_no],
           sli.[sli_status],
           sta.[description] AS [sli_status_name],
           sli.[perf_no],
           sli.[zone_no],
           prf.[performance_dt],
           prf.[performance_time],
           prf.[performance_time_display],
           prf.[title_no],
           prf.[title_name],
           prf.[production_no],
           prf.[production_name],
           prf.[production_name_long],
           prf.[production_season_no],
           prf.[production_season_name],
           sli.[rule_ind],
           ISNULL(sli.[rule_id], 0) AS [rule_id],
           rul.[rule_name]
    FROM [dbo].[T_SUB_LINEITEM] AS sli
         INNER JOIN [CTE_RULES] AS rul ON rul.[rule_no] = ISNULL(sli.[rule_id], 0)
         INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = sli.[order_no]
         INNER JOIN [CTE_SOURCES] AS sou ON sou.[source_no] = ord.[source_no]
         LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON CAST(nam.[customer_no] AS VARCHAR(20)) = sou.[customer_no]
         LEFT OUTER JOIN [dbo].[FT_GET_PRIMARY_ADDRESS]() AS adr ON CAST(adr.[customer_no] AS VARCHAR(20)) = sou.[customer_no]
         INNER JOIN [dbo].[TR_PRICE_TYPE] AS ptp ON ptp.[id] = sli.[price_type]
         INNER JOIN [dbo].[TR_SLI_STATUS] AS sta ON sta.[id] = sli.[sli_status]
         LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
    WHERE sli.[sli_status] IN (3, 12)
      AND sli.[rule_ind] = 'R'
  
 GO

 GRANT SELECT ON [dbo].[LV_CORPORATE_PASS_ORDER_DETAIL] TO [ImpUsers], [tessitura_app]
 GO

 SELECT * FROM [dbo].[LV_CORPORATE_PASS_ORDER_DETAIL] WHERE [order_no] = 2477477




--    SUBSTRING (sou.[source_name], CHARINDEX('(', sou.[source_name]) + 1, CHARINDEX(')', sou.[source_name]) - CHARINDEX('(', sou.[source_name]) -1) AS [source_customer],