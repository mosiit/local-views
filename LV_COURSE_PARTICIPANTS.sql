USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_COURSE_PARTICIPANTS]'))
    DROP VIEW [dbo].[LV_COURSE_PARTICIPANTS]
GO

CREATE VIEW [dbo].[LV_COURSE_PARTICIPANTS] AS
        SELECT sli.[order_no]
             , sli.[recipient_no] AS 'participant_id'
             , sli.[perf_no] AS 'performance_no'
             , sli.[zone_no] AS 'performance_zone'
             , prf.[performance_code] AS 'course_code'
             , prf.[performance_date] AS 'course_date'
             , prf.[performance_time] AS 'course_time'
             , prf.[performance_time_display] AS 'course_type'
             , prf.[title_name] AS 'course_venue'
             , prf.[production_name] AS 'course_title'
             , prf.[production_name_long] AS 'course_title_long'
             , ISNULL(rec.[fname],'') AS 'participant_first_name'
             , ISNULL(rec.[mname],'') AS 'participant_middle_name'
             , ISNULL(rec.[lname],'') AS 'participant_last_name'
             , LTRIM(ISNULL(rec.[fname],'') + ' ' + CASE WHEN ISNULL(rec.[mname],'') = '' THEN '' ELSE rec.[mname] + ' ' END + ISNULL(rec.[lname],'')) AS 'participant_name'
             , LTRIM(ISNULL(rec.[lname],'') + ', ' + ISNULL(rec.[fname],'') + CASE WHEN ISNULL(rec.[mname],'') = '' THEN '' ELSE ' ' + rec.[mname] END) AS 'participant_sort_name'
             , ord.[customer_no]
             , ISNULL(pre.[description],'') AS 'customer_prefix'
             , ISNULL(cus.[fname],'') AS 'customer_first_name'
             , ISNULL(cus.[mname],'') AS 'customer_middle_name'
             , ISNULL(cus.[lname],'') AS 'customer_last_name'
             , LTRIM(ISNULL(cus.[fname],'') + ' ' + CASE WHEN ISNULL(cus.[mname],'') = '' THEN '' ELSE cus.[mname] + ' ' END + ISNULL(cus.[lname],'')) AS 'customer_name'
             , LTRIM(ISNULL(cus.[lname],'') + ', ' + ISNULL(cus.[fname],'') + CASE WHEN ISNULL(cus.[mname],'') = '' THEN '' ELSE ' ' + cus.[mname] END) AS 'customer_sort_name'
             , ISNULL(adr.[street1],'') AS 'street1'
             , ISNULL(adr.[street2],'') AS 'street2'
             , ISNULL(adr.[city],'') AS 'city'
             , ISNULL(adr.[state],'') AS 'state'
             , ISNULL(LEFT(adr.[postal_code],5),'') AS 'postal_code'
        FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.performance_no = sli.[perf_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS rec (NOLOCK) ON rec.[customer_no] = sli.[recipient_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
             LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr (NOLOCK) ON adr.[customer_no] = ord.[customer_no] AND adr.primary_ind = 'Y'
             LEFT OUTER JOIN [dbo].[TR_PREFIX] AS pre (NOLOCK) ON pre.[id] = cus.[prefix]
        WHERE prf.title_name = 'Summer Courses' AND sli.[sli_status] IN (3, 12)
GO

GRANT SELECT ON [dbo].[LV_COURSE_PARTICIPANTS] TO ImpUsers
GO

SELECT * FROM [LV_COURSE_PARTICIPANTS] WHERE course_date = '2017/07/10'
      

--SELECT * FROM dbo.TR_PHONE_TYPE
--SELECT * FROM dbo.T_ADDRESS
--SELECT * FROM T_PHONE