USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_COURSE_PARTICIPANT_EXTRAS]'))
    DROP VIEW [dbo].[LV_COURSE_PARTICIPANT_EXTRAS]
GO

CREATE VIEW [dbo].[LV_COURSE_PARTICIPANT_EXTRAS] AS
        SELECT sli.[order_no]
             , sli.[recipient_no] AS 'participant_id'
             , sli.[perf_no] AS 'performance_no'
             , sli.[zone_no] AS 'performance_zone'
             , prf.[performance_code] AS 'course_code'
             , prf.[performance_date] AS 'course_date'
             , prf.[performance_time] AS 'course_time'
             , prf.[performance_time_display] AS 'course_type'
             , prf.[title_name] AS 'course_venue'
             , prf.[production_name] AS 'extras_title'
             , prf.[production_name_long] AS 'extras_title_long'
             , LTRIM(ISNULL(rec.[fname],'') + ' ' + CASE WHEN ISNULL(rec.[mname],'') = '' THEN '' ELSE rec.[mname] + ' ' END + ISNULL(rec.[lname],'')) AS 'participant_name'
             , LTRIM(ISNULL(rec.[lname],'') + ', ' + ISNULL([fname],'') + CASE WHEN ISNULL(rec.[mname],'') = '' THEN '' ELSE ' ' + rec.[mname] END) AS 'participant_sort_name'
        FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.performance_no = sli.[perf_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS rec (NOLOCK) ON rec.[customer_no] = sli.[recipient_no]
        WHERE prf.title_name = 'Courses Extras' AND sli.[sli_status] IN (3, 12)
GO

GRANT SELECT ON [dbo].[LV_COURSE_PARTICIPANT_EXTRAS] TO ImpUsers
GO


SELECT * FROM [LV_COURSE_PARTICIPANT_EXTRAS] WHERE course_date = '2017/07/10'