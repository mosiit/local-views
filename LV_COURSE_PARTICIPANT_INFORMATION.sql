USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_COURSE_PARTICIPANT_INFORMATION]'))
    DROP VIEW [dbo].[LV_COURSE_PARTICIPANT_INFORMATION]
GO

CREATE VIEW [dbo].[LV_COURSE_PARTICIPANT_INFORMATION] AS
    WITH [CTE_COURSE_PERFORMANCES] ([perf_no], [perf_zone], [perf_dt], [perf_date], [perf_time], [perf_time_disp], 
                                    [title_no], [title_name], [prod_no], [prod_name], [prod_name_long])
    AS (
        SELECT [performance_no],
               [performance_zone],
               [performance_dt],
               [performance_date],
               [performance_time],
               [performance_time_display],
               [title_no],
               [title_name],
               [production_no],
               [production_name],
               [production_name_long]
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] (NOLOCK)
        WHERE [performance_dt] > '7-1-2016'
          AND [title_no] = 1113         --1113 = Summer Courses
     ),
         [CTE_COURSE_RETURNS] ([sli_no], [ret_parent_sli_no], [sli_status], [sli_status_name], [due_amt], [paid_amt])
    AS (
        SELECT sli.[sli_no], 
               sli.[ret_parent_sli_no],
               sli.[sli_status],
               sta.[description],
               sli.[due_amt],
               sli.[paid_amt]
        FROM [dbo].[T_SUB_LINEITEM] as sli (NOLOCK)
              INNER JOIN [CTE_COURSE_PERFORMANCES] AS cou (NOLOCK) ON cou.perf_no = sli.[perf_no] AND cou.[perf_zone] = sli.[zone_no]
              INNER JOIN [dbo].[TR_SLI_STATUS] AS sta (NOLOCK) ON sta.[id] = sli.[sli_status]
        WHERE sli.sli_status = 4        --4 = Returned
       ) 
SELECT sli.[order_no],
       ord.[order_dt],
       convert(char(10),ord.[order_dt],111) as 'order_date',
       ord.[customer_no],
       IsNull(cus.[fname], '') as 'customer_first_name',
       IsNull(cus.[mname], '') as 'customer_middle_name',
       IsNull(cus.[lname], '') as 'customer_last_name',
       IsNull(adr.[address_type], 0) as 'address_type_no',
       IsNull(aty.[description], '') as 'address_type',
       IsNull(adr.[street1], '') as 'street1',
       IsNull(adr.[street2], '') as 'street2',
       IsNull(adr.[city], '') as 'city',
       IsNull(adr.[state], '') as 'state',
       IsNull(adr.[postal_code], '') as 'postal_code',
       IsNull(hph.[phone], '') as 'home_phone_no',
       IsNull(bph.[phone], '') as 'business_phone_no',
       IsNull(eml.[eaddress_type], 0) as 'email_address_type_no',
       IsNull(ety.[description], '') as 'email_address_type',
       IsNull(eml.[address], '') as 'email_address',
       sli.[sli_no] as 'sublineitem_no',
       sli.[li_seq_no] as 'sublineitem_seq',
       sli.[due_amt] as 'due_amount',
       sli.[paid_amt] as 'paid_amount',
       sli.[sli_status] as 'sale_status_no',
       sst.[description] as 'sale_status',
       IsNull(slr.[sli_no], 0) as 'return_lineitem_no',
       IsNull(slr.[due_amt], 0.00) as 'return_due_amount',
       IsNull(slr.[paid_amt], 0.00) as 'return_paid_amount',
       IsNull(slr.[sli_status], '') as 'return_status_no',
       ISNULL(slr.[sli_status_name],'') as 'return_status',
       CASE WHEN slr.[sli_no] is null THEN 1 ELSE 0 END as 'Quantity',
       prf.[perf_no],
       prf.[title_no],
       prf.[title_name],
       prf.[perf_dt] as 'course_dt',
       prf.[perf_date] as 'course_date',
       prf.[perf_time] as 'course_time',
       prf.[perf_time_disp] as 'course_time_display',
       prf.[prod_name] as 'course_title',
       prf.[prod_name_long] as 'course_title_long',
       IsNull(sli.[recipient_no], 0) as 'participant_customer_no',
       ISNull(rcp.[fname], '') as 'participant_first_name',
       IsNull(rcp.[mname], '') as 'participant_middle_name',
       IsNull(rcp.[lname], '') as 'participant_last_name',
       [dbo].[LFS_HasEarlyDropOffOrLateStay](prf.[perf_dt], IsNull(sli.[recipient_no], 0), 'Early') AS 'Early Drop-Off',
       [dbo].[LFS_HasEarlyDropOffOrLateStay](prf.[perf_dt], IsNull(sli.[recipient_no], 0), 'Late') AS 'Late Stay',
       ISNULL(bth.[key_value],'') AS 'participant_dob',
       ISNULL(grd.[key_value],'') AS 'participant_grade'
FROM [dbo].[T_SUB_LINEITEM] as sli (NOLOCK)
     INNER JOIN [CTE_COURSE_PERFORMANCES] AS prf (NOLOCK) ON prf.[perf_no] = sli.[perf_no] AND prf.[perf_zone] = sli.[zone_no]
     LEFT OUTER JOIN [CTE_COURSE_RETURNS] AS slr (NOLOCK) ON slr.[ret_parent_sli_no] = sli.[sli_no] 
     LEFT OUTER JOIN [dbo].[TR_SLI_STATUS] as sst (NOLOCK) ON sst.[id] = sli.[sli_status]
     LEFT OUTER JOIN [dbo].[T_ORDER] As ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
     LEFT OUTER JOIN [dbo].[T_CUSTOMER] as cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
     LEFT OUTER JOIN [dbo].[T_ADDRESS] as adr (NOLOCK) ON adr.[customer_no] = ord.[customer_no] and adr.[primary_ind] = 'Y'
     LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] as aty (NOLOCK) ON aty.[id] = adr.[address_type]
     LEFT OUTER JOIN [dbo].[T_PHONE] as hph (NOLOCK) ON hph.[customer_no] = ord.[customer_no] and hph.[type] = 8
     LEFT OUTER JOIN [dbo].[T_PHONE] as bph (NOLOCK) ON bph.[customer_no] = ord.[customer_no] and bph.[type] = 14
     LEFT OUTER JOIN [dbo].[T_EADDRESS] as eml (NOLOCK) ON eml.[customer_no] = ord.[customer_no] and eml.[primary_ind] = 'Y'
     LEFT OUTER JOIN [dbo].[TR_EADDRESS_TYPE] as ety (NOLOCK) ON ety.[id] = eml.[eaddress_type]
     LEFT OUTER JOIN [dbo].[T_CUSTOMER] as rcp (NOLOCK) ON rcp.[customer_no] = sli.[recipient_no]
     LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS bth (NOLOCK) ON bth.[customer_no] = sli.[recipient_no] AND bth.[keyword_no] = 1     --1 = Birth Date
     LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS grd (NOLOCK) ON grd.[customer_no] = sli.[recipient_no] AND grd.[keyword_no] = 562   --562 = School Grade
GO

GRANT SELECT ON [dbo].[LV_COURSE_PARTICIPANT_INFORMATION] TO impusers
GO

--SELECT * FROM dbo.LV_COURSE_PARTICIPANT_INFORMATION WHERE course_dt > '7-1-2019'


/*  THIS IS THE ORIGINAL CODE - CHANGING IT TO WHAT IS NOW ABOVE CHANGED SELECTION RUN TIME FROM MINUTES TO SECONDS 

CREATE VIEW [dbo].[LV_COURSE_PARTICIPANT_INFORMATION] AS
SELECT  sli.[order_no]
       ,ord.[order_dt]
       ,convert(char(10),ord.[order_dt],111) as 'order_date'
       ,ord.[customer_no]
       ,IsNull(cus.[fname], '') as 'customer_first_name'
       ,IsNull(cus.[mname], '') as 'customer_middle_name'
       ,IsNull(cus.[lname], '') as 'customer_last_name'
       ,IsNull(adr.[address_type], 0) as 'address_type_no'
       ,IsNull(aty.[description], '') as 'address_type'
       ,IsNull(adr.[street1], '') as 'street1'
       ,IsNull(adr.[street2], '') as 'street2'
       ,IsNull(adr.[city], '') as 'city'
       ,IsNull(adr.[state], '') as 'state'
       ,IsNull(adr.[postal_code], '') as 'postal_code'
       ,IsNull(hph.[phone], '') as 'home_phone_no'
       ,IsNull(bph.[phone], '') as 'business_phone_no'
       ,IsNull(eml.[eaddress_type], 0) as 'email_address_type_no'
       ,IsNull(ety.[description], '') as 'email_address_type'
       ,IsNull(eml.[address], '') as 'email_address'
       ,sli.[sli_no] as 'sublineitem_no' 
       ,sli.[li_seq_no] as 'sublineitem_seq'
       ,sli.[due_amt] as 'due_amount'
       ,sli.[paid_amt] as 'paid_amount'
       ,sli.[sli_status] as 'sale_status_no'
       ,sst.[description] as 'sale_status'
       ,IsNull(slr.[sli_no], 0) as 'return_lineitem_no'
       ,IsNull(slr.[due_amt], 0.00) as 'return_due_amount'
       ,IsNull(slr.[paid_amt], 0.00) as 'return_paid_amount'
       ,IsNull(slr.[sli_status], '') as 'return_status_no'
       ,rst.[description] as 'return_status'
       ,CASE WHEN slr.[sli_no] is null THEN 1 ELSE 0 END as 'Quantity'
       ,prf.[performance_no]
       ,prf.[title_no]
       ,prf.[title_name]
       ,prf.[performance_dt] as 'course_dt'
       ,prf.[performance_date] as 'course_date'
       ,prf.[performance_time] as 'course_time'
       ,prf.[performance_time_display] as 'course_time_display'
       ,prf.[production_name] as 'course_title'
       ,prf.[production_name_long] as 'course_title_long'
       ,IsNull(sli.[recipient_no], 0) as 'participant_customer_no'
       ,ISNull(rcp.[fname], '') as 'participant_first_name'
       ,IsNull(rcp.[mname], '') as 'participant_middle_name'
       ,IsNull(rcp.[lname], '') as 'participant_last_name'
       ,[dbo].[LFS_HasEarlyDropOffOrLateStay](prf.[performance_dt], IsNull(sli.[recipient_no], 0), 'Early') AS 'Early Drop-Off'
       ,[dbo].[LFS_HasEarlyDropOffOrLateStay](prf.[performance_dt], IsNull(sli.[recipient_no], 0), 'Late') AS 'Late Stay'
FROM [dbo].[T_SUB_LINEITEM] as sli (NOLOCK)
     LEFT OUTER JOIN [dbo].[T_SUB_LINEITEM] as slr (NOLOCK) ON slr.[ret_parent_sli_no] = sli.[sli_no]
     LEFT OUTER JOIN [dbo].[TR_SLI_STATUS] as sst (NOLOCK) ON sst.[id] = sli.[sli_status]
     LEFT OUTER JOIN [dbo].[TR_SLI_STATUS] as rst (NOLOCK) ON rst.[id] = slr.[sli_status]
     LEFT OUTER JOIN [dbo].[T_ORDER] As ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
     LEFT OUTER JOIN [dbo].[T_CUSTOMER] as cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
     LEFT OUTER JOIN [dbo].[T_ADDRESS] as adr (NOLOCK) ON adr.[customer_no] = ord.[customer_no] and adr.[primary_ind] = 'Y'
     LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] as aty ON aty.[id] = adr.[address_type]
     LEFT OUTER JOIN [dbo].[T_PHONE] as hph (NOLOCK) ON hph.[customer_no] = ord.[customer_no] and hph.[type] = 8
     LEFT OUTER JOIN [dbo].[T_PHONE] as bph (NOLOCK) ON bph.[customer_no] = ord.[customer_no] and bph.[type] = 14
     LEFT OUTER JOIN [dbo].[T_EADDRESS] as eml (NOLOCK) ON eml.[customer_no] = ord.[customer_no] and eml.[primary_ind] = 'Y'
     LEFT OUTER JOIN [dbo].[TR_EADDRESS_TYPE] as ety (NOLOCK) ON ety.[id] = eml.[eaddress_type]
     LEFT OUTER JOIN [dbo].[T_CUSTOMER] as rcp (NOLOCK) ON rcp.[customer_no] = sli.[recipient_no]
     LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] as prf (NOLOCK) ON prf.[performance_no] = sli.[perf_no] AND prf.performance_zone = sli.[zone_no]
WHERE prf.[title_name] = 'Summer Courses'
GO
*/

