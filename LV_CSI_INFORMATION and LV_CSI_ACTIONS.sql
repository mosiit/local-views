USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_CSI_ACTIONS]'))
    DROP VIEW [dbo].[LV_CSI_ACTIONS]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_CSI_INFORMATION]'))
    DROP VIEW [dbo].[LV_CSI_INFORMATION]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_CSI_INFORMATION] AS
SELECT  act.[activity_no]
       ,act.[origin] as 'origin_no'
       ,ori.[description] as 'origin_name'
       ,act.[category] as 'category_no'
       ,cat.[description] as 'category_name'
       ,act.[activity_type] as 'activity_type_no'
       ,typ.[description] as 'activity_type_name'
       ,act.[contact_type] as 'contact_type_no'
       ,con.[description] as 'contact_type_name'
       ,act.[customer_no]
       ,act.[inout_ind]
       ,act.[doc_no]
       ,act.[notes]
       ,act.[issue_dt]
       ,act.[urg_ind]
       ,act.[last_updated_by]
       ,act.[last_update_dt]
       ,act.[perf_no]
FROM T_CUST_ACTIVITY as act
     LEFT OUTER JOIN [dbo].[TR_CUST_ACTIVITY_CATEGORY] as cat ON cat.[id] = act.[category]
     LEFT OUTER JOIN [dbo].[TR_CUST_ACTIVITY_TYPE] as typ ON typ.[id] = act.[activity_type]
     LEFT OUTER JOIN [dbo].[TR_CONTACT_TYPE] as con on con.[id] = act.[contact_type]
     LEFT OUTER JOIN [dbo].[TR_ORIGIN] as ori on ori.[id] = act.[origin]
GO

GRANT SELECT ON [dbo].[LV_CSI_INFORMATION] TO impusers
GO

CREATE VIEW [dbo].[LV_CSI_ACTIONS] AS
SELECT  csi.[activity_no]
       ,csi.[origin_no]
       ,csi.[origin_name]
       ,csi.[category_no]
       ,csi.[category_name]
       ,csi.[activity_type_no]
       ,csi.[activity_type_name]
       ,csi.[contact_type_no]
       ,csi.[contact_type_name]
       ,csi.[customer_no]
       ,csi.[inout_ind]
       ,csi.[doc_no]
       ,IsNull(iac.[IsAc_no], 0) as 'issue_action_no'
       ,IsNull(iac.[action_dt], csi.[issue_dt]) as 'action_dt'
       ,IsNull(iac.[action], 0) as 'action_no'
       ,IsNull(act.[description], '') as 'action_name'
       ,IsNull(iac.[res_ind], 'N') as 'is_resolved'
       ,csi.[notes] as 'csi_notes'
       ,IsNull(iac.[notes], '') as 'action_notes'
       ,csi.[issue_dt]
       ,csi.[urg_ind]
       ,csi.[last_updated_by]
       ,csi.[last_update_dt]
       ,csi.[perf_no]
FROM [dbo].[LV_CSI_INFORMATION] as csi
     LEFT OUTER JOIN [dbo].[T_ISSUE_ACTION] as iac ON iac.[activity_no] = csi.[activity_no]
     LEFT OUTER JOIN [dbo].[TR_ACTION] AS act ON act.[id] = iac.[action]
GO

GRANT SELECT ON [dbo].[LV_CSI_INFORMATION] TO impusers
GO



