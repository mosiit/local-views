USE [impresario];
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[LV_CURRENT_ENTITLEMENTS]
AS
SELECT DISTINCT
	a.cust_memb_no, 
    a.customer_no,
    a.entitlement_no,
    b.reset_type,
    b.ent_tkw_id,
    b.entitlement_desc,
    a.num_ent,
    SUM(c.num_used) OVER (PARTITION BY c.entitlement_no, c.cust_memb_no) AS num_used
FROM dbo.LTX_CUST_ENTITLEMENT AS a
INNER JOIN dbo.LTR_ENTITLEMENT AS b
    ON a.entitlement_no = b.entitlement_no
LEFT OUTER JOIN dbo.LTX_CUST_ORDER_ENTITLEMENT AS c
    ON a.customer_no = c.customer_no
       AND a.cust_memb_no = c.cust_memb_no
       AND a.entitlement_no = c.entitlement_no
WHERE (a.num_ent > 0)
      AND (b.reset_type = 'M')
      AND a.cust_memb_no IN (
                                SELECT cust_memb_no FROM TX_CUST_MEMBERSHIP WHERE current_status IN (2, 3) AND declined_ind = 'N'
                            );


GO

