USE [impresario]
GO

/****** Object:  View [dbo].[LV_CURRENT_ENTITLEMENTS_ONLY]    Script Date: 4/30/2018 6:29:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[LV_CURRENT_ENTITLEMENTS_ONLY]
AS

/*
Modified by CCastilla/TN 4/23/2018
Now pulls data for primary affiliates also. Resolution for Zendesk ticket 12660. Added join on V_CUSTOMER_WITH_PRIMARY_GROUP and selecting customer numbers from there.
Also changed sum of num_used so it only counts when looking at the household record.
*/

SELECT	DISTINCT /*a.customer_no*/ d.customer_no, a.entitlement_no, b.reset_type, b.ent_tkw_id, b.entitlement_desc, a.num_ent, 
		--sum(c.num_used) OVER (partition BY c.entitlement_no, c.cust_memb_no) AS num_used	
		sum(case when d.customer_no = d.expanded_customer_no then c.num_used else 0 end) OVER (partition BY c.entitlement_no, c.cust_memb_no) AS num_used, 
		d.expanded_customer_no	
FROM    dbo.LTX_CUST_ENTITLEMENT AS a INNER JOIN
        dbo.LTR_ENTITLEMENT AS b ON a.entitlement_no = b.entitlement_no LEFT OUTER JOIN
        dbo.LTX_CUST_ORDER_ENTITLEMENT AS c ON a.customer_no = c.customer_no AND a.cust_memb_no = c.cust_memb_no AND a.entitlement_no = c.entitlement_no	
		join dbo.V_CUSTOMER_WITH_PRIMARY_GROUP d  ON a.customer_no = d.expanded_customer_no  --CCastilla/TN 4/23/2018
WHERE   (a.num_ent > 0) AND (b.reset_type = 'M') AND a.cust_memb_no IN
                    (
					SELECT	cust_memb_no
                    FROM	tx_cust_membership
                    WHERE	current_status = 2
					)

GO
