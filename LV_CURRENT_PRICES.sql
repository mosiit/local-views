USE impresario
GO

/*
    View that gives the up-to-date price for a given price type in a specific performance.
    NOTE: This view is dependent on there being only one active price event on any given product's price at a time.

*/


IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_CURRENT_PRICES]'))
    DROP VIEW [dbo].[LV_CURRENT_PRICES]
GO

CREATE VIEW [dbo].[LV_CURRENT_PRICES] AS
SELECT   ppt.[id] as 'perf_price_type_no'
        ,ppr.[id] as 'perf_price_no'
        ,ppt.[perf_no]
        ,ppr.[zone_no]
        ,ppt.[perf_price_layer]
        ,ppt.[price_type] as 'price_type_no'
        ,pty.[description] as 'price_type_name'   
        ,IsNull(pev.[price], ppr.[start_price]) as 'current_price'
        ,ISNull(pev.[min_price], ppr.[start_min_price]) as 'current_min_price'
        ,ppr.[edit_ind]
        ,ppt.[gl_no]
        ,ppt.[gl_resale_no]
        ,IsNull(pev.[start_dt],ppt.[start_dt]) as 'price_start_date'
        ,ppt.[end_dt] as 'price_end_date'
FROM [dbo].[T_PERF_PRICE_TYPE] as ppt (NOLOCK)
     LEFT OUTER JOIN [dbo].[T_PERF_PRICE] as ppr (NOLOCK) ON ppr.[perf_price_type] = ppt.[id] and ppr.[start_enabled] = 'Y'
     LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] as pty (NOLOCK) ON pty.[id] = ppt.[price_type]
     LEFT OUTER JOIN T_PRICE_EVENT as pev (NOLOCK) ON pev.[perf_price_no] = ppr.[id] and pev.[enabled] = 'Y' and getdate() > pev.[start_dt] 
GO

GRANT SELECT ON [dbo].[LV_CURRENT_PRICES] TO impusers
GO

