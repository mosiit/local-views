USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_CUSTOMER_WITH_ALL_ADULT_AFFILIATIONS] 
AS

-- THIS IS COPIED FROM [LV_CUSTOMER_WITH_PRIMARY_AFFILIATES], WITH THE PRIMARY_IND = 'Y' CLAUSE REMOVED
-- ADV CALL SHEETS NEEDED TO SEE DECEASED (NOT PRIMARY RECORDS) AFFLIATES ALSO

SELECT	x. customer_no, y.cust_type, x.expanded_customer_no, y.inactive, x.name_ind
FROM
	(SELECT	b.group_customer_no AS customer_no,  b.individual_customer_no AS expanded_customer_no, b.name_ind
	FROM	dbo.T_AFFILIATION b 
	WHERE	b.affiliation_type_id = 10002 AND b.inactive = 'N' -- Adult Member
	UNION ALL
	SELECT	b.individual_customer_no AS customer_no, b.group_customer_no AS expanded_customer_no, b.name_ind
	FROM	dbo.T_AFFILIATION b 
	WHERE	b.affiliation_type_id = 10002 AND b.inactive = 'N' -- Adult Member
	UNION ALL
	SELECT	a.customer_no AS customer_no, a.customer_no AS expanded_customer_no, 0 AS name_ind
	FROM	dbo.T_CUSTOMER a) AS x 
	
	JOIN dbo.T_CUSTOMER y ON x.customer_no = y.customer_no

GO


