USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_CUSTOMER_WITH_HOUSEHOLD_INFO]'))
    DROP VIEW [dbo].[LV_CUSTOMER_WITH_HOUSEHOLD_INFO]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*  View that lists all non-household constituents and the household they are attached to (if any)
    Household number is 0 and household name is blank on any record not associated with a household.
    The address comes from the FT_GET_PRIMARY_ADDRESS table function and should be the household
    address when there is a household or the individual's address when there is no household.  */

CREATE VIEW [LV_CUSTOMER_WITH_HOUSEHOLD_INFO] AS
    WITH CTE_HOUSEHOLD_LINK ([row_num], [customer_no], [household_no])
    AS  (
         SELECT ROW_NUMBER() 
                    OVER(PARTITION BY cus.[customer_no] ORDER BY hou.[customer_no] DESC) AS Row_num,
                cus.[customer_no],
                ISNULL(hou.[customer_no],0) AS [household_no]
         FROM [dbo].[T_CUSTOMER] AS cus
              INNER JOIN [dbo].[TR_CUST_TYPE] AS typ ON typ.[id] = cus.[cust_type]
              LEFT OUTER JOIN [dbo].[T_AFFILIATION] AS aff ON aff.[individual_customer_no] = cus.[customer_no]
              LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS hou ON hou.[customer_no] = aff.[group_customer_no] AND hou.[cust_type] = 7
         WHERE cus.[cust_type] <> 7
        )
    SELECT hou.[customer_no] AS [household_no],
           hou.[lname] AS [household_name],
           CASE WHEN ISNULL(hou.[customer_no],0) > 0 THEN 'Y' ELSE 'N' END AS [has_household],
           cus.[customer_no],
           ISNULL(cus.[fname],'') AS [customer_first_name],
           ISNULL(cus.[mname],'') AS [customer_middle_name],
           ISNULL(cus.[lname],'') AS [customer_last_name],
           ISNULL(nam.[display_name],'') AS [customer_full_name],
           ISNULL(nam.[sort_name],'') AS [customer_sort_name],
           ISNULL(cus.[cust_type],0) AS [customer_type],
           ISNULL(typ.[description],'') AS [customer_type_name],
           ISNULL(adr.[street1],'') AS [street1],
           ISNULL(adr.[street2],'') AS [street2],
           ISNULL(adr.[street3],'') AS [street3],
           ISNULL(adr.[city],'') AS [city],
           ISNULL(adr.[state],'') AS [state],
           CASE WHEN LEN(ISNULL(adr.[postal_code],'')) = 9 THEN
                     LEFT(ISNULL(adr.[postal_code],''),5) + '-' + RIGHT(ISNULL(adr.[postal_code],''),4)
                ELSE ISNULL(adr.[postal_code],'') END AS [postal_code]
FROM [dbo].[T_CUSTOMER] AS cus (NOLOCK)
      INNER JOIN [dbo].[TR_CUST_TYPE] AS typ (NOLOCK) ON typ.[id] = cus.[cust_type]
      INNER JOIN [CTE_HOUSEHOLD_LINK] AS lnk (NOLOCK) ON lnk.[customer_no] = cus.[customer_no] AND lnk.[row_num] = 1
      LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS hou (NOLOCK) ON hou.[customer_no] = lnk.[household_no]
      LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = cus.[customer_no]
      LEFT OUTER JOIN [dbo].[FT_GET_PRIMARY_ADDRESS]() AS adr ON adr.[customer_no] = cus.[customer_no]
WHERE cus.[cust_type] <> 7  --Not = Household
GO

GRANT SELECT ON [dbo].[LV_CUSTOMER_WITH_HOUSEHOLD_INFO] TO ImpUsers
GO
