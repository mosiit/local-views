USE [impresario]
GO

/****** Object:  View [dbo].[LV_CUST_ENTITLEMENTS]    Script Date: 9/22/2017 10:13:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_CUST_ENTITLEMENTS]

AS

/* Usage:
SELECT * FROM LV_CUST_ENTITLEMENTS where customer_no = 1252496
SELECT * FROM LV_CUST_ENTITLEMENTS where customer_no = 1252501

Modified 6/22/2015 inactive status should not be granted entitlements!
modified 6/29/2015 subtracting usage correctly for multi-/pendings 
Modified 2/25/2016 Lapsed and Decline benefits
*/

SELECT
	customer_no = cm.customer_no,
	keyword_id = kw.id,
	entitlement_keyword_desc = kw.description,
	price_type_group = pg.id,
	price_type_group_desc = pg.description,
	e.reset_type,
	reset_type_desc = gd.description,
	num_start_ent = ISNULL(MAX(CASE WHEN cm.current_status = 3 AND e.reset_type = 'M' THEN COALESCE(ce.num_ent,e.num_ent) END),0)
					+ ISNULL(MAX(CASE WHEN (cm.current_status = 3 AND e.reset_type <> 'M') OR cm.current_status = 2 THEN COALESCE(ce.num_ent,e.num_ent) END),0),
	num_remain_ent = 
					(ISNULL(MAX(CASE WHEN cm.current_status = 3 AND e.reset_type = 'M' THEN COALESCE(ce.num_ent,e.num_ent) END),0)
					+ ISNULL(MAX(CASE WHEN (cm.current_status = 3 AND e.reset_type <> 'M') OR cm.current_status = 2 THEN COALESCE(ce.num_ent,e.num_ent) END),0))

					/* -  ISNULL(MAX(coe.cust_num_used),0) */
					-  (
					ISNULL(MAX(CASE WHEN cm.current_status = 3 AND e.reset_type = 'M' THEN coe.cust_num_used END),0)
					+ ISNULL(MAX(CASE WHEN (cm.current_status = 3 AND e.reset_type <> 'M') OR cm.current_status = 2 THEN coe.cust_num_used END),0)
					)

FROM dbo.LTR_ENTITLEMENT e
	JOIN dbo.TR_TKW kw ON e.ent_tkw_id = kw.id
	JOIN dbo.T_MEMB_LEVEL ml ON e.memb_level_no = ml.memb_level_no
	JOIN dbo.T_MEMB_ORG mo ON mo.memb_org_no = ml.memb_org_no
	JOIN dbo.TX_CUST_MEMBERSHIP cm ON (ml.memb_org_no = cm.memb_org_no AND ml.memb_level = cm.memb_level)
	JOIN dbo.TR_PRICE_TYPE_GROUP pg ON pg.id = e.ent_price_type_group
	JOIN dbo.TR_GOOESOFT_DROPDOWN gd ON (gd.code = 1002 AND gd.short_desc = e.reset_type)
	LEFT JOIN dbo.LTX_CUST_ENTITLEMENT ce 
		ON (e.entitlement_no = ce.entitlement_no 
		AND ce.customer_no = cm.customer_no
		AND ce.cust_memb_no = cm.cust_memb_no
		AND (e.reset_type = 'M'
			OR 
			(e.reset_type = 'D' AND CONVERT(VARCHAR(10),CURRENT_TIMESTAMP,101) = ce.entitlement_date)
			)
		)
	LEFT JOIN (
		SELECT
			customer_no,
			cust_memb_no,
			entitlement_no,
			entitlement_date,
			cust_num_used = ISNULL(SUM(num_used),0)
		FROM dbo.LTX_CUST_ORDER_ENTITLEMENT e1
		GROUP BY customer_no, cust_memb_no, entitlement_no, entitlement_date) coe
	ON (coe.customer_no = cm.customer_no 
		AND coe.cust_memb_no = cm.cust_memb_no
		AND coe.entitlement_no= e.entitlement_no
		AND (
			e.reset_type = 'M'
			OR
			(e.reset_type = 'D' AND CONVERT(VARCHAR(10),CURRENT_TIMESTAMP,101) = coe.entitlement_date)
			)
		)
/*WHERE (
	(cm.current_status in (2,3) and CURRENT_TIMESTAMP BETWEEN cm.init_dt AND cm.expr_dt)
	OR (cm.current_status = 3 AND CURRENT_TIMESTAMP BETWEEN DATEADD(MONTH,0-ml.renewal,cm.init_dt) AND cm.expr_dt) --or pending 
	)*/
WHERE ( 
		   (cm.current_status = 2 
			AND CURRENT_TIMESTAMP BETWEEN CAST(CONVERT(varchar(10),cm.init_dt,101) as datetime) AND CAST(CONVERT(varchar(10),cm.expr_dt,101) as datetime) + '23:59:59')
		OR (cm.current_status = 3
			AND CURRENT_TIMESTAMP BETWEEN DATEADD(MONTH,0-ml.renewal,CAST(CONVERT(varchar(10),cm.init_dt,101) as datetime)) AND CAST(CONVERT(varchar(10),cm.expr_dt,101) as datetime) + '23:59:59')
		OR (cm.current_status = 7 AND ISNULL(e.allow_lapsed,'Y') = 'Y'
			AND  CURRENT_TIMESTAMP BETWEEN CAST(CONVERT(varchar(10),cm.init_dt,101) as datetime) AND dbo.FS_ADD_MONTHS(CAST(CONVERT(varchar(10),cm.expr_dt,101) as datetime) + '23:59:59',ISNULL(ml.lapse_susp,0)))
	)
/* Do not grant if membership benefits are declined, and a declinable benefit  */
	AND NOT (
		ISNULL(cm.declined_ind,'N') = 'Y'
		AND 
		ISNULL(e.decline_benefit,'Y') = 'Y')
GROUP BY cm.customer_no, 
	kw.id,
	kw.description,
	pg.id,
	pg.description,
	e.reset_type,
	gd.description


GO


