

    ALTER VIEW [dbo].[LV_EMAIL_CPPS] AS
    WITH CTE_CONTACT_POINTS AS (SELECT [contact_point_id], [1] AS [BillsCPP], [2] AS [BoardCPP], [7] AS [PersonalCPP], [8] AS [PersonalCFGCPP], [10] AS [newsCPP], 
                                       [12] AS [supportCPP], [15] AS [dnpHCPP], [24] AS [CorpCPP], [25] AS [EduCPP], [26] AS [AdultCPP], [28] AS [MediaCPP], 
                                       [29] AS [TPPCPP], [30] AS [MOSatHomeCPP], [31] AS [MOSenEspanolCPP]
                                      FROM (SELECT [contact_point_id], [purpose_id] 
                                             FROM [dbo].[TX_CONTACT_POINT_PURPOSE] AS cpx) p  
                                PIVOT (COUNT ([purpose_id])   
                                FOR [purpose_id] IN ( [1], [2], [7], [8], [10], [12], [15], [24], [25], [26], [28], [29], [30], [31] )) AS pvt)
    SELECT a.[customer_no], 
            a.[primary_ind], 
            a.[eaddress_no],
            t.[description] AS [eaddress_type],
            REPLACE(REPLACE(a.[address],CHAR(10),''),CHAR(13),'') AS [address],
            10 AS [newsCPPflag], 
            ISNULL(c.[newsCPP], 0) AS [newsCPP],
            CASE ISNULL(c.[newsCPP], 0) WHEN 0 THEN 'N' ELSE 'Y' END  AS [newsCPP_ind],
            12 AS [supportCPPflag], 
            ISNULL(c.[supportCPP], 0) AS [supportCPP],
            CASE ISNULL(c.[supportCPP], 0) WHEN 0 THEN 'N' ELSE 'Y' END AS [supportCPP_ind],
            25 AS [EduCPPflag],
            ISNULL([EduCPP], 0) AS [EduCPP],
            CASE ISNULL([EduCPP], 0) WHEN 0 THEN 'N' ELSE 'Y' END AS [EduCPP_ind],
            26 AS [AdultCPPflag],
            ISNULL(c.[AdultCPP], 0) AS [AdultCPP],
            CASE ISNULL(c.[AdultCPP], 0) WHEN 0 THEN 'N' ELSE 'Y' END AS [AdultCPP_ind],
            30 [MOSatHomeCPPflag],
            ISNULL(c.[MOSatHomeCPP], 0) AS [MOSatHomeCPP],
            CASE ISNULL(c.[MOSatHomeCPP], 0) WHEN 0 THEN 'N' ELSE 'Y' END AS [MOSatHomeCPP_ind],
            31 AS [MOSenEspanolCPPflag], 
            ISNULL(c.[MOSenEspanolCPP], 0) AS [MOSenEspanolCPP],
            CASE ISNULL(c.[MOSenEspanolCPP], 0) WHEN 0 THEN 'N' ELSE 'Y' END AS [MOSenEspanolCPP_ind],
            1 AS [BillsCPPflag],
            ISNULL(c.[BillsCPP], 0) AS [BillsCPP],
            CASE ISNULL(c.[BillsCPP], 0) WHEN 0 THEN 'N' ELSE 'Y' END AS [BillsCPP_ind],
            2 AS [BoardCPPflag],
            ISNULL(c.[BoardCPP], 0) AS [BoardCPP],
            CASE ISNULL(c.[BoardCPP], 0) WHEN 0 THEN 'N' ELSE 'Y' END AS [BoardCPP_ind],
            15 [dnpHCPPflag],
            ISNULL(c.[dnpHCPP], 0) AS [dnpHCPP],
            CASE ISNULL(c.[dnpHCPP], 0) WHEN 0 THEN 'N' ELSE 'Y' END AS [dnpHCPP_ind],
            24 [CorpCPPflag], 
            ISNULL(c.[CorpCPP], 0) AS [CorpCPP],
            CASE ISNULL(c.[CorpCPP], 0) WHEN 0 THEN 'N' ELSE 'Y' END AS [CorpCPP_ind],
            28 AS [MediaCPPflag],
            ISNULL(c.[MediaCPP], 0) AS [MediaCPP],
            CASE ISNULL(c.[MediaCPP], 0) WHEN 0 THEN 'N' ELSE 'Y' END AS [MediaCPP_ind],
            29 AS [TPPCPPflag],
            ISNULL(c.[TPPCPP], 0) AS [TPPCPP],
            CASE ISNULL(c.[TPPCPP], 0) WHEN 0 THEN 'N' ELSE 'Y' END AS [TPPCPP_ind],
            7 AS [PersonalCPPflag],
            ISNULL(c.[PersonalCPP], 0) AS [PersonalCPP],
            CASE ISNULL(c.[PersonalCPP], 0) WHEN 0 THEN 'N' ELSE 'Y' END AS [PersonalCPP_ind],
            8 AS [PersonalCFGCPPflag],
            ISNULL(c.[PersonalCFGCPP], 0) AS [PersonalCFGCPP],
            CASE ISNULL(c.[PersonalCFGCPP], 0) WHEN 0 THEN 'N' ELSE 'Y' END AS [PersonalCFGCPP_ind]
    FROM T_EADDRESS (NOLOCK) a 
         LEFT OUTER JOIN [CTE_CONTACT_POINTS] AS c ON c.[contact_point_id] = a.[eaddress_no]
         INNER JOIN [dbo].[TR_EADDRESS_TYPE] AS t ON t.[id] = a.[eaddress_type]
    WHERE a.[eaddress_type] IN (1, 2, 3, 4, 5, 17) 
      AND a.[inactive] = 'N' 
      AND a.[market_ind] ='Y'
GO

GRANT SELECT ON [dbo].[LV_EMAIL_CPPS] TO [ImpUsers], [tessitura_app]
GO

--SELECT * FROM [dbo].[LV_EMAIL_CPPS]

  --SELECT * 
  --FROM [dbo].[TX_CONTACT_POINT_PURPOSE] (NOLOCK)
  --WHERE [purpose_id] IN (1, 2, 7, 8, 10, 12, 15, 24, 25, 26, 28, 29, 30, 31)

