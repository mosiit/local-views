USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_EMPLOYER_MAIN]
AS

SELECT c.search_customer_no AS customer_no, c.type, a.title, best.best_name AS employer
FROM LV_A1_AND_INDIVIDUAL c
INNER JOIN T_AFFILIATION AS a
	ON a.individual_customer_no = c.join_customer_no
	AND a.affiliation_type_id = 10007 -- Employee_Main
INNER JOIN LV_ADV_BEST_NAME AS best
	ON a.group_customer_no = best.customer_no 
GO

GRANT SELECT ON  [dbo].[LV_EMPLOYER_MAIN] to impusers
GO