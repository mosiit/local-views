USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_EMP_INDUSTRY_MAIN]
AS

SELECT c.search_customer_no AS customer_no, k.key_value
FROM LV_A1_AND_INDIVIDUAL c
INNER JOIN T_AFFILIATION AS a
	ON a.individual_customer_no = c.join_customer_no
	AND a.affiliation_type_id = 10007 -- Employee_Main
INNER JOIN TX_CUST_KEYWORD AS k  
	ON a.group_customer_no = k.customer_no 
	AND k.keyword_no = 421 -- Industry
GO

GRANT SELECT ON  [dbo].[LV_EMP_INDUSTRY_MAIN] to impusers
GO
