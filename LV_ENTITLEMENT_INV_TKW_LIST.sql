USE [impresario]
GO

/****** Object:  View [dbo].[LV_ENTITLEMENT_INV_TKW_LIST]    Script Date: 9/22/2017 10:12:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER View [dbo].[LV_ENTITLEMENT_INV_TKW_LIST]
AS

/*********************************************************************************************************
DISTINCT usage of V_INV_TKW_LIST (perf + tkw)
for keyword listings with Entitlements procedures
*********************************************************************************************************/

select	DISTINCT 
	perf_no, tkw
FROM V_INV_TKW_LIST


GO


