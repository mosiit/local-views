USE impresario
GO

    IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_ENTITLEMENT_PRODUCTIONS]'))
        DROP VIEW [dbo].[LV_ENTITLEMENT_PRODUCTIONS]
GO
    
    CREATE VIEW [dbo].[LV_ENTITLEMENT_PRODUCTIONS] AS
    SELECT DISTINCT prf.[production_no], 
                    prf.[production_name]
    FROM [LTX_CUST_ORDER_ENTITLEMENT] AS ent
         INNER JOIN [dbo].[T_SUB_LINEITEM] AS sli ON sli.[sli_no] = ent.[sli_no]
         INNER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] 
                                                                     AND prf.[performance_zone] = sli.[zone_no]
GO

    GRANT SELECT ON [dbo].[LV_ENTITLEMENT_PRODUCTIONS] TO ImpUsers

GO

   --SELECT * FROM dbo.LV_ENTITLEMENT_PRODUCTIONS ORDER BY production_name

