USE impresario;
GO

    IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_GIFT_ENTITLEMENTS]'))
        DROP VIEW [dbo].[LV_GIFT_ENTITLEMENTS];
GO

SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;
GO


CREATE VIEW [dbo].[LV_GIFT_ENTITLEMENTS] AS
    WITH [CTE_ENTITLEMENT_SCANS] ([scan_string], [scan_dt], [scan_count])
    AS (SELECT nsc.scan_str, MIN(CAST(nsc.[update_dt] AS DATE)), COUNT(*)
        FROM [dbo].[T_NSCAN_EVENT_CONTROL] AS nsc 
        INNER JOIN [dbo].[LT_ENTITLEMENT_GIFT] AS gft ON gft.[gift_code] = nsc.[scan_str] 
                                                     AND nsc.[ticket_ok] = 'Y'
        GROUP BY nsc.[scan_str])
    SELECT gft.[id_key] AS [gift_id],
           gft.[customer_no] AS [giver_no],
           nmg.[display_name] AS [giver_name],
           nmg.[sort_name] AS [giver_name_sort],
           gft.[gift_customer_no] AS [recipient_no],
           CASE WHEN gft.[customer_no] = gft.[gift_customer_no] THEN CONVERT(VARCHAR(75),ISNULL(gft.[gift_recip_name],''))
                ELSE CONVERT(VARCHAR(75),ISNULL(nmr.[display_name],ISNULL(gft.[gift_recip_name],''))) END AS [recipient_name],
           CASE WHEN gft.[customer_no] = gft.[gift_customer_no] THEN CONVERT(VARCHAR(75),ISNULL(gft.[gift_recip_name],''))
                ELSE CONVERT(VARCHAR(75),ISNULL(nmr.[sort_name],ISNULL(gft.[gift_recip_name],''))) END AS [recipient_name_sort],
           ISNULL(gft.[gift_recip_email],'') AS [recipient_email],
           CAST(gft.[gift_code] AS VARCHAR(25)) AS [gift_code],
           CAST(gft.[group_gift_code] AS VARCHAR(25)) AS [group_gift_code],
           gft.[create_dt],
           gft.[init_dt],
           gft.[expr_dt],
           gft.[ltx_cust_entitlement_id],
           gft.[entitlement_no],
           ent.[entitlement_code],
           ent.[ent_price_type_group],
           grp.[description] AS [price_type_group_name],
           ent.[ent_tkw_id],
           tkw.[description] AS [keyword],
           CAST(ent.[entitlement_desc] AS VARCHAR(100)) AS [entitlement_desc],
           CASE WHEN ISNULL(ord.pass_status,'') = '' 
                  OR ISNULL(eord.num_used,0) = 0 THEN gft.[num_items]
                ELSE eord.[num_used] END AS [num_items],
           gft.[gift_status],
           CASE gft.[gift_status]
                WHEN 'C' THEN 'Created'
                WHEN 'A' THEN 'Accepted'
                ELSE gft.[gift_status] END AS [gift_status_name],
           CASE WHEN ISNULL(eord.num_used,0) > 0 THEN ISNULL(eord.num_used,0)
                WHEN ISNULL(eord.num_used,0) <= 0 THEN ISNULL(gft.[num_items],0)
                ELSE 1 END AS [counter_all],
           CASE WHEN gft.[gift_status] = 'C' AND ISNULL(eord.num_used,0) > 0 THEN ISNULL(eord.num_used,0)
                WHEN gft.[gift_status] = 'C' AND ISNULL(eord.num_used,0) <= 0 THEN ISNULL(gft.[num_items],0)
                ELSE 0 END AS [counter_created],
           CASE WHEN gft.[gift_status] = 'A' AND ISNULL(eord.num_used,0) > 0 THEN ISNULL(eord.num_used,0)
                WHEN gft.[gift_status] = 'A' AND ISNULL(eord.num_used,0) <= 0 THEN ISNULL(gft.[num_items],0)
                ELSE 0 END AS [counter_accepted],
           CASE WHEN gft.[gift_status] NOT IN ('C','A') AND ISNULL(eord.num_used,0) > 0 THEN ISNULL(eord.num_used,0)
                WHEN gft.[gift_status] NOT IN ('C','A') AND ISNULL(eord.num_used,0) <= 0 THEN ISNULL(gft.[num_items],0)
                ELSE 0 END AS [counter_other],
          CASE WHEN gft.[customer_no] = gft.[gift_customer_no] 
                 AND gft.[customer_no] = oin.[customer_no] 
                 AND sli.[created_by] = 'WebAPI' 
                 AND prf.title_no = 27 
                 AND ISNULL(eord.num_used,0) > 0 THEN ISNULL(eord.num_used,0)
                WHEN gft.[customer_no] = gft.[gift_customer_no] 
                 AND gft.[customer_no] = oin.[customer_no] 
                 AND sli.[created_by] = 'WebAPI' 
                 AND prf.title_no = 27 
                 AND ISNULL(eord.num_used,0) <= 0 THEN ISNULL(gft.[num_items],0)
                ELSE 0 END AS [counter_return_to_giver],
           ISNULL(ent1.set_no,0) AS [set_no],
           ISNULL(est.[name],'No Set') AS [set_name],
           ISNULL(eord.order_no,0) AS [order_no],
           ISNULL(oin.[customer_no],0) AS [order_customer_no],
           ISNULL(sli.[created_by],'') AS [order_created_by],
           ISNULL(sli.[create_loc],'') AS [order_create_loc],
           ISNULL(oin.[source_no],0) AS [order_source_no],
           ISNULL(med.[source_name],'') AS [order_source],
           ISNULL(oin.[mos],0) AS [order_mode_of_sale_no],
           ISNULL(mos.[description],'') AS [order_mode_of_sale],                      
           ISNULL(eord.sli_no,0) AS [sli_no],
           ISNULL(eord.perf_no,0) AS [perf_no],
           ISNULL(prf.[performance_no],0) AS [performance_no],
           ISNULL(prf.[performance_zone],0) AS [performance_zone],
           ISNULL(prf.[performance_dt],scn.[scan_dt]) AS [performance_dt],
           ISNULL(prf.[performance_date],'') AS [performance_date],
           ISNULL(prf.[performance_time],'') AS [performance_time],
           ISNULL(prf.[performance_time_display],'') AS [performance_time_display],
           ISNULL(prf.[title_no],0) AS [title_no],
           ISNULL(prf.[title_name],'') AS [title_name],
           ISNULL(prf.[production_no],0) AS [production_no],
           CASE WHEN ISNULL(prf.[production_name],'') = '' AND ISNULL(scn.[scan_count],0) = 0 THEN 'Not Used'
                WHEN ISNULL(prf.[production_name],'') = '' AND ISNULL(scn.[scan_count],0) <> 0 THEN 'Gate Scan'
                ELSE ISNULL(prf.[production_name],'') END AS [production_name],
           CASE WHEN ISNULL(ord.pass_status,'') = '' AND ISNULL(scn.[scan_count], 0) = 0 THEN 0
                WHEN ISNULL(ord.pass_status,'') = '' AND ISNULL(scn.[scan_count], 0) <> 0 THEN scn.[scan_count]
                ELSE ISNULL(eord.num_used, 0) END AS [num_used],
           CASE WHEN ISNULL(ord.pass_status,'') = '' AND ISNULL(scn.[scan_count],0) = 0 THEN 'Not Used'
                WHEN ISNULL(ord.pass_status,'') = '' AND ISNULL(scn.[scan_count],0) <> 0 THEN 'Used'
                ELSE ISNULL(ord.[pass_status],'Not Used') END AS [pass_status],
           --ISNULL(ord.pass_status,'Not Used') AS [pass_status],
           CASE WHEN ISNULL(ord.[price_type_desc],'') = '' AND ISNULL(scn.[scan_count],0) = 0 THEN 'Not Used'
                WHEN ISNULL(ord.[price_type_desc],'') = '' AND ISNULL(scn.[scan_count],0) <> 0 THEN 'Gate Scan'
                ELSE ISNULL(ord.[price_type_desc],'Not Used') END AS [price_type_desc],
           --ISNULL(ord.price_type_desc,'Not Used') AS [price_type_desc],
           ISNULL(ord.short_desc,'') AS [shord_desc],
           ISNULL(scn.[scan_count],0) AS [scan_count]
    FROM [dbo].[LT_ENTITLEMENT_GIFT] AS gft (NOLOCK)
         LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nmg ON nmg.[customer_no] = gft.[customer_no]          --display name for the giver
         LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nmr ON nmr.[customer_no] = gft.[gift_customer_no]     --display name for the recipient
         LEFT OUTER JOIN [dbo].[LTR_ENTITLEMENT] AS ent (NOLOCK) ON ent.[entitlement_no] = gft.[entitlement_no]
         LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE_GROUP] AS grp ON grp.[id] = ent.[ent_price_type_group]
         LEFT OUTER JOIN [dbo].[TR_TKW] AS tkw ON tkw.[id] = ent.[ent_tkw_id]
         LEFT OUTER JOIN [dbo].[LTX_CUST_ENTITLEMENT] AS ent1 (NOLOCK) ON ent1.id_key = gft.ltx_cust_entitlement_id
         LEFT OUTER JOIN [dbo].[LTX_CUST_ENTITLEMENT] AS ent2 (NOLOCK) ON ent2.gift_no = gft.id_key
         LEFT OUTER JOIN [dbo].[LT_ENTITLEMENT_SET] AS est (NOLOCK) ON est.[set_no] = ent1.[set_no]
         LEFT OUTER JOIN [dbo].[LTX_CUST_ORDER_ENTITLEMENT] AS eord (NOLOCK) ON eord.ltx_cust_entitlement_id = ent2.id_key
         LEFT OUTER JOIN [dbo].[LV_ORDERS_WITH_ENTITLEMENTS] AS ord (NOLOCK) ON ord.[order_no] = eord.[order_no] AND ord.[sli_no] = eord.[sli_no]
         LEFT OUTER JOIN [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK) ON sli.[sli_no] = eord.[sli_no]
         LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
         LEFT OUTER JOIN [dbo].[T_ORDER] AS oin ON oin.[order_no] = eord.[order_no]
         LEFT OUTER JOIN [dbo].[TR_MOS] AS mos ON mos.[id] = oin.[MOS]
         LEFT OUTER JOIN [dbo].[TX_APPEAL_MEDIA_TYPE] AS med ON med.[source_no] = oin.[source_no]
         LEFT OUTER JOIN [CTE_ENTITLEMENT_SCANS] AS scn ON scn.[scan_string] = gft.[gift_code]
GO
    --WHERE gft.[create_dt] BETWEEN '7-1-2019' AND '3-3-2020'
    ----AND gft.[pass_status] = 'Used'
    --AND gft.[customer_no] = 2653957

    GRANT SELECT ON [dbo].[LV_GIFT_ENTITLEMENTS] TO ImpUsers
GO


--SELECT * FROM [dbo].[LV_GIFT_ENTITLEMENTS] WHERE create_dt BETWEEN '2-1-2020' AND '3-3-2020' and [pass_status] = 'Used'
--SELECT * FROM dbo.LT_ENTITLEMENT_GIFT WHERE create_dt BETWEEN '7-1-2019' AND '7-3-2019'



--    CREATE VIEW [dbo].[LV_GIFT_ENTITLEMENTS] AS
--    SELECT gft.[id_key] AS [gift_id],
--           gft.[customer_no] AS [giver_no],
--           nmg.[display_name] AS [giver_name],
--           nmg.[sort_name] AS [giver_name_sort],
--           gft.[gift_customer_no] AS [recipient_no],
--           CASE WHEN gft.[customer_no] = gft.[gift_customer_no] THEN CONVERT(VARCHAR(75),ISNULL(gft.[gift_recip_name],''))
--                ELSE CONVERT(VARCHAR(75),ISNULL(nmr.[display_name],ISNULL(gft.[gift_recip_name],''))) END AS [recipient_name],
--           CASE WHEN gft.[customer_no] = gft.[gift_customer_no] THEN CONVERT(VARCHAR(75),ISNULL(gft.[gift_recip_name],''))
--                ELSE CONVERT(VARCHAR(75),ISNULL(nmr.[sort_name],ISNULL(gft.[gift_recip_name],''))) END AS [recipient_name_sort],
--           ISNULL(gft.[gift_recip_email],'') AS [recipient_email],
--           CAST(gft.[gift_code] AS VARCHAR(25)) AS [gift_code],
--           CAST(gft.[group_gift_code] AS VARCHAR(25)) AS [group_gift_code],
--           gft.[create_dt],
--           gft.[init_dt],
--           gft.[expr_dt],
--           gft.[ltx_cust_entitlement_id],
--           gft.[entitlement_no],
--           ent.[entitlement_code],
--           ent.[ent_price_type_group],
--           grp.[description] AS [price_type_group_name],
--           ent.[ent_tkw_id],
--           tkw.[description] AS [keyword],
--           CAST(ent.[entitlement_desc] AS VARCHAR(100)) AS [entitlement_desc],
--           CASE WHEN ISNULL(ord.pass_status,'') = '' 
--                  OR ISNULL(eord.num_used,0) = 0 THEN gft.[num_items]
--                ELSE eord.[num_used] END AS [num_items],
--           gft.[gift_status],
--           CASE gft.[gift_status]
--                WHEN 'C' THEN 'Created'
--                WHEN 'A' THEN 'Accepted'
--                ELSE gft.[gift_status] END AS [gift_status_name],
--           CASE WHEN ISNULL(eord.num_used,0) > 0 THEN ISNULL(eord.num_used,0)
--                WHEN ISNULL(eord.num_used,0) <= 0 THEN ISNULL(gft.[num_items],0)
--                ELSE 1 END AS [counter_all],
--           CASE WHEN gft.[gift_status] = 'C' AND ISNULL(eord.num_used,0) > 0 THEN ISNULL(eord.num_used,0)
--                WHEN gft.[gift_status] = 'C' AND ISNULL(eord.num_used,0) <= 0 THEN ISNULL(gft.[num_items],0)
--                ELSE 0 END AS [counter_created],
--           CASE WHEN gft.[gift_status] = 'A' AND ISNULL(eord.num_used,0) > 0 THEN ISNULL(eord.num_used,0)
--                WHEN gft.[gift_status] = 'A' AND ISNULL(eord.num_used,0) <= 0 THEN ISNULL(gft.[num_items],0)
--                ELSE 0 END AS [counter_accepted],
--           CASE WHEN gft.[gift_status] NOT IN ('C','A') AND ISNULL(eord.num_used,0) > 0 THEN ISNULL(eord.num_used,0)
--                WHEN gft.[gift_status] NOT IN ('C','A') AND ISNULL(eord.num_used,0) <= 0 THEN ISNULL(gft.[num_items],0)
--                ELSE 0 END AS [counter_other],
--           CASE WHEN gft.[customer_no] = gft.[gift_customer_no] AND ISNULL(eord.num_used,0) > 0 THEN ISNULL(eord.num_used,0)
--                WHEN gft.[customer_no] = gft.[gift_customer_no] AND ISNULL(eord.num_used,0) <= 0 THEN ISNULL(gft.[num_items],0)
--                ELSE 0 END AS [counter_return_to_giver],
--           ISNULL(ent1.set_no,0) AS [set_no],
--           ISNULL(est.[name],'No Set') AS [set_name],
--           ISNULL(eord.order_no,0) AS [order_no],
--           ISNULL(eord.sli_no,0) AS [sli_no],
--           ISNULL(eord.perf_no,0) AS [perf_no],
--           ISNULL(prf.[performance_no],0) AS [performance_no],
--           ISNULL(prf.[performance_zone],0) AS [performance_zone],
--           prf.[performance_dt],
--           ISNULL([performance_date],'') AS [performance_date],
--           ISNULL(prf.[performance_time],'') AS [performance_time],
--           ISNULL(prf.[performance_time_display],'') AS [performance_time_display],
--           ISNULL(prf.[title_no],0) AS [title_no],
--           ISNULL(prf.[title_name],'') AS [title_name],
--           ISNULL(prf.[production_no],0) AS [production_no],
--           CASE WHEN ISNULL(prf.[production_name],'') = '' THEN 'Not Used'
--                ELSE prf.[production_name] END AS [production_name],
--           CASE WHEN ISNULL(ord.pass_status,'') = '' THEN 0
--                ELSE ISNULL(eord.num_used,0) END AS [num_used],
--           ISNULL(ord.pass_status,'Not Used') AS [pass_status],
--           ISNULL(ord.price_type_desc,'Not Used') AS [price_type_desc],
--           ISNULL(ord.short_desc,'') AS [shord_desc]
--    FROM LT_ENTITLEMENT_GIFT AS gft (NOLOCK)
--         LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nmg ON nmg.[customer_no] = gft.[customer_no]          --display name for the giver
--         LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nmr ON nmr.[customer_no] = gft.[gift_customer_no]     --display name for the recipient
--         LEFT OUTER JOIN [dbo].[LTR_ENTITLEMENT] AS ent (NOLOCK) ON ent.[entitlement_no] = gft.[entitlement_no]
--         LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE_GROUP] AS grp ON grp.[id] = ent.[ent_price_type_group]
--         LEFT OUTER JOIN [dbo].[TR_TKW] AS tkw ON tkw.[id] = ent.[ent_tkw_id]
--         LEFT OUTER JOIN [dbo].[LTX_CUST_ENTITLEMENT] AS ent1 (NOLOCK) ON ent1.id_key = gft.ltx_cust_entitlement_id
--         LEFT OUTER JOIN [dbo].[LTX_CUST_ENTITLEMENT] AS ent2 (NOLOCK) ON ent2.gift_no = gft.id_key
--         LEFT OUTER JOIN [dbo].[LT_ENTITLEMENT_SET] AS est (NOLOCK) ON est.[set_no] = ent1.[set_no]
--         LEFT OUTER JOIN [dbo].[LTX_CUST_ORDER_ENTITLEMENT] AS eord (NOLOCK) ON eord.ltx_cust_entitlement_id = ent2.id_key
--         LEFT OUTER JOIN [dbo].[LV_ORDERS_WITH_ENTITLEMENTS] AS ord (NOLOCK) ON ord.[order_no] = eord.[order_no] AND ord.[sli_no] = eord.[sli_no]
--         LEFT OUTER JOIN [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK) ON sli.[sli_no] = eord.[sli_no]
--         LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
--GO
