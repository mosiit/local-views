USE impresario
GO

    IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_GIFT_ENTITLEMENTS_PARTIALLY_USED]'))
        DROP VIEW [dbo].[LV_GIFT_ENTITLEMENTS_PARTIALLY_USED]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


    CREATE VIEW [dbo].[LV_GIFT_ENTITLEMENTS_PARTIALLY_USED] AS
         SELECT gft.[id_key] AS [gift_id],
                gft.[customer_no] AS [giver_no],
                nmg.[display_name] AS [giver_name],
                nmg.[sort_name] AS [giver_name_sort],
                gft.[gift_customer_no] AS [recipient_no],
                nmr.[display_name] AS [recipient_name],
                nmr.[sort_name] AS [recipient_name_sort],
                CAST(gft.[gift_code] AS VARCHAR(25)) AS [gift_code],
                CAST(gft.[group_gift_code] AS VARCHAR(25)) AS [group_gift_code],
                gft.[create_dt],
                gft.[entitlement_no],
                ent.[entitlement_code],
                ent.[ent_price_type_group],
                grp.[description] AS [price_type_group_name],
                CAST(ent.[entitlement_desc] AS VARCHAR(100)) AS [entitlement_desc],
                ent.[ent_tkw_id],
                tkw.[description] AS [keyword],
                gft.num_items,
                SUM(eord.num_used) AS [num_used],
                gft.num_items - SUM(eord.num_used) AS [num_left]
         FROM LT_ENTITLEMENT_GIFT AS gft (NOLOCK)
              LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nmg ON nmg.[customer_no] = gft.[customer_no]          --display name for the giver
              LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nmr ON nmr.[customer_no] = gft.[gift_customer_no]     --display name for the recipient
              LEFT OUTER JOIN [dbo].[LTR_ENTITLEMENT] AS ent (NOLOCK) ON ent.[entitlement_no] = gft.[entitlement_no]
              LEFT OUTER JOIN [dbo].[LTX_CUST_ENTITLEMENT] AS ent2 (NOLOCK) ON ent2.gift_no = gft.id_key
              LEFT OUTER JOIN [dbo].[LTX_CUST_ORDER_ENTITLEMENT] AS eord (NOLOCK) ON eord.ltx_cust_entitlement_id = ent2.id_key
              LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE_GROUP] AS grp ON grp.[id] = ent.[ent_price_type_group]
              LEFT OUTER JOIN [dbo].[TR_TKW] AS tkw ON tkw.[id] = ent.[ent_tkw_id]
         GROUP BY gft.[id_key], gft.[customer_no], nmg.[display_name], nmg.[sort_name], gft.[gift_customer_no], [nmr].[display_name], nmr.[sort_name],
                  CAST(gft.[group_gift_code] AS VARCHAR(25)), CAST(gft.[gift_code] AS VARCHAR(25)), gft.[create_dt], gft.[entitlement_no], 
                  ent.[entitlement_code], ent.[ent_price_type_group], grp.[description], CAST(ent.[entitlement_desc] AS VARCHAR(100)), ent.[ent_tkw_id], 
                  tkw.[description], gft.[num_items]
         HAVING gft.num_items - SUM(eord.num_used) > 0
GO

GRANT SELECT ON [dbo].[LV_GIFT_ENTITLEMENTS_PARTIALLY_USED] TO impusers


--SELECT * FROM [dbo].[LV_GIFT_ENTITLEMENTS_PARTIALLY_USED]


