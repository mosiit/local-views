USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_HISTORY_ORDER_INFO] AS
        SELECT his.[history_no],
               his.[performance_date],
               his.[order_no],
               his.[customer_no],
               ord.[order_dt],
               ISNULL(ord.[MOS],0) AS [mode_of_sale],
               ISNULL(mos.[description],'') AS [mode_of_sale_name],
               ISNULL(ord.[source_no],0) AS [source_no],
               ISNULL(sou.[source_name],'') AS [source_name],
               ISNULL(ord.[solicitor],0) AS [solicitor],
               ISNULL(usr.[full_name],'') AS [solicitor_name],
               ISNULL(usr.[sort_name],'') AS [solicitor_sort],
               ISNULL(usr.[location],'') AS [solicitor_location],
               ISNULL(usr.[inactive],'') AS [solicitor_inactive],
               ISNULL(ord.[tot_due_amt],0.00) AS [total_amount_due],
               ISNULL(ord.[tot_paid_amt],0.00) AS [total_amount_paid],
               ISNULL(ord.[tot_due_amt],0.00) - ISNULL(ord.[tot_paid_amt],0.00) AS [balance_due]
        FROM [dbo].[LT_HISTORY_TICKET] AS his
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = his.[order_no]
             LEFT OUTER JOIN [dbo].[TR_MOS] AS mos ON mos.[id] = ord.[MOS]
             LEFT OUTER JOIN [dbo].[TX_APPEAL_MEDIA_TYPE] AS sou ON sou.[source_no] = ord.[source_no]
             LEFT OUTER JOIN [dbo].[LV_MuseumUserInfo] AS usr ON usr.[userid] = ord.[solicitor]
GO

GRANT SELECT ON [dbo].[LV_HISTORY_ORDER_INFO] TO [ImpUsers], [tessitura_app]
GO




        
