USE [impresario]
GO

    IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_LIBRARY_LIST]'))
        DROP VIEW [dbo].[LV_LIBRARY_LIST];
GO

SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;
GO

    /*  Get a list of all libraries based on three separater criteria  
            1. Customer record has a customer type of 11 (Library)
            2. Customer record has a customer type of 12 (Organization) and the customer name contains the word Library
            3. Customer record has a customer type of 20 (N-Scan Collected Pass) and the customer name contains the word Library  */
    
          CREATE VIEW [dbo].[LV_LIBRARY_LIST] AS
          SELECT cus.[customer_no],
                 0 AS [second_customer_no],
                 cus.[lname] AS [library_name],
                 cus.[cust_type] AS [customer_type_no],
                 ISNULL(typ.[description],'Unknown') AS [customer_type],
                 'customer_type' AS [match_type],
                 'Paid' AS [library_type],
                 ISNULL(adr.[street1],'') AS [address1],
                 ISNULL(adr.[street2],'') AS [address2],
                 ISNULL(adr.[city],'') AS [city],
                 ISNULL(adr.[state],'') AS [state],
                 LEFT(ISNULL(adr.[postal_code],''),5) AS [postal_code],
                 ISNULL(mem.[cust_memb_no],0) AS [cust_memb_no],
                 ISNULL(mem.[memb_level],'') AS [memb_level],
                 mem.[init_dt],
                 mem.[expr_dt],
                 CASE WHEN ISNULL(mem.[expr_dt],DATEADD(DAY,-1,GETDATE())) > GETDATE() THEN 'Y' ELSE 'N' END AS [has_active_membership]
          FROM [dbo].[T_CUSTOMER] AS cus
               LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr ON adr.[customer_no] = cus.[customer_no] AND adr.[primary_ind] = 'Y'
               LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] AS typ ON typ.[id] = cus.[cust_type]
               LEFT OUTER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem ON mem.[customer_no] = cus.[customer_no] AND mem.memb_org_no = 8 AND mem.[current_status] = 2 AND mem.[cur_record] = 'Y'
          WHERE cus.[inactive] = 1 
            AND cus.[cust_type] = 11     --cust_type 11 = Library
    
    UNION SELECT cus.[customer_no],
                 0 AS [second_customer_no],
                 cus.[lname] AS [library_name],
                 cus.[cust_type] AS [customer_type_no],
                 ISNULL(typ.[description],'Unknown') AS [customer_type],
                 'customer_name' AS [match_type],
                 'Paid' AS [library_type],
                 ISNULL(adr.[street1],'') AS [address1],
                 ISNULL(adr.[street2],'') AS [address2],
                 ISNULL(adr.[city],'') AS [city],
                 ISNULL(adr.[state],'') AS [state],
                 LEFT(ISNULL(adr.postal_code,''),5) AS [postal_code],
                 ISNULL(mem.[cust_memb_no],0) AS [cust_memb_no],
                 ISNULL(mem.[memb_level],'') AS [memb_level],
                 mem.[init_dt],
                 mem.[expr_dt],
                 CASE WHEN ISNULL(mem.[expr_dt],DATEADD(DAY,-1,GETDATE())) > GETDATE() THEN 'Y' ELSE 'N' END AS [has_active_membership]
          FROM [dbo].[T_CUSTOMER] AS cus
               LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr ON adr.[customer_no] = cus.[customer_no] AND adr.[primary_ind] = 'Y'
               LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] AS typ ON typ.[id] = cus.[cust_type]
               LEFT OUTER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem ON mem.[customer_no] = cus.[customer_no] AND mem.memb_org_no = 8 AND mem.[current_status] = 2 AND mem.[cur_record] = 'Y'
          WHERE cus.[inactive] = 1 
            AND cus.[cust_type] = 12     --cust_type 12 = Orgainization
            AND cus.[lname] LIKE '%Library%'

    UNION SELECT cus.[customer_no],
                 ISNULL(cu2.[customer_no],0) AS [second_customer_no],
                 cus.[lname] AS [library_name],
                 cus.[cust_type] AS [customer_type_no],
                 ISNULL(typ.[description],'Unknown') AS [customer_type],
                 'show_and_go',
                 'Free' AS [library_type],
                 ISNULL(ad2.[street1],'') AS [address1],
                 ISNULL(ad2.[street2],'') AS [address2],
                 ISNULL(ad2.[city],'') AS [city],
                 ISNULL(ad2.[state],'') AS [state],
                 LEFT(ISNULL(ad2.[postal_code],''),5) AS [postal_code],
                 ISNULL(mem.[cust_memb_no],0) AS [cust_memb_no],
                 ISNULL(mem.[memb_level],'') AS [memb_level],
                 mem.[init_dt],
                 mem.[expr_dt],
                 CASE WHEN ISNULL(mem.[expr_dt],DATEADD(DAY,-1,GETDATE())) > GETDATE() THEN 'Y' ELSE 'N' END AS [has_active_membership]
          FROM [dbo].[T_CUSTOMER] AS cus
               LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cu2 ON cu2.[lname] = cus.[lname] AND cu2.[cust_type] = 11 AND cu2.inactive = 1
               --LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr ON adr.[customer_no] = cus.[customer_no] AND adr.[primary_ind] = 'Y'
               LEFT OUTER JOIN [dbo].[T_ADDRESS] AS ad2 ON ad2.[customer_no] = cu2.[customer_no] AND ad2.[primary_ind] = 'Y'
               LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] AS typ ON typ.[id] = cus.[cust_type]
               LEFT OUTER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem ON mem.[customer_no] = ISNULL(cu2.[customer_no],cus.[customer_no]) AND mem.memb_org_no = 8 AND mem.[current_status] = 2 AND mem.[cur_record] = 'Y'
          WHERE cus.[inactive] = 1 
            AND cus.[cust_type] = 20     --cust_type 20 = 'N-Scan Collected Pass
            AND cus.[lname] LIKE '%library%'
            AND cus.[lname] <> 'LPF - Library Pass Free'
GO

GRANT SELECT ON [dbo].[LV_LIBRARY_LIST] TO ImpUsers;
GO

SELECT * FROM [dbo].[LV_LIBRARY_LIST] WHERE library_type = 'Free' --AND has_active_membership = 'N';


--SELECT lname, COUNT(*) FROM dbo.T_CUSTOMER WHERE cust_type = 11 AND customer_no IN (SELECT customer_no FROM dbo.LV_LIBRARY_LIST WHERE library_type = 'Free') GROUP BY lname HAVING COUNT(*) > 1