USE [impresario]
GO

    IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_LIBRARY_MEMBERSHIP_LIST]'))
        DROP VIEW [dbo].[LV_LIBRARY_MEMBERSHIP_LIST];
GO

SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;
GO

    CREATE VIEW [dbo].[LV_LIBRARY_MEMBERSHIP_LIST] AS
            WITH [most_recent_memberships] (customer_no, [cust_memb_no]) AS
                (SELECT [customer_no], MAX([cust_memb_no])
                 FROM [dbo].[TX_CUST_MEMBERSHIP] 
                 WHERE [memb_level] = 'L' AND [current_status] IN (1,2) 
                 GROUP BY [customer_no])
        SELECT mem.[cust_memb_no],
               mem.[customer_no],
               ct1.[description] AS [customer_type],
               ISNULL(scn.[customer_no],0) AS [scan_customer_no],
               ISNULL(ct2.[description],'') AS [scan_customer_type],
               mem.[memb_level],
               mem.[init_dt],
               mem.[expr_dt],
               mem.[current_status] AS [current_status_no],
               sta.[description] AS [current_status],
               cus.[lname] AS [library_name],
               CASE WHEN mem.[current_status] = 1 THEN 'Inactive'
                    WHEN mem.[current_status] = 2 and ISNULL(scn.[lname],'') <> '' THEN 'Free'
                    ELSE 'Paid' END AS [library_type],
               ISNULL(adr.[street1],'') AS [street1],
               ISNULL(adr.[street2],'') AS [street2],
               ISNULL(adr.[street3],'') AS [street3],
               ISNULL(adr.[city],'') AS [city],
               ISNULL(adr.[state],'') AS [state],
               LEFT(ISNULL(adr.[postal_code],''),5) AS [postal_code]
        FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK)
             INNER JOIN [most_recent_memberships] AS rec (NOLOCK) ON rec.[cust_memb_no] = mem.[cust_memb_no]
             INNER JOIN [dbo].[TR_CURRENT_STATUS] AS sta (NOLOCK) ON sta.[id] = mem.[current_status]
             INNER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = mem.[customer_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS scn (NOLOCK) ON scn.[lname] = cus.[lname] AND scn.[cust_type] = 20 AND [mem].[current_status] IN (1,2)
             LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr (NOLOCK) ON adr.[customer_no] = cus.[customer_no] AND adr.[primary_ind] = 'Y'
             INNER JOIN [dbo].[TR_CUST_TYPE] AS ct1 (NOLOCK) ON ct1.[id] = cus.[cust_type]
             LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] AS ct2 (NOLOCK) ON ct2.[id] = scn.[cust_type]
    GO

    GRANT SELECT ON [dbo].[LV_LIBRARY_MEMBERSHIP_LIST] TO ImpUsers
    GO

    SELECT * FROM [dbo].[LV_LIBRARY_MEMBERSHIP_LIST]
