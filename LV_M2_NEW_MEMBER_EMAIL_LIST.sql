USE [impresario]
GO

/****** Object:  View [dbo].[LV_M2_NEW_MEMBER_EMAIL_LIST]    Script Date: 7/9/2019 12:09:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER VIEW [dbo].[LV_M2_NEW_MEMBER_EMAIL_LIST] AS
WITH [cte_email_login] ([login_email])
AS (SELECT DISTINCT [login] FROM [dbo].[T_CUST_LOGIN]
    WHERE [login_type] = 1 AND [inactive] = 'N' AND [login] NOT LIKE '%kiosk%@mos.org')
SELECT 'A0' AS [affiliation_type],
		-- H. Sheridan, 20190626 - Changed per work order 98982 - Membership Welcome Workflow Follow-up to #92178
	   mem.[customer_no] AS [order__member_id],
       mem.[customer_no],
       mem.[memb_level],
       lev.[description] AS [order__level_description],
       CAST(mem.[create_dt] AS DATE) AS [custom_date], --Per Scot: Create Date, not Init Date
       CAST(mem.[init_dt] AS DATE) AS [init_dt],
       CASE WHEN mem.[NRR_status] = 'NE' THEN 'New'
            ELSE 'Renew/Rejoin' END AS [order__NRR_Status],
       mem.[current_status],
       sta.[description] AS [current_status_name],
       cus.[display_name] AS [member_name],
       ISNULL(sal.[lsal_desc],'') AS [order__salutation],
       CASE WHEN eml.[address] LIKE '%kiosk%@mos.org' THEN ''
            WHEN eml.[address] IS NULL THEN '' 
            ELSE ISNULL(eml.[address], '') END AS [address],
       ISNULL(eml.[primary_ind], 'N') AS [email_primary_ind],
       ISNULL(eml.[inactive], 'Y') AS [email_inactive],
       ISNULL(shp.[description], '') AS [ship_method],
        CASE WHEN ISNULL(shp.[description],'') = 'one step' THEN 'Y'
             ELSE 'N' END AS [order__one_step],
        CASE WHEN lgn.[login] LIKE '%kiosk%@mos.org' THEN ''
             WHEN ISNULL(lgn.[login], '') <> '' THEN lgn.[login]
             ELSE ISNULL(elg.login_email,'') END AS [order__login],
        --CASE WHEN lgn.[login] LIKE '%kiosk%@mos.org' THEN ''
        --     ELSE ISNULL(lgn.[login], '') END AS [order__login],
        CASE WHEN lgn.[login] LIKE '%kiosk%@mos.org' THEN 'N'
             WHEN ISNULL(lgn.[login],'') = '' THEN 'N' 
             ELSE 'Y' END AS [order__login_option2],
        CASE WHEN ISNULL(elg.login_email,'') = '' THEN 'N'
             ELSE 'Y' END AS [order__login_option3]
FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK)
    INNER JOIN [dbo].[TR_CURRENT_STATUS] AS sta (NOLOCK) ON sta.[id] = mem.[current_status]
    INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS cus ON cus.[customer_no] = mem.[customer_no]
	 -- H. Sheridan, 20190703 - Added check for eml.inactive here (from WHERE).  As part of the join, it doesn't have to exist.  If part of WHERE, it must evaluate to true.
    LEFT OUTER JOIN [dbo].[T_EADDRESS] AS eml (NOLOCK) ON eml.[customer_no] = mem.[customer_no] AND ISNULL(eml.[primary_ind], 'N') = 'Y' AND eml.inactive = 'N'
    LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev (NOLOCK) ON mem.[memb_level] = lev.[memb_level]
    LEFT OUTER JOIN [dbo].[TX_CUST_SAL] AS sal (NOLOCK) ON sal.[customer_no] = mem.[customer_no] AND sal.[signor] = 0
    LEFT OUTER JOIN [dbo].[TR_SHIP_METHOD] AS shp (NOLOCK) ON shp.[id] = mem.[ship_method] AND mem.[ship_method] = 3
    LEFT OUTER JOIN [dbo].[T_CUST_LOGIN] AS lgn (NOLOCK) ON lgn.[customer_no] = mem.[customer_no] AND lgn.[login] = eml.[address] AND lgn.[login_type] = 1
    LEFT OUTER JOIN [cte_email_login] AS elg (NOLOCK) ON elg.[login_email] = eml.[address]
WHERE mem.memb_org_no = 4 -- household memberships
      AND mem.[current_status] IN ( 2, 3 ) -- active and pending memberships
	  -- H. Sheridan, 20190703 - Moved to join above.  If part of the WHERE clause, this excludes members with no email on the household but who do have A1 and A2 records with emails.
      --AND eml.inactive = 'N'
	  -- H. Sheridan, 20190626 - Added per work order 98982 - Membership Welcome Workflow Follow-up to #92178
	  AND mem.ben_provider = 0
	  -- H. Sheridan, 20190626 - Added per work order 98982 - Membership Welcome Workflow Follow-up to #92178
	  AND NOT EXISTS (SELECT 1 FROM [dbo].[LT_M2_WELCOME_SENT_LOG] wsl (NOLOCK) WHERE wsl.customer_no = cus.customer_no AND DATEDIFF(DAY, wsl.[custom_date], GETDATE()) <= 180)
UNION ALL
SELECT 'A1' AS [affiliation_type],
		-- H. Sheridan, 20190626 - Changed per work order 98982 - Membership Welcome Workflow Follow-up to #92178
       --mem.[cust_memb_no],
	   aff.[group_customer_no] AS [order__member_id],
       aff.[individual_customer_no],
       mem.[memb_level],
       lev.[description] AS [order__level_description],
       CAST(mem.[create_dt] AS DATE) AS [custom_date], --Per Scot: Create Date, not Init Date
       CAST(mem.[init_dt] AS DATE) AS [init_dt],
       CASE WHEN mem.[NRR_status] = 'NE' THEN 'New'
            ELSE 'Renew/Rejoin' END AS [order__NRR_Status],
       mem.[current_status],
       sta.[description] AS [current_status_name],
       ca1.[display_name] AS [member_name],
       ISNULL(sal.[lsal_desc],'') AS [order__salutation],
       CASE WHEN ea1.[address] LIKE '%kiosk%@mos.org' THEN ''
            WHEN ea1.[address] IS NULL THEN '' 
            ELSE ISNULL(ea1.[address], '') END AS [address],
       ISNULL(ea1.[primary_ind], 'N') AS [email_primary_ind],
       ISNULL(ea1.[inactive], 'Y') AS [email_inactive],
       ISNULL(shp.[description], '') AS [ship_method],
        CASE WHEN ISNULL(shp.[description],'') = 'one step' THEN 'Y'
             ELSE 'N' END AS [order__one_step],
        CASE WHEN lgn.[login] LIKE '%kiosk%@mos.org' THEN ''
             WHEN ISNULL(lgn.[login], '') <> '' THEN lgn.[login]
             ELSE ISNULL(elg.login_email,'') END AS [order__login],
        CASE WHEN lgn.[login] LIKE '%kiosk%@mos.org' THEN 'N'
             WHEN ISNULL(lgn.[login],'') = '' THEN 'N' 
             ELSE 'Y' END AS [order__login_option2],
        CASE WHEN ISNULL(elg.login_email,'') = '' THEN 'N'
             ELSE 'Y' END AS [order__login_option3]
FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK)
    INNER JOIN [dbo].[TR_CURRENT_STATUS] AS sta (NOLOCK) ON sta.[id] = mem.[current_status]
    INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS cus ON cus.[customer_no] = mem.[customer_no]
	-- H. Sheridan, 20190703 - Added check for eml.inactive here (from WHERE).  As part of the join, it doesn't have to exist.  If part of WHERE, it must evaluate to true.
    LEFT OUTER JOIN [dbo].[T_EADDRESS] AS eml (NOLOCK) ON eml.[customer_no] = mem.[customer_no] AND ISNULL(eml.[primary_ind], 'N') = 'Y' AND eml.inactive = 'N'
    LEFT OUTER JOIN [dbo].[T_AFFILIATION] AS aff (NOLOCK) ON aff.[group_customer_no] = mem.[customer_no] AND aff.[affiliation_type_id] = 10002 AND aff.name_ind = -1
    INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS ca1 ON ca1.[customer_no] = aff.[individual_customer_no]
    LEFT OUTER JOIN [dbo].[T_EADDRESS] AS ea1 (NOLOCK) ON ea1.[customer_no] = aff.[individual_customer_no] AND ISNULL(ea1.[primary_ind], 'N') = 'Y'
    LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev (NOLOCK) ON mem.[memb_level] = lev.[memb_level]
    LEFT OUTER JOIN [dbo].[TX_CUST_SAL] AS sal (NOLOCK) ON sal.[customer_no] = aff.[individual_customer_no] AND sal.[signor] = 0
    LEFT OUTER JOIN [dbo].[TR_SHIP_METHOD] AS shp (NOLOCK) ON shp.[id] = mem.[ship_method] AND mem.[ship_method] = 3
    LEFT OUTER JOIN [dbo].[T_CUST_LOGIN] AS lgn (NOLOCK) ON lgn.[customer_no] = aff.[individual_customer_no] AND lgn.[login] = ea1.[address] AND lgn.[login_type] = 1
    LEFT OUTER JOIN [cte_email_login] AS elg (NOLOCK) ON elg.[login_email] = ea1.[address]
WHERE mem.memb_org_no = 4 -- household memberships
      AND mem.[current_status] IN ( 2, 3 ) -- active and pending memberships
	  -- H. Sheridan, 20190703 - Moved to join above.  If part of the WHERE clause, this excludes members with no email on the household but who do have A1 and A2 records with emails.
      --AND eml.inactive = 'N'
	  -- H. Sheridan, 20190626 - Added per work order 98982 - Membership Welcome Workflow Follow-up to #92178
	  AND mem.ben_provider = 0
	  -- H. Sheridan, 20190626 - Added per work order 98982 - Membership Welcome Workflow Follow-up to #92178
	  AND NOT EXISTS (SELECT 1 FROM [dbo].[LT_M2_WELCOME_SENT_LOG] wsl (NOLOCK) WHERE wsl.customer_no = cus.customer_no AND DATEDIFF(DAY, wsl.[custom_date], GETDATE()) <= 180)
UNION ALL
SELECT 'A2' AS [affiliation_type],
		-- H. Sheridan, 20190626 - Changed per work order 98982 - Membership Welcome Workflow Follow-up to #92178
       --mem.[cust_memb_no],
	   aff.[group_customer_no] AS [order__member_id],
       aff.[individual_customer_no],
       mem.[memb_level],
       lev.[description] AS [order__level_description],
       CAST(mem.[create_dt] AS DATE) AS [custom_date], --Per Scot: Create Date, not Init Date
       CAST(mem.[init_dt] AS DATE) AS [init_dt],
       CASE WHEN mem.[NRR_status] = 'NE' THEN 'New'
            ELSE 'Renew/Rejoin' END AS [order__NRR_Status],
       mem.[current_status],
       sta.[description] AS [current_status_name],
       ca2.[display_name] AS [member_name],
       ISNULL(sal.[lsal_desc],'') AS [order__salutation],
       CASE WHEN ea2.[address] LIKE '%kiosk%@mos.org' THEN ''
            WHEN ea2.[address] IS NULL THEN '' 
            ELSE ISNULL(ea2.[address], '') END AS [address],
       ISNULL(ea2.[primary_ind], 'N') AS [email_primary_ind],
       ISNULL(ea2.[inactive], 'Y') AS [email_inactive],
       ISNULL(shp.[description], '') AS [ship_method],
        CASE WHEN ISNULL(shp.[description],'') = 'one step' THEN 'Y'
             ELSE 'N' END AS [order__one_step],
        CASE WHEN lgn.[login] LIKE '%kiosk%@mos.org' THEN ''
             WHEN ISNULL(lgn.[login], '') <> '' THEN lgn.[login]
             ELSE ISNULL(elg.login_email,'') END AS [order__login],
        CASE WHEN lgn.[login] LIKE '%kiosk%@mos.org' THEN 'N'
             WHEN ISNULL(lgn.[login],'') = '' THEN 'N' 
             ELSE 'Y' END AS [order__login_option2],
        CASE WHEN ISNULL(elg.login_email,'') = '' THEN 'N'
             ELSE 'Y' END AS [order__login_option3]
FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK)
    INNER JOIN [dbo].[TR_CURRENT_STATUS] AS sta (NOLOCK) ON sta.[id] = mem.[current_status]
    INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS cus ON cus.[customer_no] = mem.[customer_no]
	-- H. Sheridan, 20190703 - Added check for eml.inactive here (from WHERE).  As part of the join, it doesn't have to exist.  If part of WHERE, it must evaluate to true.
    LEFT OUTER JOIN [dbo].[T_EADDRESS] AS eml (NOLOCK) ON eml.[customer_no] = mem.[customer_no] AND ISNULL(eml.[primary_ind], 'N') = 'Y' AND eml.inactive = 'N'
    LEFT OUTER JOIN [dbo].[T_AFFILIATION] AS aff (NOLOCK) ON aff.[group_customer_no] = mem.[customer_no] AND aff.[affiliation_type_id] = 10002 AND aff.name_ind = -2
    INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS ca2 ON ca2.[customer_no] = aff.[individual_customer_no]
    LEFT OUTER JOIN [dbo].[T_EADDRESS] AS ea2 (NOLOCK) ON ea2.[customer_no] = aff.[individual_customer_no] AND ISNULL(ea2.[primary_ind], 'N') = 'Y'
    LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev (NOLOCK) ON mem.[memb_level] = lev.[memb_level]
    LEFT OUTER JOIN [dbo].[TX_CUST_SAL] AS sal (NOLOCK) ON sal.[customer_no] = aff.[individual_customer_no] AND sal.[signor] = 0
    LEFT OUTER JOIN [dbo].[TR_SHIP_METHOD] AS shp (NOLOCK) ON shp.[id] = mem.[ship_method] AND mem.[ship_method] = 3
    LEFT OUTER JOIN [dbo].[T_CUST_LOGIN] AS lgn (NOLOCK) ON lgn.[customer_no] = aff.[individual_customer_no] AND lgn.[login] = ea2.[address] AND lgn.[login_type] = 1
    LEFT OUTER JOIN [cte_email_login] AS elg (NOLOCK) ON elg.[login_email] = ea2.[address]
WHERE mem.memb_org_no = 4 -- household memberships
      AND mem.[current_status] IN ( 2, 3 ) -- active and pending memberships
	  -- H. Sheridan, 20190703 - Moved to join above.  If part of the WHERE clause, this excludes members with no email on the household but who do have A1 and A2 records with emails.
      --AND eml.inactive = 'N'
	  -- H. Sheridan, 20190626 - Added per work order 98982 - Membership Welcome Workflow Follow-up to #92178
	  AND mem.ben_provider = 0
	  -- H. Sheridan, 20190626 - Added per work order 98982 - Membership Welcome Workflow Follow-up to #92178
	  AND NOT EXISTS (SELECT 1 FROM [dbo].[LT_M2_WELCOME_SENT_LOG] wsl (NOLOCK) WHERE wsl.customer_no = cus.customer_no AND DATEDIFF(DAY, wsl.[custom_date], GETDATE()) <= 180)



GO


SELECT * FROM [dbo].[LV_M2_NEW_MEMBER_EMAIL_LIST]