USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_M2_VXM_SURVEY]
AS

-- THIS A VIEW TO BE USED BY MAIL2 TO SEND OUT VXM SURVEYS TO SCHOOL GROUPS. 
-- DSJ 8/25/2017 

WITH SchoolPriceTypes
AS
(
	SELECT id ,description, short_desc
	FROM dbo.TR_PRICE_TYPE 
	WHERE description LIKE '%school%'
		AND inactive = 'N'
) 
SELECT DISTINCT 
	o.order_no,
	o.initiator_no, 
	e.address,
	CAST(prf.performance_date AS DATE) AS custom_date, 
	DATEDIFF(d,GETDATE(),prf.performance_date) daysAgoSchoolVisited
FROM T_ORDER o
	INNER JOIN [dbo].[T_SUB_LINEITEM] as sli 
		ON sli.order_no = o.order_no
		AND sli.sli_status IN (3,12) -- Seated, Paid & Ticketed, Paid, this is to make sure the school actually showed up that day and not a canceled order 
	INNER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf 
		ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
	INNER JOIN SchoolPriceTypes as pty 
		ON pty.[id] = sli.[price_type]
	LEFT JOIN dbo.FT_GET_PRIMARY_EADDRESS() e 
		ON e.customer_no = o.initiator_no 
WHERE 
	o.customer_no <> 637487 -- exclude Boston Duck Tours
	AND o.tot_due_amt - o.tot_paid_amt = 0  -- no balance due 
	AND e.address IS NOT NULL 
	AND DATEDIFF(d,GETDATE(),prf.performance_date) = -1 -- this number should correspond with how often they are sending the surveys out. 
	AND o.MOS IN (12,13) -- Schools, Web Sales School
GO

GRANT SELECT ON  [dbo].[LV_M2_VXM_SURVEY] to ImpUsers
GO


