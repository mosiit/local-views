USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_MEMBERSHIP_HIERARCHY] AS
      SELECT 24 AS [memb_org_no], 'AF_L_Membership' AS [memb_org], 'A' AS [memb_org_sort]
UNION SELECT  5, 'AF Membership', 'B'
UNION SELECT 26, 'Innovator Membership', 'C'
UNION SELECT  4, 'Household Membership', 'D'
UNION SELECT 10, 'EXEC Passholder Membership', 'E'
UNION SELECT 13, 'Teacher Partner', 'F'
UNION SELECT 27, 'Museum Staff and Volunteers', 'G'
UNION SELECT  7, 'Corporate Membership', 'H'
UNION SELECT  8, 'Library Membership', 'I'
UNION SELECT  9, 'Social Agency Membership', 'J'
GO

GRANT SELECT ON [dbo].[LV_MEMBERSHIP_HIERARCHY] TO [ImpUsers], [tessitura_app]
GO

SELECT * FROM [dbo].[LV_MEMBERSHIP_HIERARCHY] ORDER BY memb_org_sort
