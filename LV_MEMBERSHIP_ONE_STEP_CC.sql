USE [impresario]
GO

/****** Object:  View [dbo].[LV_MEMBERSHIP_ONE_STEP_CC]    Script Date: 12/27/2016 3:11:01 PM ******/
DROP VIEW [dbo].[LV_MEMBERSHIP_ONE_STEP_CC]
GO

/****** Object:  View [dbo].[LV_MEMBERSHIP_ONE_STEP_CC]    Script Date: 12/27/2016 3:11:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_MEMBERSHIP_ONE_STEP_CC]
AS
	-- 6/30/2017, H. Sheridan - added block of code for work order 87689 - Update to LV_MEMBERSHIP_ONE_STEP_CC by 6/30/17
    SELECT DISTINCT
                (customer_no),
                id AS act_id
       FROM     t_account_data
       WHERE    id IN (SELECT DISTINCT
                                        a.key_value AS act_id
                                FROM    VXS_CUST_KEYWORD a WITH (NOLOCK)
                                WHERE   a.keyword_no = 417)
                AND inactive = 'N'
     
	   UNION  

      SELECT DISTINCT
                (customer_no),
                id AS act_id
       FROM     t_account_data
       WHERE    customer_no IN (SELECT DISTINCT
                                        a.customer_no
                                FROM    V_CUSTOMER_WITH_PRIMARY_GROUP a WITH (NOLOCK)
                                JOIN    (SELECT a1.customer_no
                                         FROM   vxs_const_cust a1 WITH (NOLOCK)
                                         WHERE  a1.constituency IN (30)) AS e ON e.customer_no = a.expanded_customer_no
                                WHERE   ISNULL(a.inactive, 1) = 1)
                AND payment_method_group_id = 3
				AND inactive = 'N'

       UNION

       SELECT DISTINCT
                customer_no,
                MAX(id) OVER (PARTITION BY customer_no) AS act_id
       FROM     t_account_data
       WHERE    customer_no IN (SELECT DISTINCT
                                        a.customer_no
                                FROM    V_CUSTOMER_WITH_PRIMARY_GROUP a WITH (NOLOCK)
                                JOIN    (SELECT a1.customer_no
                                         FROM   VXS_CONST_CUST a1 WITH (NOLOCK)
                                         WHERE  a1.constituency IN (30)) AS e ON e.customer_no = a.expanded_customer_no
                                WHERE   ISNULL(a.inactive, 1) = 1)
                AND customer_no NOT IN (SELECT DISTINCT
                                                customer_no
                                        FROM    t_account_data
                                        WHERE   customer_no IN (SELECT DISTINCT
                                                                        a.customer_no
                                                                FROM    V_CUSTOMER_WITH_PRIMARY_GROUP a WITH (NOLOCK)
                                                                JOIN    (SELECT a1.customer_no
                                                                         FROM   VXS_CONST_CUST a1 WITH (NOLOCK)
                                                                         WHERE  a1.constituency IN (30)) AS e ON e.customer_no = a.expanded_customer_no
                                                                WHERE   ISNULL(a.inactive, 1) = 1)
                                                AND payment_method_group_id = 3)
												AND inactive = 'N'
       GROUP BY customer_no, id
       --ORDER BY customer_no
GO

GRANT SELECT ON [dbo].[LV_MEMBERSHIP_ONE_STEP_CC] TO [ImpUsers]
GO
