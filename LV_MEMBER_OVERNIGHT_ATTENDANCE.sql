USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_MEMBER_OVERNIGHT_ATTENDANCE]'))
DROP VIEW [dbo].[LV_MEMBER_OVERNIGHT_ATTENDANCE]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_MEMBER_OVERNIGHT_ATTENDANCE] AS
    SELECT [performance_no], 
           [performance_zone],
           [overnight_dt], 
           [production_name],
           [order_no],
           [customer_no],
           [group_name],
           [member_name],
           [member_sort],
           [city],
           [state],
           [order_notes],
           [adult],
           [child],
           ([Adult] + [Child]) AS [total]
    FROM (SELECT prf.[performance_no], 
                prf.[performance_zone],
                CONVERT(DATE,prf.[performance_dt]) AS [overnight_dt], 
                prf.[production_name],
                sli.[order_no],
                ord.[customer_no],
                LEFT(cus.[sort_name],CHARINDEX ('/', cus.[sort_name]) - 1) AS [group_name],
                LTRIM(ISNULL(cus.[fname],'') + ' ' + ISNULL(cus.[lname],'')) AS [Member_name],
                cus.[sort_name] AS [member_sort],
                adr.[city],
                adr.[state],
                CASE WHEN ptp.[description] IN ('Member Service','Member Senior') THEN 'Adult'
                     ELSE REPLACE(ptp.[description],'Member ','') END AS [price_type],
                ISNULL(ord.[notes],'') AS [order_notes],
                sli.[sli_no]
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK)
                 LEFT OUTER JOIN [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK) ON sli.[perf_no] = prf.[performance_no] AND sli.[zone_no] = prf.[performance_zone]
                 INNER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
                 LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
                 LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr (NOLOCK) ON adr.[customer_no] = ord.[customer_no] AND adr.[primary_ind] = 'Y'
                INNER JOIN [dbo].[TR_PRICE_TYPE] AS ptp (NOLOCK) ON ptp.[id] = sli.[price_type]
            WHERE prf.[production_name] = 'Member Overnight'
              AND sli.[sli_status] IN (3, 12)   -- 3 = Seated Paid / 12 = Ticketed Paid
           ) m
            PIVOT (COUNT([sli_no])
                   FOR  [price_type] IN ([Adult], [Child])
                  ) piv
GO

GRANT SELECT ON [dbo].[LV_MEMBER_OVERNIGHT_ATTENDANCE] TO ImpUsers
GO

--SELECT * FROM [LV_MEMBER_OVERNIGHT_ATTENDANCE] WHERE overnight_dt = '11-9-2018' AND order_no IN (1414408,1417185,1426275,1427507,1440117,1481425) ORDER BY [member_sort]

