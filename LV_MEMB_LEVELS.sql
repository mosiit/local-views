USE [impresario]
GO

/****** Object:  View [dbo].[LV_MEMB_LEVELS]    Script Date: 9/22/2017 10:11:44 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_MEMB_LEVELS]

AS

/* SELECT * FROM LV_MEMB_LEVELS */

SELECT 
	l.memb_level_no,
	memb_org_level = o.description + ' | ' + l.description,
	level_description = l.description,
	l.renewal
FROM T_MEMB_LEVEL l
	JOIN dbo.T_MEMB_ORG o ON l.memb_org_no = o.memb_org_no
	

GO


