USE [impresario]
GO

/****** Object:  View [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]    Script Date: 9/19/2018 2:26:01 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_MOS_RESOURCES]'))
DROP VIEW [dbo].[LV_MOS_RESOURCES]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

    CREATE VIEW [dbo].[LV_MOS_RESOURCES] AS
    SELECT rsr.[id],
           ISNULL(rsr.[worker_customer_no],0) AS [worker_customer_no],
           cat.[description] AS [resource_category],
           typ.[description] AS [resource_type],
           CASE WHEN ISNULL(rsr.[worker_customer_no],0) > 0 THEN CONVERT(VARCHAR(100),nam.[display_name_short])
                ELSE ISNULL(rsr.[description],'') END as [resource_name],
           CASE WHEN ISNULL(rsr.[worker_customer_no],0) > 0 THEN CONVERT(VARCHAR(100),nam.[sort_name])
                ELSE ISNULL(rsr.[description],'') END as [resource_name_sort],
           rsr.[inactive]
    FROM [dbo].[T_RESOURCE] AS rsr
         LEFT OUTER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS nam ON nam.[customer_no] = rsr.[worker_customer_no]
         INNER JOIN dbo.[TR_RESOURCE_TYPE] AS typ ON typ.[id] = rsr.[type]
         INNER JOIN dbo.[TR_RESOURCE_CATEGORY] AS cat ON cat.[id] = typ.[category]
    

GO


GRANT SELECT ON [dbo].[LV_MOS_RESOURCES] TO ImpUsers
GO




