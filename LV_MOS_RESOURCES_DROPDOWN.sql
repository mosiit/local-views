--DROP VIEW [dbo].[LV_MOS_RESOURCES_DROPDOWN]
--GO

CREATE VIEW [dbo].[LV_MOS_RESOURCES_DROPDOWN] AS
SELECT  0 AS [id], 
        0 AS [worker_customer_no], 
        '' AS[resource_category], 
        '' AS [resource_type], 
        '(none)' AS [resource_name], 
        '(none)' AS [resource_name_sort],
        'N' AS [inactive]
UNION ALL
SELECT  [id], 
        [worker_customer_no], 
        [resource_category], 
        [resource_type], 
        [resource_name], 
        [resource_name_sort],
        [inactive]
FROM [dbo].[LV_MOS_RESOURCES]
GO

GRANT SELECT ON [dbo].[LV_MOS_RESOURCES_DROPDOWN] TO ImpUsers
GO

SELECT * FROM LV_MOS_RESOURCES_DROPDOWN WHERE resource_type IN ('','Drop-In Activity') and inactive = 'N'