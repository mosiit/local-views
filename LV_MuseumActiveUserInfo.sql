USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_MuseumActiveUserInfo]'))
    DROP VIEW [dbo].[LV_MuseumActiveUserInfo]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LV_MuseumActiveUserInfo] AS
SELECT  [userid],
        [fname],
        [lname],
        [full_name],
        [sort_name],
        [email_address],
        [location],
        [default_user_group],
        [last_login_dt],
        [days_since_last_login],
        [pw_expr_dt],
        [locked_ind],
        [num_logins_allowed],
        [inactive],
        [worker_customer_no]
FROM [dbo].[LV_MuseumUserInfo]
WHERE [inactive] = 'N'
GO

GRANT SELECT ON [dbo].[LV_MuseumActiveUserInfo] to impusers
GO
