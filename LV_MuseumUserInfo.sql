USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_MuseumUserInfo]'))
    DROP VIEW [dbo].[LV_MuseumUserInfo]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_MuseumUserInfo] AS
SELECT  usr.[userid],
        ISNULL(usr.[fname],'') AS [fname],
        ISNULL(usr.[lname],'') AS [lname],
        LTRIM(ISNULL(usr.[fname],'') + ' ' + ISNULL(usr.[lname],'')) as [full_name],
        LTRIM(ISNULL(usr.[lname],'') + ', ' + ISNULL(usr.[fname],'')) as [sort_name],
        ISNULL(usr.[email_address], '') as [email_address],
        ISNULL(usr.[location],'') AS [location],
        ISNULL(usg.[UG_name], 'None') as [default_user_group],
        usr.[last_login_dt],
        ISNULL(datediff(day,usr.[last_login_dt], getdate()), -1) as [days_since_last_login],
        usr.[pw_expr_dt],
        usr.[locked_ind],
        usr.[num_logins_allowed],
        usr.[inactive],
        ISNULL(usr.[worker_customer_no],0) AS [worker_customer_no]
FROM [dbo].[T_METUSER] as usr
     LEFT OUTER JOIN [dbo].[TX_USER_GROUP] as txg (NOLOCK) ON txg.[userid] = usr.[userid] and txg.[default_ind] = 'Y'
     LEFT OUTER JOIN [dbo].[T_METUSERGROUP] as usg (NOLOCK) ON usg.[UG_id] = txg.[UG_id]
GO

GRANT SELECT ON [dbo].[LV_MuseumUserInfo] to impusers
GO
