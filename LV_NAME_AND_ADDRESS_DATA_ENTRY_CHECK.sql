USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_KEYWORDS]'))
    DROP VIEW [dbo].[LV_NAME_AND_ADDRESS_DATA_ENTRY_CHECK]
GO

CREATE VIEW [dbo].[LV_NAME_AND_ADDRESS_DATA_ENTRY_CHECK] AS
    SELECT cus.[customer_no],
           CASE WHEN ISNULL(cus.[lname],'') = '' THEN '?'
                WHEN ASCII(LEFT(ISNULL(cus.[lname],''),1)) BETWEEN 65 AND 90 THEN LEFT(cus.[lname],1)
                WHEN ASCII(LEFT(ISNULL(cus.[lname],''),1)) BETWEEN 97 AND 122 THEN UPPER(LEFT(cus.[lname],1))
                WHEN ASCII(LEFT(ISNULL(cus.[lname],''),1)) BETWEEN 48 AND 57 THEN '#'
                ELSE '?' END AS [customer_grouping],
           cus.[cust_type] AS [customer_type_no],
           ctp.[description] AS [customer_type],
           ISNULL(cus.[fname],'') AS [first_name],
           [dbo].[LFS_PotentialCaseProblem] (ISNULL(cus.[fname],''),'N') AS [first_name_PP],
           ISNULL(cus.[mname],'') AS [middle_name],
           [dbo].[LFS_PotentialCaseProblem] (ISNULL(cus.[mname],''),'N') AS [middle_name_PP],
           ISNULL(cus.[lname],'') AS [last_name],
           [dbo].[LFS_PotentialCaseProblem] (ISNULL(cus.[lname],''),'N') AS [last_name_PP],
           ISNULL(adr.[address_no],0) AS [address_no],
           ISNULL(adr.[address_type],0) AS [address_type_no],
           CASE WHEN ISNULL(adr.[address_no],0) = 0 THEN 'Missing'
                ELSE ISNULL(atp.[description],'') END AS [address_type],
           ISNULL(adr.[inactive],'') AS [inactive],
           ISNULL(adr.[primary_ind],'N') AS [primary_ind],
           CASE WHEN ISNULL(adr.[address_no],0) > 0 THEN 1 ELSE 0 END AS [address_counter],
           CASE WHEN ISNULL(adr.[address_no],0) > 0 AND ISNULL(adr.[inactive],'') = 'N' THEN 1 ELSE 0 END AS [active_address_counter],
           CASE WHEN ISNULL(adr.[address_no],0) > 0 and ISNULL(adr.[primary_ind],'N') = 'Y' THEN 1 ELSE 0 END AS [primary_address_counter],
           ISNULL(adr.[street1],'') AS [street1],
           [dbo].[LFS_PotentialCaseProblem] (ISNULL(adr.[street1],''),'N') AS [street1_PP],
           ISNULL(adr.[street2],'') AS [street2],
           [dbo].[LFS_PotentialCaseProblem] (ISNULL(adr.[street2],''),'N') AS [street2_PP],
           ISNULL(adr.[street2],'') AS [street3],
           [dbo].[LFS_PotentialCaseProblem] (ISNULL(adr.[street3],''),'N') AS [street3_PP],
           ISNULL(adr.[city],'') AS [city],
           [dbo].[LFS_PotentialCaseProblem] (ISNULL(adr.[city],''),'N') AS [city_PP],
           ISNULL(adr.[state],'') AS [state],
           [dbo].[LFS_PotentialCaseProblem] (ISNULL(adr.[state],''),'Y') AS [state_PP],
           ISNULL(adr.[postal_code],'') AS [postal_code],
           CASE WHEN ISNULL(adr.[postal_code],'') = '' THEN 'Missing'
                WHEN DATALENGTH(ISNULL(adr.[postal_code],'')) < 5 THEN 'Invalid'
                WHEN DATALENGTH(ISNULL(adr.[postal_code],'')) > 9 THEN 'Invalid'
                ELSE 'None' END AS [postal_code_PP]
    FROM [dbo].[T_CUSTOMER] AS cus 
         LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] AS ctp ON ctp.[id] = cus.[cust_type]
         LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr ON adr.[customer_no] = cus.[customer_no]
         LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS atp ON atp.[id] = adr.[address_type]
    WHERE CHARINDEX('non-Constit', ISNULL(cus.[fname],'')) = 0 
GO
    
GRANT SELECT ON [dbo].[LV_NAME_AND_ADDRESS_DATA_ENTRY_CHECK] TO ImpUsers
GO

--SELECT * FROM [dbo].[LV_NAME_AND_ADDRESS_DATA_ENTRY_CHECK] WHERE [customer_no] = 10185

SELECT * FROM [dbo].[LV_NAME_AND_ADDRESS_DATA_ENTRY_CHECK] WHERE [customer_type_no] = 1 and [first_name_PP] <> 'None'




