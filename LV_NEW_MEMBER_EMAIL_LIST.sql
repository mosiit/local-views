USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_NEW_MEMBER_EMAIL_LIST] AS
WITH [cte_email_login] ([login_email])
AS (SELECT DISTINCT [login] FROM [dbo].[T_CUST_LOGIN]
    WHERE [login_type] = 1 AND [inactive] = 'N' AND [login] NOT LIKE '%kiosk%@mos.org')
SELECT 'A0' AS [affiliation_type],
	   mem.[customer_no] AS [order__member_id],
       mem.[customer_no],
       mem.[memb_level],
       lev.[description] AS [order__level_description],
       CAST(mem.[create_dt] AS DATE) AS [custom_date], --Per Scot: Create Date, not Init Date
       CAST(mem.[init_dt] AS DATE) AS [init_dt],
       CASE WHEN mem.[NRR_status] = 'NE' THEN 'New'
            ELSE 'Renew/Rejoin' END AS [order__NRR_Status],
       mem.[current_status],
       sta.[description] AS [current_status_name],
       cus.[display_name] AS [member_name],
       ISNULL(sal.[lsal_desc],'') AS [order__salutation],
       CASE WHEN eml.[address] LIKE '%kiosk%@mos.org' THEN ''
            WHEN eml.[address] IS NULL THEN '' 
            ELSE ISNULL(eml.[address], '') END AS [address],
       ISNULL(eml.[primary_ind], 'N') AS [email_primary_ind],
       ISNULL(eml.[inactive], 'Y') AS [email_inactive],
       ISNULL(shp.[description], '') AS [ship_method],
        CASE WHEN ISNULL(shp.[description],'') = 'one step' THEN 'Y'
             ELSE 'N' END AS [order__one_step],
        CASE WHEN lgn.[login] LIKE '%kiosk%@mos.org' THEN ''
             WHEN ISNULL(lgn.[login], '') <> '' THEN lgn.[login]
             ELSE ISNULL(elg.login_email,'') END AS [order__login],
        CASE WHEN lgn.[login] LIKE '%kiosk%@mos.org' THEN 'N'
             WHEN ISNULL(lgn.[login],'') = '' THEN 'N' 
             ELSE 'Y' END AS [order__login_option2],
        CASE WHEN ISNULL(elg.login_email,'') = '' THEN 'N'
             ELSE 'Y' END AS [order__login_option3]
FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK)
    INNER JOIN [dbo].[TR_CURRENT_STATUS] AS sta (NOLOCK) ON sta.[id] = mem.[current_status]
    INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS cus ON cus.[customer_no] = mem.[customer_no]
    LEFT OUTER JOIN [dbo].[T_EADDRESS] AS eml (NOLOCK) ON eml.[customer_no] = mem.[customer_no] AND ISNULL(eml.[primary_ind], 'N') = 'Y' AND eml.inactive = 'N'
    LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev (NOLOCK) ON mem.[memb_level] = lev.[memb_level]
    LEFT OUTER JOIN [dbo].[TX_CUST_SAL] AS sal (NOLOCK) ON sal.[customer_no] = mem.[customer_no] AND sal.[signor] = 0
    LEFT OUTER JOIN [dbo].[TR_SHIP_METHOD] AS shp (NOLOCK) ON shp.[id] = mem.[ship_method] AND mem.[ship_method] = 3
    LEFT OUTER JOIN [dbo].[T_CUST_LOGIN] AS lgn (NOLOCK) ON lgn.[customer_no] = mem.[customer_no] AND lgn.[login] = eml.[address] AND lgn.[login_type] = 1
    LEFT OUTER JOIN [cte_email_login] AS elg (NOLOCK) ON elg.[login_email] = eml.[address]
WHERE mem.memb_org_no = 4 -- household memberships
      AND mem.[current_status] IN ( 2, 3 ) -- active and pending memberships
	  AND mem.ben_provider = 0
UNION ALL
SELECT 'A1' AS [affiliation_type],
	   aff.[group_customer_no] AS [order__member_id],
       aff.[individual_customer_no],
       mem.[memb_level],
       lev.[description] AS [order__level_description],
       CAST(mem.[create_dt] AS DATE) AS [custom_date], --Per Scot: Create Date, not Init Date
       CAST(mem.[init_dt] AS DATE) AS [init_dt],
       CASE WHEN mem.[NRR_status] = 'NE' THEN 'New'
            ELSE 'Renew/Rejoin' END AS [order__NRR_Status],
       mem.[current_status],
       sta.[description] AS [current_status_name],
       ca1.[display_name] AS [member_name],
       ISNULL(sal.[lsal_desc],'') AS [order__salutation],
       CASE WHEN ea1.[address] LIKE '%kiosk%@mos.org' THEN ''
            WHEN ea1.[address] IS NULL THEN '' 
            ELSE ISNULL(ea1.[address], '') END AS [address],
       ISNULL(ea1.[primary_ind], 'N') AS [email_primary_ind],
       ISNULL(ea1.[inactive], 'Y') AS [email_inactive],
       ISNULL(shp.[description], '') AS [ship_method],
        CASE WHEN ISNULL(shp.[description],'') = 'one step' THEN 'Y'
             ELSE 'N' END AS [order__one_step],
        CASE WHEN lgn.[login] LIKE '%kiosk%@mos.org' THEN ''
             WHEN ISNULL(lgn.[login], '') <> '' THEN lgn.[login]
             ELSE ISNULL(elg.login_email,'') END AS [order__login],
        CASE WHEN lgn.[login] LIKE '%kiosk%@mos.org' THEN 'N'
             WHEN ISNULL(lgn.[login],'') = '' THEN 'N' 
             ELSE 'Y' END AS [order__login_option2],
        CASE WHEN ISNULL(elg.login_email,'') = '' THEN 'N'
             ELSE 'Y' END AS [order__login_option3]
FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK)
    INNER JOIN [dbo].[TR_CURRENT_STATUS] AS sta (NOLOCK) ON sta.[id] = mem.[current_status]
    INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS cus ON cus.[customer_no] = mem.[customer_no]
	LEFT OUTER JOIN [dbo].[T_EADDRESS] AS eml (NOLOCK) ON eml.[customer_no] = mem.[customer_no] AND ISNULL(eml.[primary_ind], 'N') = 'Y' AND eml.inactive = 'N'
    LEFT OUTER JOIN [dbo].[T_AFFILIATION] AS aff (NOLOCK) ON aff.[group_customer_no] = mem.[customer_no] AND aff.[affiliation_type_id] = 10002 AND aff.name_ind = -1
    INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS ca1 ON ca1.[customer_no] = aff.[individual_customer_no]
    LEFT OUTER JOIN [dbo].[T_EADDRESS] AS ea1 (NOLOCK) ON ea1.[customer_no] = aff.[individual_customer_no] AND ISNULL(ea1.[primary_ind], 'N') = 'Y'
    LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev (NOLOCK) ON mem.[memb_level] = lev.[memb_level]
    LEFT OUTER JOIN [dbo].[TX_CUST_SAL] AS sal (NOLOCK) ON sal.[customer_no] = aff.[individual_customer_no] AND sal.[signor] = 0
    LEFT OUTER JOIN [dbo].[TR_SHIP_METHOD] AS shp (NOLOCK) ON shp.[id] = mem.[ship_method] AND mem.[ship_method] = 3
    LEFT OUTER JOIN [dbo].[T_CUST_LOGIN] AS lgn (NOLOCK) ON lgn.[customer_no] = aff.[individual_customer_no] AND lgn.[login] = ea1.[address] AND lgn.[login_type] = 1
    LEFT OUTER JOIN [cte_email_login] AS elg (NOLOCK) ON elg.[login_email] = ea1.[address]
WHERE mem.memb_org_no = 4 -- household memberships
      AND mem.[current_status] IN ( 2, 3 ) -- active and pending memberships
	  AND mem.ben_provider = 0
UNION ALL
SELECT 'A2' AS [affiliation_type],
	   aff.[group_customer_no] AS [order__member_id],
       aff.[individual_customer_no],
       mem.[memb_level],
       lev.[description] AS [order__level_description],
       CAST(mem.[create_dt] AS DATE) AS [custom_date], --Per Scot: Create Date, not Init Date
       CAST(mem.[init_dt] AS DATE) AS [init_dt],
       CASE WHEN mem.[NRR_status] = 'NE' THEN 'New'
            ELSE 'Renew/Rejoin' END AS [order__NRR_Status],
       mem.[current_status],
       sta.[description] AS [current_status_name],
       ca2.[display_name] AS [member_name],
       ISNULL(sal.[lsal_desc],'') AS [order__salutation],
       CASE WHEN ea2.[address] LIKE '%kiosk%@mos.org' THEN ''
            WHEN ea2.[address] IS NULL THEN '' 
            ELSE ISNULL(ea2.[address], '') END AS [address],
       ISNULL(ea2.[primary_ind], 'N') AS [email_primary_ind],
       ISNULL(ea2.[inactive], 'Y') AS [email_inactive],
       ISNULL(shp.[description], '') AS [ship_method],
        CASE WHEN ISNULL(shp.[description],'') = 'one step' THEN 'Y'
             ELSE 'N' END AS [order__one_step],
        CASE WHEN lgn.[login] LIKE '%kiosk%@mos.org' THEN ''
             WHEN ISNULL(lgn.[login], '') <> '' THEN lgn.[login]
             ELSE ISNULL(elg.login_email,'') END AS [order__login],
        CASE WHEN lgn.[login] LIKE '%kiosk%@mos.org' THEN 'N'
             WHEN ISNULL(lgn.[login],'') = '' THEN 'N' 
             ELSE 'Y' END AS [order__login_option2],
        CASE WHEN ISNULL(elg.login_email,'') = '' THEN 'N'
             ELSE 'Y' END AS [order__login_option3]
FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK)
    INNER JOIN [dbo].[TR_CURRENT_STATUS] AS sta (NOLOCK) ON sta.[id] = mem.[current_status]
    INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS cus ON cus.[customer_no] = mem.[customer_no]
    LEFT OUTER JOIN [dbo].[T_EADDRESS] AS eml (NOLOCK) ON eml.[customer_no] = mem.[customer_no] AND ISNULL(eml.[primary_ind], 'N') = 'Y' AND eml.inactive = 'N'
    LEFT OUTER JOIN [dbo].[T_AFFILIATION] AS aff (NOLOCK) ON aff.[group_customer_no] = mem.[customer_no] AND aff.[affiliation_type_id] = 10002 AND aff.name_ind = -2
    INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS ca2 ON ca2.[customer_no] = aff.[individual_customer_no]
    LEFT OUTER JOIN [dbo].[T_EADDRESS] AS ea2 (NOLOCK) ON ea2.[customer_no] = aff.[individual_customer_no] AND ISNULL(ea2.[primary_ind], 'N') = 'Y'
    LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev (NOLOCK) ON mem.[memb_level] = lev.[memb_level]
    LEFT OUTER JOIN [dbo].[TX_CUST_SAL] AS sal (NOLOCK) ON sal.[customer_no] = aff.[individual_customer_no] AND sal.[signor] = 0
    LEFT OUTER JOIN [dbo].[TR_SHIP_METHOD] AS shp (NOLOCK) ON shp.[id] = mem.[ship_method] AND mem.[ship_method] = 3
    LEFT OUTER JOIN [dbo].[T_CUST_LOGIN] AS lgn (NOLOCK) ON lgn.[customer_no] = aff.[individual_customer_no] AND lgn.[login] = ea2.[address] AND lgn.[login_type] = 1
    LEFT OUTER JOIN [cte_email_login] AS elg (NOLOCK) ON elg.[login_email] = ea2.[address]
WHERE mem.memb_org_no = 4 -- household memberships
      AND mem.[current_status] IN ( 2, 3 ) -- active and pending memberships
	  AND mem.ben_provider = 0
GO

--SELECT * FROM [dbo].[LV_NEW_MEMBER_EMAIL_LIST]