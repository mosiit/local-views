USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_NS_LARGEST_GP]'))
    DROP VIEW [dbo].[LV_NS_LARGEST_GP]
GO

CREATE VIEW [dbo].[LV_NS_LARGEST_GP] AS
SELECT  n1.[customer_no]
      , n1.apply_date
      , SUM(n1.value) AS 'sum_value'
FROM [dbo].[LT_NIGHTLY_SUMMARY] AS n1 
WHERE n1.[sort_order] in (8, 11, 14) 
  AND n1.[value] = (SELECT MAX([value]) FROM [dbo].[LT_NIGHTLY_SUMMARY] AS n2 WHERE n2.[customer_no] = n1.[customer_no] AND n2.[sort_order] in (8,11,14) GROUP BY n2.[customer_no]) 
  AND n1.[apply_date] = (SELECT MAX([apply_date]) FROM [dbo].[LT_NIGHTLY_SUMMARY] as n2 WHERE n1.[customer_no] = n2.[customer_no] AND n1.[value] = n2.[value] AND n2.[sort_order] in (8,11,14) GROUP BY n2.[customer_no])
GROUP BY n1.[customer_no], n1.[apply_date]
GO

GRANT SELECT ON [dbo].[LV_NS_LARGEST_GP] TO ImpUsers
GO


SELECT * FROM [dbo].[LV_NS_LARGEST_GP]
GO


