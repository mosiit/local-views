USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_NS_LATEST_GP]'))
    DROP VIEW [dbo].[LV_NS_LATEST_GP]
GO


CREATE VIEW [dbo].[LV_NS_LATEST_GP] AS
SELECT n1.[customer_no]
     , n1.[apply_date]
     , SUM(n1.[value]) AS 'sum_value'
FROM [dbo].[LT_NIGHTLY_SUMMARY] AS n1 
WHERE n1.[sort_order] in (9, 12, 15) 
  AND n1.[apply_date] = (SELECT MAX([apply_date]) FROM [dbo].[LT_NIGHTLY_SUMMARY] as n2 WHERE n1.[customer_no] = n2.[customer_no] AND n2.[sort_order] in (9,12,15) GROUP BY n2.[customer_no]) 
GROUP BY n1.[customer_no], n1.[apply_date]
GO

GRANT SELECT ON [dbo].[LV_NS_LATEST_GP] TO impUsers
GO

SELECT * FROM [dbo].[LV_NS_LATEST_GP]
