USE [impresario];
GO

-- =================================================================
-- Author:		Aileen Duffy-Brown, Tessitura Network Tech Services
-- Create date: 12/11/2018
-- Description:	Orders where pass/entitlement price 
-- type groups 2 & 8 are used
-- =================================================================

SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
GO

ALTER VIEW [dbo].[LV_ORDERS_WITH_ENTITLEMENTS]
AS
SELECT s.[sli_no],
       s.[order_no],
       s.[perf_no],
       s.[sli_status],
       CASE WHEN s.[sli_status] IN ( 2, 3, 11, 12 ) THEN 'Used'
            WHEN s.[sli_status] IN ( 4, 7, 8, 13 ) THEN 'Returned'
            ELSE 'Other' END AS [pass_status],
       s.[li_seq_no],
       s.[last_updated_by],
       s.[last_update_dt],
       p.[description] AS [price_type_desc],
       p.[short_desc],
       p.[price_type_group],
       o.[customer_no]
FROM [dbo].[T_SUB_LINEITEM] AS s
    INNER JOIN [dbo].[TR_PRICE_TYPE] AS p ON p.[id] = s.[price_type]
    INNER JOIN [dbo].[T_ORDER] AS o ON o.[order_no] = s.[order_no]
WHERE p.[price_type_group] IN ( 2, 8 );

GO
GRANT SELECT ON [dbo].[LV_ORDERS_WITH_ENTITLEMENTS] TO ImpUsers;
GO

