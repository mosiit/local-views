USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_ORDER_DETAIL]'))
    DROP VIEW [dbo].[LV_ORDER_DETAIL]
GO

CREATE VIEW [dbo].[LV_ORDER_DETAIL] AS
SELECT  sli.[order_no]
       ,sli.[sli_no]
       ,sli.[li_seq_no]
       ,ord.[customer_no]
       ,sli.[created_by]
       ,sli.[create_dt]
       ,sli.[create_loc]
       ,IsNull(mac.[machine_description], sli.[create_loc]) as 'create_loc_description'
       ,sli.[last_updated_by]
       ,sli.[last_update_dt]
       ,sli.[sli_status] as 'sli_status_no'
       ,sls.[description] as 'sli_status_name'
	   ,prf.performance_code AS perf_code
       ,prf.[performance_type] AS 'performance_type_no'
       ,prf.[performance_type_name]
       ,prf.[title_no]
       ,prf.[title_name]
       ,prf.[production_no]
       ,prf.[production_name]
       ,prf.[season_no]
       ,prf.[season_name]
       ,sli.[perf_no]
       ,sli.[zone_no]
       ,zon.[description] as 'zone_name'
       ,prf.[performance_dt]
       ,prf.[performance_date]
       ,prf.[performance_time]
       ,sli.[price_type] as 'price_type_no'
       ,pty.[description] as 'price_type_name'
       ,pty.[short_desc] as 'price_type_name_short'
       ,IsNull(sli.[comp_code],0) as 'comp_code_no'
       ,IsNull(cmp.[description],'None') as 'comp_code_name'
       ,Isnull(convert(varchar(30),cmp.[short_desc]), 'None') as 'comp_code_name_short'
       ,sli.[due_amt] as 'due_amount'
       ,[dbo].[LFS_GET_PERFORMANCE_PRICE] (sli.[perf_no], sli.[zone_no], sli.[price_type], sli.[create_dt]) as 'current_price'
       ,IsNull(rtn.[due_amt], 0.00) as 'returned_amount'
       ,(sli.[due_amt] + IsNull(rtn.[due_amt], 0.00)) as 'total_amount'
       ,sli.[paid_amt] as 'paid_amount'
       ,IsNull(rtn.[paid_amt], 0.00) as 'returned_payment'
       ,(sli.[paid_amt] + IsNull(rtn.[paid_amt], 0.00)) as 'total_paid'
       ,sli.[fee_amt]
       ,sli.[fee_parent_sli_no]
       ,sli.[seat_no]
       ,IsNull(sli.[ticket_no], 0) as 'ticket_no'
       ,sli.[batch_no]
       ,IsNull(sli.[ret_parent_sli_no], 0) as 'ret_parent_sli_no'
       ,IsNull(sli.[recipient_no], 0) as 'recipient_no'
       ,rtrim(IsNull(cus.[fname], '') + ' ' + IsNull(cus.[lname], '')) as 'recipient_name'
       ,isNull(sli.[rule_id], 0) as 'rule_id'
       ,IsNull(sli.[rule_ind], '') as 'rule_ind'
       ,IsNull(rul.[description], '') as 'rule_name'
       ,sli.[original_price_type]
	   ,ord.class AS order_category
	   ,ord.initiator_no
	   ,ord.MOS  
FROM [dbo].[T_SUB_LINEITEM] as sli (NOLOCK)
     LEFT OUTER JOIN [dbo].[T_SUB_LINEITEM] as rtn (NOLOCK) ON IsNull(rtn.[ret_parent_sli_no], 0) = sli.[sli_no] and isnull(rtn.[sli_status], 0) = 4
     LEFT OUTER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
     LEFT OUTER JOIN [dbo].[TR_SLI_STATUS] as sls (NOLOCK) ON sls.[id] = sli.[sli_status]
     LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] as pty (NOLOCK) ON pty.[id] = sli.[price_type]
     LEFT OUTER JOIN [dbo].[TR_COMP_CODE] as cmp (NOLOCK) ON cmp.[id] = sli.[comp_code]
     LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] as prf (NOLOCK) ON prf.[performance_no] = sli.[perf_no] and prf.[performance_zone] = sli.[zone_no]
     LEFT OUTER JOIN [dbo].[T_CUSTOMER] as cus (NOLOCK) ON cus.[customer_no] = IsNull(sli.[recipient_no], 0)
     LEFT OUTER JOIN [dbo].[T_PRICING_RULE] as rul ON rul.[id] = IsNull(sli.[rule_id], 0)
     LEFT OUTER JOIN [dbo].[T_ZONE] as zon ON zon.[zone_no] = sli.[zone_no]
     LEFT OUTER JOIN [dbo].[LTR_MACHINE_MAP] as mac ON mac.[machine_name] = sli.[create_loc]
GO

GRANT SELECT ON [dbo].[LV_ORDER_DETAIL] TO impusers
GO


--SELECT * FROM [LV_ORDER_DETAIL] WHERE order_no = 202757
--SELECT * FROM [dbo].[LV_CURRENT_PRICES] WHERE Perf_no = 5568
--SELECT * FROM T_SUB_LINEITEM WHERE perf_no = 5980
