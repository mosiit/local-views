USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_ORDER_FEES]'))
    DROP VIEW [dbo].[LV_ORDER_FEES]
GO

CREATE VIEW [dbo].[LV_ORDER_FEES] AS
SELECT sli.[id],
       sli.[order_no],
       sli.[customer_no],
       sli.[created_by],
       sli.[fee_no],
       fee.[description] AS [fee_name],
       fee.[season] AS [fee_season],
       sea.[description] AS [season_name],
       fee.[fiscal_year],
       fee.[fee_gl_no],
       fee.[campaign_no],
       cmp.[description] AS [campaign_name],
       cmp.[fyear] AS [campaign_fiscal_year],
       fee.[category] AS [fee_category_no],
       cat.[description] AS [fee_category_name],
       fee.[fee_amt] AS [fee_default_amt],
       sli.[fee_override_ind],
       CASE WHEN sli.[fee_override_ind] = 'Y' THEN sli.[fee_override_amt]
            ELSE sli.[fee_amt] END AS [fee_amt_due],
       sli.[fee_amt_paid],
       sli.[user_defined_ind],
       ISNULL(sli.[fee_frequency],'') AS [fee_frequency],
       CASE WHEN sli.[fee_override_ind] = 'Y' and sli.[fee_amt_paid] = [sli].[fee_override_amt] THEN 12
            WHEN sli.[fee_override_ind] = 'N' and sli.[fee_amt_paid] = [sli].[fee_amt] THEN 12
            ELSE 2 END AS [fee_sli_status],
        CASE WHEN sli.[fee_override_ind] = 'Y' and sli.[fee_amt_paid] = [sli].[fee_override_amt] THEN 'Ticketed, Paid'
             WHEN sli.[fee_override_ind] = 'N' and sli.[fee_amt_paid] = [sli].[fee_amt] THEN 'Ticketed Paid'
             ELSE 'Seated, Unpaid' END AS [fee_sli_status_name]
FROM [dbo].[T_SLI_FEE] AS sli
     INNER JOIN [dbo].[T_FEE] AS fee ON fee.[fee_no] = sli.[fee_no]
     INNER JOIN [dbo].[TR_SEASON] AS sea ON sea.[id] = fee.[season]
     INNER JOIN [dbo].[T_CAMPAIGN] AS cmp ON cmp.[campaign_no] = fee.[campaign_no]
     INNER JOIN [dbo].[TR_FEE_CATEGORY] AS cat ON cat.[id] = fee.[category]
GO


--SELECT [order_no], COUNT(*) FROM [dbo].[T_SLI_FEE] WHERE [fee_frequency] IS NULL GROUP BY [order_no] HAVING COUNT(*) <> 1
--SELECT * FROM [dbo].[T_SLI_FEE] WHERE [order_no] = 364417

--SELECT * FROM [dbo].[LV_ORDER_DETAIL] WHERE order_no = 1553159
--SELECT * FROM [dbo].[LV_ORDER_FEES] WHERE order_no = 1553159

