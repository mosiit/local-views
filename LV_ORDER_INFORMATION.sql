USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_ORDER_INFORMATION]'))
    DROP VIEW [dbo].[LV_ORDER_INFORMATION]
GO

CREATE VIEW [dbo].[LV_ORDER_INFORMATION] AS
SELECT    ord.[order_no] AS 'order_no'
        , ord.[appeal_no] AS 'order_appeal_no'
        , app.[description] AS 'order_appeal_name'
        , ord.[customer_no] AS 'order_customer_no'
        , LTRIM(RTRIM(ISNULL(cus.[fname],'') + ' ' 
                + CASE WHEN ISNULL(cus.[mname],'') = '' THEN '' ELSE cus.[mname] + ' ' END 
                + ISNULL(cus.[lname],''))) AS 'order_customer_name'
        , ISNULL(ord.[initiator_no],0) AS 'order_initiator_no'
        , LTRIM(RTRIM(ISNULL(ini.[fname],'') + ' ' 
                + CASE WHEN ISNULL(ini.[mname],'') = '' THEN '' ELSE ini.[mname] + ' ' END 
                + ISNULL(ini.[lname],''))) AS 'order_initiator_name'
        , ord.[solicitor] AS 'order_solicitor'
        , LTRIM(RTRIM(ISNULL(sol.[fname],'') + ' ' + ISNULL(sol.[lname],''))) AS 'order_solicitor_name'
        , ord.[created_by] AS 'order_created_by'
        , LTRIM(RTRIM(ISNULL(cre.[fname],'') + ' ' + ISNULL(cre.[lname],''))) AS 'order_created_by_name'
        , ord.[create_dt] AS 'order_create_dt'
        , CONVERT(CHAR(10),[ord].[create_dt],111) AS 'order_create_date'
        , CONVERT(CHAR(8),[ord].[create_dt],108) AS 'order_create_time'
        , ord.[MOS] AS 'order_mode_of_sale'
        , mos.[description] AS 'order_mode_of_sale_name'
        , ord.[channel] AS 'order_channel'
        , cha.[description] AS 'order_channel_name'
        , ord.[hold_until_dt] AS 'order_hold_until_dt'
        , CONVERT(CHAR(10),ord.[hold_until_dt],111) AS 'order_hold_until_date'
        , CONVERT(CHAR(8),ord.[hold_until_dt],108) AS 'order_hold_until_time'
        , CASE WHEN ord.[customer_no] = 0 THEN 'Y' ELSE 'N' END AS 'order_is_annonymous'
        , CASE WHEN ord.[customer_no] = 0 THEN 'Annonymous' ELSE 'Known' END AS 'order_annonymous_indicator'
        , CASE WHEN  ord.[customer_no] = 0 THEN 1 ELSE 0 END AS 'order_counter_annonymous'
        , CASE WHEN  ord.[customer_no] = 0 THEN 0 ELSE 1 END AS 'order_counter_known'
        , 1 AS 'order_counter'
        , ord.[tot_due_amt] AS 'order_total_due'
        , ord.[tot_paid_amt] AS 'order_total_paid'
        , ord.[tot_due_amt] - ord.[tot_paid_amt] AS 'order_balance_due'
        , ord.[transaction_no] AS 'order_transaction_number'
        , ISNULL(ord.[notes],'') AS 'order_notes'
FROM [dbo].[T_ORDER] AS ord (NOLOCK)
     LEFT OUTER JOIN [dbo].[T_APPEAL] AS app (NOLOCK) ON app.[appeal_no] = ord.[appeal_no]
     LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
     LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS ini (NOLOCK) ON ini.[customer_no] = ord.[initiator_no]
     LEFT OUTER JOIN [dbo].[T_METUSER] AS sol (NOLOCK) ON sol.[userid] = ord.[solicitor]
     LEFT OUTER JOIN [dbo].[T_METUSER] AS cre (NOLOCK) ON cre.[userid] = ord.[created_by]
     LEFT OUTER JOIN [dbo].[TR_MOS] AS mos (NOLOCK) ON mos.[id] = ord.[MOS]
     LEFT OUTER JOIN [dbo].[TR_SALES_CHANNEL] AS cha (NOLOCK) ON cha.[id] = ord.[channel]
GO

GRANT SELECT ON [dbo].[LV_ORDER_INFORMATION] TO ImpUsers
GO

--SELECT * FROM [dbo].[LV_ORDER_INFORMATION] WHERE [order_create_date] between '2016/12/01' AND '2016/12/31' and order_total_due = 0.0



--SELECT * FROM T_CUSTOMER WHERE customer_no = 3240654
--SELECT * FROM T_CUSTOMER WHERE customer_no = 3242865

