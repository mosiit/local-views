USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_ORDER_PAYMENTS]'))
    DROP VIEW [dbo].[LV_ORDER_PAYMENTS]
GO

CREATE VIEW [dbo].[LV_ORDER_PAYMENTS] AS
SELECT   trn.[order_no]
        ,IsNull(trn.[sequence_no], 0) as 'transaction_sequence_no'
        ,IsNull(trn.[transaction_no], 0) as 'transaction_no'
        ,trn.[create_dt] as 'transaction_create_dt'
        ,ISNull(trn.[created_by], '') as 'transaction_created_by'
        ,IsNull(trn.[create_loc], '') as 'transaction_create_loc'
        ,IsNull(mac.[machine_description], IsNull(trn.[create_loc], '')) as 'transaction_create_loc_description'
        ,IsNull(trn.[ref_no], 0) as 'transaction_reference_no'
        ,trn.[trn_dt] as 'transaction_dt'
        ,IsNull(trn.[trn_type], '') as 'transaction_type_no'
        ,IsNull(tty.[description], '') as 'transaction_type_name'
        ,IsNull(trn.[trn_amt], 0.00) as 'transaction_amount'
        ,IsNull(trn.[posted_status], '') as 'transaction_posted'
        ,IsNull(trn.[batch_no], 0) as 'transaction_batch_no'
        ,IsNull(trn.[tckt_tran_flag], '') as 'tckt_tran_flag'
        ,ISNULL(trn.[perf_no],0) AS 'transaction_perf_no'
        ,IsNull(pay.[payment_no], 0) as 'payment_no'
        ,IsNull(pay.[sequence_no], 0) as 'payment_sequence_no'
        ,IsNull(pay.[created_by], '') as 'payment_created_by'
        ,IsNull(pay.[create_dt], trn.[create_dt]) as 'payment_create_dt'
        ,IsNull(pay.[pmt_dt], trn.[trn_dt]) as 'payment_dt'
        ,IsNull(pay.[pmt_amt], 0.00) as 'payment_amount'
        ,IsNull(pay.[pmt_method], 0) as 'payment_method_no'
        ,IsNull(pme.[description], '') as 'payment_method_name'
        ,ISNULL(pme.[pmt_type], 0) AS 'payment_type_no'
        ,ISNULL(pty.[description], '') AS 'payment_type_name'
        ,IsNull(pay.[account_no], '') as 'payment_account_no'
        ,IsNull(pay.[check_no], 0) as 'payment_check_no'
        ,IsNull(pay.[check_name], '') as 'payment_check_name'
        ,IsNull(pay.[ccref_no], '') as 'payment_cc_reference_no'
        ,IsNull(pay.[tendered_amt], 0.00) as 'payment_tendered_amount'
        ,IsNull(pay.[act_id], 0) as 'payment_act_no'
        ,IsNull(pay.[customer_no], 0) as 'payment_customer_no'
        ,CASE WHEN IsNull(cus.[fname], '') = '' THEN '' ELSE (cus.[fname] + ' ') END 
                    + CASE WHEN IsNull(cus.[mname], '') = '' THEN '' ELSE (cus.[mname] + ' ') END 
                    + IsNull(cus.[lname], '') as 'payment_customer_name'
       ,IsNull(pay.[notes], '') as 'payment_notes'
FROM [dbo].[T_TRANSACTION] as trn (NOLOCK)
     LEFT OUTER JOIN [dbo].[TR_TRANSACTION_TYPE] As tty (NOLOCK) ON tty.[id] = trn.[trn_type]
     LEFT OUTER JOIN [dbo].[T_PAYMENT] as pay (NOLOCK) ON pay.[transaction_no] = trn.[transaction_no] and pay.[sequence_no] = trn.[sequence_no]
     LEFT OUTER JOIN [dbo].[TR_PAYMENT_METHOD] as pme (NOLOCK) ON pme.[id] = pay.[pmt_method]
     LEFT OUTER JOIN [dbo].[TR_PAYMENT_TYPE] AS pty (NOLOCK) ON pty.[id] = pme.[pmt_type]
     LEFT OUTER JOIN [dbo].[T_CUSTOMER] as cus (NOLOCK) ON cus.[customer_no] = IsNull(pay.[customer_no], 0)
     LEFT OUTER JOIN [dbo].[LTR_MACHINE_MAP] as mac ON mac.[machine_name] = trn.[create_loc]
GO

GRANT SELECT ON [dbo].[LV_ORDER_PAYMENTS] TO impusers
GO


--SELECT * FROM T_PAYMENT
--SELECT * FROM dbo.TR_PAYMENT_METHOD
--SELECT * FROM [dbo].[LV_ORDER_PAYMENTS] WHERE [order_no]  = 228869