USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_OUTSTANDING_PLEDGES]'))
    DROP VIEW [dbo].[LV_OUTSTANDING_PLEDGES]
GO

CREATE VIEW [dbo].[LV_OUTSTANDING_PLEDGES] AS
SELECT  con.[ref_no]
       ,con.[customer_no]
       ,con.cont_dt
       ,cmp.[fyear]
       ,cat.[description] as 'campaign_category'
       ,con.[cont_amt]
       ,con.[recd_amt]
       ,fnd.[description] as 'fund_name'
       ,fnd.[nonrestricted_income_gl_no]
       ,con.custom_4
       ,con.custom_3
FROM T_CONTRIBUTION as con (NOLOCK)
     INNER JOIN T_CAMPAIGN as cmp (NOLOCK) ON cmp.[campaign_no] = con.[campaign_no]
     INNER JOIN TR_CAMPAIGN_CATEGORY as cat (NOLOCK) ON cat.[id] = cmp.[category]
     INNER JOIN T_FUND as fnd (NOLOCK) ON fnd.[fund_no] = con.[fund_no]
WHERE con.[cont_type] = 'P' 
  and con.[custom_4] <> 'Converted Match Pledge' 
  and con.[cont_amt] - con.[recd_amt] > 0
GO

GRANT SELECT ON [dbo].[LV_OUTSTANDING_PLEDGES] TO impusers
GO

SELECT * FROM [dbo].[LV_OUTSTANDING_PLEDGES]


