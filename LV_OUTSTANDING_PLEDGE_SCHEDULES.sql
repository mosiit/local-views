USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_OUTSTANDING_PLEDGE_SCHEDULES]'))
    DROP VIEW [dbo].[LV_OUTSTANDING_PLEDGE_SCHEDULES]
GO

CREATE VIEW [dbo].[LV_OUTSTANDING_PLEDGE_SCHEDULES] AS
SELECT  con.[ref_no]
       ,con.[customer_no]
       ,sal.[esal1_desc]
       ,con.[cont_dt]
       ,cmp.[fyear]
       ,cc.[description] as 'campaign_category'
       ,con.[cont_amt]
       ,con.[recd_amt]
       ,fnd.[description] as 'fund_name'
       ,fnd.[nonrestricted_income_gl_no]
       ,con.[custom_4]
       ,con.[custom_3]
       ,sch.[sch_no]
       ,sch.[amt_due]
       ,sch.[due_dt]
       ,sch.[status]
       ,sch.[amt_recd]
       ,sch.[transaction_no]
       ,sch.[rec_status]
FROM [dbo].[T_CONTRIBUTION] as con (NOLOCK)
     INNER JOIN [dbo].[T_CAMPAIGN] as cmp (NOLOCK) ON cmp.campaign_no = con.campaign_no
     INNER JOIN [dbo].[TR_CAMPAIGN_CATEGORY] as cc (NOLOCK) ON cc.id = cmp.category
     INNER JOIN [dbo].[T_FUND] as fnd (NOLOCK) ON fnd.fund_no = con.fund_no
     INNER JOIN [dbo].[T_SCHEDULE] as sch (NOLOCK) ON sch.ref_no = con.ref_no
     INNER JOIN [dbo].[TX_CUST_SAL] as sal (NOLOCK) ON sal.customer_no = con.customer_no
WHERE con.cont_type = 'P' 
  and con.cont_amt - recd_amt > 0 
  and con.custom_4 <> 'Converted Match Pledge' 
  and sal.default_ind = 'Y'
GO

GRANT SELECT ON [dbo].[LV_OUTSTANDING_PLEDGE_SCHEDULES] TO impusers
GO


SELECT * FROM [dbo].[LV_OUTSTANDING_PLEDGE_SCHEDULES] ORDER BY ref_no, sch_no
