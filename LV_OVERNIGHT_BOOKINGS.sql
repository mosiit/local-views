USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_OVERNIGHT_BOOKINGS]'))
DROP VIEW [dbo].[LV_OVERNIGHT_BOOKINGS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_OVERNIGHT_BOOKINGS] AS
    WITH [on_category] ([overnight_dt], [booking_category])
    AS (SELECT DISTINCT CONVERT(DATE,bkg.[default_datetime]), rsr.[description] 
        FROM dbo.T_RESOURCE AS rsr (NOLOCK)
             LEFT OUTER JOIN [dbo].[T_BOOKING_ASSIGNMENT] AS asn (NOLOCK) ON asn.[resource_id] = rsr.[id]
             LEFT OUTER JOIN [dbo].[T_BOOKING] AS bkg (NOLOCK) ON bkg.[id] = asn.[booking_id]
             LEFT OUTER JOIN [dbo].[TR_RESOURCE_TYPE] AS rtp (NOLOCK) ON rtp.[id] = asn.[resource_type]
        WHERE rtp.[description] = 'ON Category')
    SELECT prf.[performance_no], 
           prf.[performance_zone],
           CONVERT(DATE,prf.[performance_dt]) AS [overnight_dt], 
           CONVERT(DATE,bkg.[default_datetime]) AS [booking_dt], 
           prf.[performance_time], 
           prf.[title_no], 
           prf.[title_name], 
           prf.[production_name],
           REPLACE(ISNULL(onc.[booking_category],''), ' Overnight', '') AS [overnight_category],
           sli.[order_no],
           ord.[customer_no],
           ord.[booking_no],
           bkg.[description],
           ISNULL(bkg.[notes],'') AS [notes],
           bkg.[default_datetime],
           LTRIM(ISNULL(cus.[fname],'') + ' ' + ISNULL(cus.[lname],'')) AS [group_name],
           LTRIM(ISNULL(ini.[fname],'') + ' ' + ISNULL(ini.[lname],'')) AS [contact_name],
           ISNULL(ini.[fname],'') AS [contact_first_name],
           adr.[city] AS [group_city],
           adr.[state] AS [group_state],
           ISNULL(ord.[custom_4],'') AS [departure_time],
           count(sli.[sli_no]) AS [attending]
    FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK)
         LEFT OUTER JOIN [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK) ON sli.[perf_no] = prf.[performance_no] AND sli.[zone_no] = prf.[performance_zone]
         INNER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
         LEFT OUTER JOIN [dbo].[T_BOOKING] AS bkg (NOLOCK) ON bkg.[id] = ord.[booking_no]
         LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
         LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr (NOLOCK) ON adr.[customer_no] = ord.[customer_no] AND adr.[primary_ind] = 'Y'
         LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS ini (NOLOCK) ON ini.[customer_no] = ord.[initiator_no]
         LEFT OUTER JOIN [on_category] AS onc (NOLOCK) ON onc.[overnight_dt] = CONVERT(DATE,bkg.[default_datetime])
    WHERE prf.[title_no] = 61              -- 61 = Overnight Programs
      AND sli.[sli_status] IN (2, 3, 12)   -- 2 = Seated Unpaid / 3 = Seated Paid / 12 = Ticketed Paid
      AND ord.[customer_no] <> 3741534     -- 3741534 = Overnight Schedule Constituent Record
    GROUP BY prf.[performance_no], prf.[performance_zone], prf.[performance_dt], prf.[performance_date], prf.[performance_time], prf.[title_no], 
             prf.[title_name], prf.[production_name], onc.[booking_category], sli.[order_no], ord.[customer_no], ord.[booking_no], bkg.[description], ISNULL(bkg.[notes],''), 
             bkg.[default_datetime], LTRIM(ISNULL(cus.[fname],'') + ' ' + ISNULL(cus.[lname],'')), LTRIM(ISNULL(ini.[fname],'') + ' ' + ISNULL(ini.[lname],'')),
             ISNULL(ini.[fname],''), adr.[city], adr.[state], ISNULL(ord.[custom_4],'')
    UNION ALL
    SELECT 0 AS [performance_no], 
           0 AS [performance_zone],
           CONVERT(DATE,bkg.[default_datetime]) AS [overnight_dt], 
           CONVERT(DATE,bkg.[default_datetime]) AS [booking_dt], 
           CONVERT(DATE,bkg.[default_datetime]) AS [performance_time], 
           61 as [title_no], 
           'Overnight Programs' AS [title_name],
           cat.[description] AS [production_name],
           REPLACE(ISNULL(onc.[booking_category],''), ' Overnight', '') AS [overnight_category],
           ord.[order_no],
           ord.[customer_no],
           ord.[booking_no],
           bkg.[description],
           ISNULL(bkg.[notes],'') AS [notes],
           bkg.[default_datetime],
           LTRIM(ISNULL(cus.[fname],'') + ' ' + ISNULL(cus.[lname],'')) AS [group_name],
           '' AS [contact_name],
           '' AS [contact_first_name],
           '' AS [group_city],
           '' AS [group_state],
           '' AS [departure_time],
           0 AS [attending]
    FROM [dbo].[T_ORDER] AS ord (NOLOCK)
         INNER JOIN [dbo].[T_BOOKING] AS bkg (NOLOCK) ON bkg.[id] = ord.[booking_no]
         INNER JOIN [dbo].[TR_BOOKING_CATEGORY] AS cat (NOLOCK) ON cat.[id] = bkg.[category]
         LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
         LEFT OUTER JOIN [on_category] AS onc (NOLOCK) ON onc.[overnight_dt] = CONVERT(DATE,bkg.[default_datetime])
    WHERE ord.[customer_no] = 3741534      -- 3741534 = Overnight Schedule Constituent Record

GO

GRANT SELECT ON [dbo].[LV_OVERNIGHT_BOOKINGS] TO ImpUsers
GO

SELECT * FROM [dbo].[LV_OVERNIGHT_BOOKINGS] WHERE overnight_dt = '6-7-2018'
GO
