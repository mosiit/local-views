USE [impresario]
GO

/****** Object:  View [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]    Script Date: 9/19/2018 2:26:01 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_OVERNIGHT_DATE_NOTES]'))
DROP VIEW [dbo].[LV_OVERNIGHT_DATE_NOTES]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_OVERNIGHT_DATE_NOTES] AS
    WITH [perf_list] ([perf_no]) 
    AS (SELECT DISTINCT [performance_no] FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE title_no IN (61,7051))
    SELECT csi.[activity_no],
           csi.[issue_dt],
           csi.[perf_no],
           CONVERT(DATE,prf.[perf_dt]) AS [overnight_dt],
           atp.[category] AS [activity_category_no],
           cat.[description] AS [activity_category],
           csi.[activity_type] AS [activity_type_no],
           atp.[description] AS [activity_type],
           csi.[customer_no],
           csi.[contact_type] AS [contact_type_no],
           ctp.[description] AS [contact_type],
           LTRIM(ISNULL(cus.[fname],'') + ' ' + ISNULL(cus.[lname],'')) AS [group_name],
           csi.[origin] AS [activity_origin_no],
           ori.[description] AS [activity_origin],
           csi.[notes] AS [date_notes]
    FROM [dbo].[T_CUST_ACTIVITY] AS csi (NOLOCK)
         INNER JOIN [dbo].[T_PERF] AS prf (NOLOCK) ON prf.[perf_no] = csi.[perf_no]
         INNER JOIN [perf_list] AS lst (NOLOCK) ON lst.[perf_no] = prf.[perf_no]
         INNER JOIN [dbo].[TR_CUST_ACTIVITY_TYPE] AS atp (NOLOCK) ON atp.[id] = csi.[activity_type]
         INNER JOIN [dbo].[TR_CUST_ACTIVITY_CATEGORY] AS cat (NOLOCK) ON cat.[id] = atp.[category]
         INNER JOIN [dbo].[TR_CONTACT_TYPE] AS ctp (NOLOCK) ON ctp.[id] = csi.[contact_type]
         LEFT OUTER JOIN [dbo].[TR_ORIGIN] AS ori (NOLOCK) ON ori.[id] = csi.[origin]
         LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = csi.[customer_no]
    WHERE atp.[category] = 37    --37 = Overnight Programs
GO

SELECT * FROM [dbo].[LV_OVERNIGHT_DATE_NOTES] WHERE [overnight_dt] = '11-9-2018' --AND activity_type IN ('24 Hour Power','Sleeping Space Request','Special Requests')
--SELECT * FROM [dbo].[LV_OVERNIGHT_DATE_NOTES] WHERE [activity_type] = 'Date Notes' AND customer_no <> 3741534

