USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_OVERNIGHT_INSTRUCTOR_SCHEDULE]'))
DROP VIEW [dbo].[LV_OVERNIGHT_INSTRUCTOR_SCHEDULE]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_OVERNIGHT_INSTRUCTOR_SCHEDULE] AS 
    SELECT  ROW_NUMBER() OVER(PARTITION BY asn.[overnight_dt], asn.[assigned_resource] ORDER BY asn.[overnight_dt]) AS [Row_num], 
            asn.[booking_no], 
            asn.[overnight_dt],
            asn.[day_of_week],
            asn.[assigned_resource],
            asn.[resource_notes],
            asn.[resource_schedule],
            'E' AS [schedule_shift],
            'Early' AS [schedule_shift_time],
            '8:00 PM to 9:00 PM' AS [schedule_shift_label],
            rsr.[worker_customer_no],
            LTRIM(ISNULL(cus.[fname],'') + ' ' + ISNULL(cus.[lname],'')) AS [schedule_instructor]
    FROM [dbo].[LV_OVERNIGHT_RESOURCE_ASSIGNMENTS] AS asn (NOLOCK)
         LEFT OUTER JOIN [dbo].[LTR_OVERNIGHT_SCHEDULE] AS sch (NOLOCK) ON sch.[schedule_dt] = asn.[overnight_dt] AND sch.[first_activity] = asn.[resource_id]
         LEFT OUTER JOIN [dbo].[LV_MOS_RESOURCES] AS rsr (NOLOCK) ON rsr.[id] = sch.[schedule_staff]
         LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = rsr.[worker_customer_no]
    WHERE asn.[resource_type_no] = 13           --13 = Drop-In Activity
      AND ISNULL(rsr.[worker_customer_no],0) > 0  
    UNION ALL
    SELECT  ROW_NUMBER() OVER(PARTITION BY asn.[overnight_dt], asn.[assigned_resource] ORDER BY asn.[overnight_dt]) AS [Row_num],
            asn.[booking_no], 
            asn.[overnight_dt],
            asn.[day_of_week],
            asn.[assigned_resource],
            asn.[resource_notes],
            asn.[resource_schedule],
            'L' AS [schedule_shift],
            'Late' AS [schedule_shift_time],
            '9:00 PM to 10:00 PM' AS [schedule_shift_label],
            rsr.[worker_customer_no],
            LTRIM(ISNULL(cus.[fname],'') + ' ' + ISNULL(cus.[lname],''))
    FROM [dbo].[LV_OVERNIGHT_RESOURCE_ASSIGNMENTS] AS asn (NOLOCK)
         LEFT OUTER JOIN [dbo].[LTR_OVERNIGHT_SCHEDULE] AS sch (NOLOCK) ON sch.[schedule_dt] = asn.[overnight_dt] AND sch.[second_activity] = asn.[resource_id]
         LEFT OUTER JOIN [dbo].[LV_MOS_RESOURCES] AS rsr (NOLOCK) ON rsr.[id] = sch.[schedule_staff]
         LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = rsr.[worker_customer_no]
    WHERE asn.[resource_type_no] = 13       --13 = Drop-In Activity
      AND ISNULL(rsr.[worker_customer_no],0) > 0
GO

    GRANT SELECT ON [dbo].[LV_OVERNIGHT_INSTRUCTOR_SCHEDULE] TO impusers
GO

    --SELECT * FROM [dbo].[LV_OVERNIGHT_INSTRUCTOR_SCHEDULE] WHERE overnight_dt = '10-18-2018' AND assigned_resource = 'Puff Mobiles'

--SELECT * FROM LTR_OVERNIGHT_SCHEDULE
--SELECT * FROM dbo.TR_RESOURCE_TYPE