USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_OVERNIGHT_RESOURCE_ASSIGNMENTS]'))
DROP VIEW [dbo].[LV_OVERNIGHT_RESOURCE_ASSIGNMENTS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

    CREATE VIEW [dbo].[LV_OVERNIGHT_RESOURCE_ASSIGNMENTS] AS
        WITH [presentations] ([id], [description], [abbreviation],[abbreviation_sort])
        AS (SELECT  [id], 
                    [description],
                    CASE WHEN [description] = 'Manager On Duty' THEN 'MOD'
                         WHEN [description] = 'Head Overnight' THEN 'HON'
                         WHEN [description] = 'Overnight' THEN 'ON'
                         WHEN [description] like '%Swing' THEN 'SWING'
                         WHEN [description] like 'Opening Welcome%' THEN 'OW'
                         WHEN [description] = 'Lightning Show' THEN 'TOE'
                         WHEN [description] like 'Planetarium%' THEN 'PLA'
                         WHEN [description] = 'Lunar Landers' THEN 'LUN'
                         ELSE [description] END AS [abbreviation],
                    CASE WHEN [description] IN ('Manager On Duty','MOD') THEN 'A'
                         WHEN [description] IN ('Head Overnight', 'HON') THEN 'B'
                         WHEN [description] IN ('Overnight', 'ON') THEN 'C'
                         WHEN [description] like '%Swing' THEN 'D'
                         WHEN [description] like 'Opening Welcome%' OR [description] = 'OW' THEN 'R'
                         WHEN [description] = 'Lightning Show' OR [description] = 'TOE' THEN 'S'
                         WHEN [description] like 'Planetarium%' OR [description] = 'PLA' THEN 'T'
                         WHEN [description] = 'Lunar Landers' THEN 'U'
                         ELSE 'Z' END AS [abbreviation_sort]
            FROM [dbo].[TR_QUALIFICATION])
        SELECT  bkg.[booking_no],
                bkg.[order_no],
                bkg.[customer_no],
                bkg.[group_name],
                bkg.[overnight_dt],
                bkg.[overnight_category],
                asn.[id] AS [assignment_id],
                asn.[booking_id],
                asn.[resource_type] AS [resource_type_no],
                rtp.[category],
                cat.[description] AS [resource_category],
                rtp.[description] AS [resource_type],
                asn.[resource_id],
                CASE WHEN rtp.[description] = 'Omni' THEN ISNULL(rsr.[description],'')
                     ELSE ISNULL(qua.[description],rtp.[description]) END AS [resource_name],
                CASE WHEN rtp.[description] = 'Registration Assistant' THEN 'REG'
                     WHEN rtp.[description] = 'Intern' THEN 'INT'
                     WHEN rtp.[description] = 'Volunteer' THEN 'VOL'
                     ELSE ISNULL(pre.[abbreviation],'') END AS [resource_name_abbreviated],
                CASE WHEN rtp.[description] = 'Registration Assistant' THEN 'G'
                     WHEN rtp.[description] = 'Intern' THEN 'E'
                     WHEN rtp.[description] = 'Volunteer' THEN 'F'
                     WHEN ISNULL(pre.[abbreviation_sort],'') = '' THEN 'Z'
                     ELSE ISNULL(pre.[abbreviation_sort],'') END AS [resource_sort],
                CASE WHEN ISNULL(asn.[notes],'') <> '' THEN asn.[notes] 
                     WHEN ISNULL(rsr.[notes],'') <> '' THEN rsr.[notes]
                     ELSE ISNULL(def.[job_schedule],'') END AS [resource_schedule],
                ISNULL(rsr.[worker_customer_no],0) AS [resource_worker_no],
                CASE WHEN rtp.[category] = -3 THEN LTRIM(ISNULL(cus.[fname],'') + ' ' + ISNULL(cus.[lname],'')) 
                     WHEN rtp.[description] = 'Omni' THEN ''
                     ELSE ISNULL(rsr.[description],'') END AS [assigned_resource],
                DATENAME(WEEKDAY,sch.[start_dt]) AS [day_of_week],
                ISNULL(rsr.[notes],'') AS [resource_notes],
                sch.[start_dt],
                ISNULL(sch.[count],0) AS [scheduled],
                asn.[qualifications]
        FROM [dbo].[LV_OVERNIGHT_BOOKINGS] AS bkg (NOLOCK)
             INNER JOIN dbo.T_BOOKING_ASSIGNMENT AS asn (NOLOCK) ON asn.[booking_id] = bkg.[booking_no]
             LEFT OUTER JOIN [dbo].[TR_QUALIFICATION] AS qua (NOLOCK) ON qua.[id] = asn.[qualifications]
             LEFT OUTER JOIN [dbo].[T_RESOURCE] AS rsr (NOLOCK) ON rsr.[id] = asn.[resource_id]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = rsr.[worker_customer_no]
             LEFT OUTER JOIN [dbo].[T_RESOURCE_SCHEDULE] AS sch (NOLOCK) ON sch.[booking_assignment_id] = asn.[id]
             LEFT OUTER JOIN [presentations] AS pre  (NOLOCK) ON pre.[id] = qua.[id]
             INNER JOIN [dbo].[TR_RESOURCE_TYPE] AS rtp (NOLOCK) ON rtp.[id] = asn.[resource_type]
             LEFT OUTER JOIN [dbo].[LTR_OVERNIGHT_SCHEDULE_DEFAULTS] AS def (NOLOCK) ON def.[job_name] = CASE WHEN rtp.[description] = 'Omni' THEN ISNULL(rsr.[description],'') 
                                                                                                              ELSE ISNULL(qua.[description],rtp.[description]) END
             INNER JOIN [dbo].[TR_RESOURCE_CATEGORY] AS cat (NOLOCK) ON cat.[id] = rtp.[category]
        
GO

GRANT SELECT ON [dbo].[LV_OVERNIGHT_RESOURCE_ASSIGNMENTS] TO ImpUsers
GO

--SELECT * FROM [dbo].[LV_OVERNIGHT_RESOURCE_ASSIGNMENTS] WHERE [overnight_dt] = '10-18-2018' AND resource_type = 'Drop-In Activity' AND assigned_resource = 'Puff Mobiles'
--SELECT ORDER_NO, resource_type, resource_name, resource_name_abbreviated, resource_sort, assigned_resource, resource_schedule, resource_notes 
--SELECT * FROM [dbo].[LV_OVERNIGHT_RESOURCE_ASSIGNMENTS] WHERE overnight_dt = '6-7-2018' AND assigned_resource = 'Buses Drop-off'
--SELECT [booking_no], [assigned_resource] FROM [dbo].[LV_OVERNIGHT_RESOURCE_ASSIGNMENTS] (NOLOCK) WHERE [customer_no] = 3739865 AND [resource_name] = 'ON Category'
         

         
--         SELECT * FROM dbo.T_RESOURCE WHERE id = 1