USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_PAYMENT_GL_NUMBERS]'))
    DROP VIEW [dbo].[LV_PAYMENT_GL_NUMBERS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_PAYMENT_GL_NUMBERS] AS
    SELECT pay.[payment_no],
           pay.[sequence_no],
           pay.[transaction_no],
           pay.[order_no],
           pay.[perf_no],
           pay.[title_no],
           pay.[title_name],
           ISNULL(gln.[gl_account_no],'') AS [gl_account_no],
           ISNULL(gln.[gl_description],'') AS [gl_account_description]
    FROM [dbo].[LV_PERFORMANCE_PAYMENT_INFO] AS pay (NOLOCK)
         LEFT OUTER JOIN [dbo].[T_PERF_PRICE_TYPE] AS pty (NOLOCK) ON pty.[id] = pay.pmap_no
         LEFT OUTER JOIN [dbo].[T_GL_ACCOUNT] AS gln (NOLOCK) ON gln.[id] = pty.[gl_no]
GO

GRANT SELECT ON [dbo].[LV_PAYMENT_GL_NUMBERS] TO ImpUsers
GO


--SELECT * FROM dbo.LV_PAYMENT_GL_NUMBERS WHERE payment_no = 1394447 AND sequence_no = 2142755
        