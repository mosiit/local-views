USE [impresario]
GO

/****** Object:  View [dbo].[LV_PENDING_MEMBERSHIP_INFO]    Script Date: 11/7/2016 11:56:55 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP VIEW [dbo].[LV_PENDING_MEMBERSHIP_INFO]
GO

CREATE VIEW [dbo].[LV_PENDING_MEMBERSHIP_INFO]
AS
	SELECT  cm.customer_no,
			cm.cust_memb_no AS customer_memb_no,
			-- Lifted from table function 'FT_CONSTITUENT_DISPLAY_NAME'
			CASE WHEN COALESCE(c.fname, '') <> '' THEN COALESCE(p.description + ' ', '') + c.fname + ' ' + CASE WHEN LEN(c.mname) > 0 THEN c.mname + ' '
																												ELSE ''
																										   END + c.lname + CASE WHEN s.id > 0 THEN ', ' + s.description
																																ELSE ''
																														   END
				 ELSE c.lname
			END AS customer_display_name,
			CASE WHEN COALESCE(c.fname, '') <> '' THEN c.fname + ' ' + c.lname + CASE WHEN s.id > 0 THEN ', ' + s.description
																					  ELSE ''
																				 END
				 ELSE c.lname
			END AS customer_display_name_short,
			cm.campaign_no AS campaign_no,
			ca.[description] AS campaign_desc,
			mlc.[description] AS memb_level_category_description,
			mo.[description] AS memb_org_description,
			cm.memb_level AS memb_level_code,
			ml1.[description] AS memb_level_description,
			cm.init_dt AS initiation_date,
			cm.inception_dt AS inception_date,
			cm.expr_dt AS expiration_date,
			cm.rein_dt AS reinstated_date,
			cm.renew_dt AS renew_date,
			cm.susp_dt AS suspended_date,
			cm.cancel_dt AS cancel_date,
			cm.lapse_dt AS lapse_date,
			cm.orig_memb_level AS original_memb_level_code,
			ml2.[description] AS original_memb_level_description,
			cm.orig_expiry_dt AS original_expiry_date,
			--CASE WHEN cm.cur_record = 'Y' THEN 'Yes'
			--	 ELSE 'No'
			--END AS current_record,
			--cs.[description] AS current_status_desc,
			cm.memb_amt,
			cm.recog_amt AS recognition_amt,
			cm.AVC_amt AS avc_amt,
			ml1.admission_adult + ml1.admission_child + ml1.admission_other AS 'entries_allowed',
			CASE WHEN cm.ben_provider > 0 THEN 'Yes'
				 ELSE 'No'
			END AS benefactor_provider,
			CASE WHEN cm.ben_holder_ind = 'Y' THEN 'Yes'
				 ELSE 'No'
			END AS benefit_holder,
			CASE WHEN cm.declined_ind = 'Y' THEN 'Yes'
				 ELSE 'No'
			END AS declined_benefits,
			CASE cm.NRR_status
				WHEN 'NE' THEN 'New'
				WHEN 'RN' THEN 'Renewed'
				WHEN 'RI' THEN 'Reinstated'
			END AS nrr_status,
			cm.memb_trend AS memb_trend,
			mt1.[description] AS memb_trend_desc,
			cm.current_status AS current_status_orig,
			cs.description AS current_status_orig_desc,
			current_status = CASE cm.current_status
							   WHEN 3 THEN 2
							   ELSE cm.current_status
							 END,
			sm.[description] AS ship_method_desc,
			cm.num_copies AS nbr_copies,
			cm.notes,
			c.fname AS customer_first_name,
			c.mname AS customer_middle_name,
			c.lname AS customer_last_name,
			c.sort_name
	FROM    dbo.TX_CUST_MEMBERSHIP cm
	JOIN    dbo.T_MEMB_LEVEL ml1 ON cm.memb_org_no = ml1.memb_org_no AND cm.memb_level = ml1.memb_level
	JOIN    dbo.TR_CURRENT_STATUS cs ON cm.current_status = cs.id
	JOIN    dbo.T_MEMB_LEVEL ml2 ON cm.orig_memb_level = ml2.memb_level
	JOIN    dbo.TR_MEMB_LEVEL_CATEGORY mlc ON ml1.category = mlc.id
	JOIN    dbo.T_MEMB_ORG mo ON cm.memb_org_no = mo.memb_org_no
	JOIN    dbo.T_CAMPAIGN ca ON cm.campaign_no = ca.campaign_no
	JOIN    dbo.T_CUSTOMER c ON cm.customer_no = c.customer_no
	JOIN    dbo.TR_MEMB_TREND mt1 ON cm.memb_trend = mt1.id
	LEFT JOIN dbo.TR_SHIP_METHOD sm ON cm.ship_method = sm.id
	LEFT JOIN dbo.TR_PREFIX p WITH (NOLOCK) ON c.prefix = p.id AND p.id > 0
	LEFT JOIN dbo.TR_SUFFIX s WITH (NOLOCK) ON c.suffix = s.id AND s.id > 0
	--WHERE   (cm.cur_record = 'Y' or cm.current_status = 3)
	WHERE	cm.cur_record = 'N'
	AND		cm.current_status = 3 -- 'Pending'
	AND		cm.expr_dt > CURRENT_TIMESTAMP


GO

GRANT SELECT ON [dbo].[LV_PENDING_MEMBERSHIP_INFO] TO impusers
GO