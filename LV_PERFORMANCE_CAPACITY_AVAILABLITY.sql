USE [impresario]
GO

ALTER VIEW [dbo].[LV_PERFORMANCE_CAPACITY_AVAILABLITY]
AS

-- Calculating Capacity and InStock information for all shows for the next 30 days

WITH    CapacityCTE(perf_no, zone_no, zmap_no, capacity)
AS (
	SELECT seat.perf_no,
		seat.zone_no,
		seat.zmap_no,
		SUM(CASE seat.status_code
				WHEN 'BLK' THEN 0
				WHEN 'NS' THEN 0
				ELSE 1
			END) AS capacity
    FROM [dbo].[TX_PERF_SEAT] (NOLOCK) seat
    WHERE seat.[seat_no] >= 0
          AND seat.[pkg_no] >= 0
    GROUP BY  seat.perf_no,
            seat.zone_no,
            seat.zmap_no
)
SELECT DISTINCT 
		perf.title_name,
		perf.production_name_long,
		perf.performance_time_display,
		perf.performance_date,
		perf.performance_time,
		perf.performance_no,
		perf.production_no,
		cap.available,
		seat.capacity
FROM   CapacityCTE seat
INNER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] (NOLOCK) perf 
	ON perf.performance_no = seat.perf_no
    AND perf.performance_zone = seat.zone_no
    AND perf.performance_zone_map = seat.zmap_no
INNER JOIN dbo.T_PERF_ZONE_SUMMARY (NOLOCK) cap 
ON perf.performance_no = cap.perf_no
	AND perf.performance_zone = cap.zone_no
WHERE DATEDIFF(d, GETDATE(), perf.performance_date) between 0 and 30 ;
GO

GRANT SELECT ON  [dbo].[LV_PERFORMANCE_CAPACITY_AVAILABLITY] to impusers

