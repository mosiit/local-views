USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_PERFORMANCE_PAYMENT_INFO]'))
    DROP VIEW [dbo].[LV_PERFORMANCE_PAYMENT_INFO]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_PERFORMANCE_PAYMENT_INFO] AS
SELECT pmt.[payment_no],
       pmt.[sequence_no],
       pmt.[transaction_no],
       pmt.[pmt_dt],
       convert(char(10),pmt.[pmt_dt],111) as [pmt_date],
       trx.[trn_dt],
       convert(char(10),trx.[trn_dt],111) as [trn_date],
       bat.[posted_dt],
       convert(char(10),bat.[posted_dt],111) as [posted_date],
       trx.[perf_no],
       prf.[perf_dt],
       convert(char(10),prf.[perf_dt],111) as [perf_date],
       prf.[perf_code],
       ttl.[inv_no] AS [title_no],
       ttl.[description] as [title_name],
       prd.[description] AS [production_name],
       ISNULL(lng.[value],'') as [production_name_long],
       pmt.[pmt_amt],
       trx.[trn_amt],
       trx.[trn_type],
       typ.[description] as [trn_type_name],
       mth.pmt_type as [pmt_type],
       IsNull(pty.[description], 'Unknown') as [pmt_type_name],
       pmt.[pmt_method],
       mth.[description] as [pmt_method_name],
       trx.[ref_no],
       trx.[order_no],
       IsNull(pmt.[account_no],'') as [account_no],
       IsNull(pmt.[check_no],'') as [check_no],
       IsNull(pmt.[check_name],'') as [check_name],
       pmt.[batch_no],
       pmt.[customer_no],
       IsNull(trx.[posted_status], 'N') as [posted_status],
       IsNull(trx.[tckt_tran_flag],'N') as [tckt_tran_flag],
       CASE WHEN ISNULL(gla.[gl_no],0) = 0 THEN ISNULL(gla.[default_gl_no],0)
             ELSE gla.[gl_no] END as [default_gl_no],
       CASE WHEN ISNULL(gla.[gl_no],0) = 0 THEN ISNULL(gla.[default_gl_account],'Unknown Account')
             ELSE gla.[gl_account] END as [default_gl_account],
       gld.[gl_description] as [default_gl_description],
       pmt.[created_by] as [pmt_created_by],
       pmt.[create_dt] as [pmt_create_dt],
       trx.[create_loc] as [trx_create_loc] ,
       trx.[created_by] as [trx_created_by],
       trx.[create_dt] as [trx_create_dt],
       IsNull(pmt.[notes],'') as [pmt_notes],
       trx.[pmap_no]
FROM [dbo].[T_PAYMENT] as pmt (NOLOCK)
     LEFT OUTER JOIN [dbo].[T_TRANSACTION] as trx (NOLOCK) ON trx.[transaction_no] = pmt.[transaction_no] and trx.[sequence_no] = pmt.[sequence_no]
     LEFT OUTER JOIN [dbo].[T_BATCH] AS bat (NOLOCK) ON bat.[batch_no] = pmt.[batch_no]
     LEFT OUTER JOIN [dbo].[TR_PAYMENT_METHOD] as mth (NOLOCK) ON mth.[id] = pmt.[pmt_method]
     LEFT OUTER JOIN [dbo].[TR_PAYMENT_TYPE] as pty (NOLOCK) ON pty.[id] = mth.[pmt_type]
     LEFT OUTER JOIN [dbo].[TR_TRANSACTION_TYPE] as typ (NOLOCK) ON typ.[id] = trx.[trn_type]
     LEFT OUTER JOIN [dbo].[T_PERF] as prf (NOLOCK) ON prf.[perf_no] = trx.[perf_no]
     LEFT OUTER JOIN [dbo].[T_PERF_PRICE_LAYER] as lay (NOLOCK) ON lay.[id] = (SELECT max([id]) 
                                                                               FROM [dbo].[T_PERF_PRICE_LAYER] (NOLOCK)
                                                                               WHERE [perf_no] = prf.[perf_no])
     LEFT OUTER JOIN [dbo].[LV_PERFORMANCE_PRICE_GL_NUMBERS] AS gla ON gla.[performance_no] = prf.[perf_no]
                                                                    AND gla.[performance_zone] = (SELECT MIN([performance_zone]) 
                                                                                                  FROM [dbo].[LV_PERFORMANCE_PRICE_GL_NUMBERS] (NOLOCK)
                                                                                                  WHERE [performance_no] = prf.[perf_no])
                                                                    AND gla.[template] = lay.[template]
                                                                    AND gla.[price_type] = (SELECT MIN([id]) 
                                                                                            FROM [dbo].[TR_PRICE_TYPE]
                                                                                            WHERE [id] IN (SELECT [price_type] 
                                                                                                           FROM [dbo].[TX_TEMPLATE_PRICE_TYPE] (NOLOCK)
                                                                                                           WHERE [template] = lay.[template])
                                                                                              AND [description] NOT LIKE '%comp%'      --IGNORE COMP PRICE TYPES
                                                                                              AND [description] NOT LIKE '%member%')   --AND MEMBER PRICE TYPES BECAUSE SOMETIMES THEY ARE FREE
     LEFT OUTER JOIN [dbo].[T_GL_ACCOUNT] As gld (NOLOCK) ON gld.[id] = gla.[gl_no]
     LEFT OUTER JOIN [dbo].[T_PROD_SEASON] as sea (NOLOCK) ON sea.prod_season_no = prf.[prod_season_no]
     LEFT OUTER JOIN [dbo].[T_PRODUCTION] as pro (NOLOCK) ON pro.prod_no = sea.[prod_no]
     LEFT OUTER JOIN [dbo].[T_INVENTORY] as ttl (NOLOCK) ON ttl.[inv_no] = pro.[title_no]
     LEFT OUTER JOIN [dbo].[T_INVENTORY] as prd (NOLOCK) ON prd.[inv_no] = pro.[prod_no]
     LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] as lng (NOLOCK) ON lng.[inv_no] = pro.[prod_no] and lng.[content_type] = (SELECT MAX([id]) FROM [dbo].[TR_INV_CONTENT] WHERE [description] = 'Long Title')
     WHERE trx.[trn_type] in (31, 32, 33)        --31 = Ticket Change / 32 = Ticket Purchase / 33 = Ticket Refund
GO

GRANT SELECT ON [dbo].[LV_PERFORMANCE_PAYMENT_INFO] TO impusers
GO
