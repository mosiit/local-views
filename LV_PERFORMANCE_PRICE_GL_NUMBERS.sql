USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LV_PERFORMANCE_PRICE_GL_NUMBERS]') AND type = N'V')
    DROP VIEW [LV_PERFORMANCE_PRICE_GL_NUMBERS]
GO

CREATE VIEW [dbo].[LV_PERFORMANCE_PRICE_GL_NUMBERS] AS
SELECT prf.[performance_no],
       prf.[performance_code],
       prf.[title_no],
       prf.[title_name],
       prf.[production_no],
       prf.[production_name],
       prf.[production_season_no],
       prf.[production_season_name],
       dbo.[LF_GetFiscalYear](prf.[performance_dt]) AS [fiscal_year],
       CAST(prf.[performance_dt] AS DATE) AS [performance_dt],
       prf.[performance_zone],
       prf.[performance_time],
       CASE WHEN ISDATE(prf.[performance_zone_text]) = 1 THEN prf.[performance_time_display]
            ELSE prf.[performance_zone_text] END AS [performance_display],
       pri.[perf_price_type],
       pri.[start_price],
       pri.[start_enabled],
       pri.[edit_ind],
       ptp.[desig_code],
       dsc.[description] AS [designation],
       ptp.[price_type],
       typ.[description] AS [price_type_name],
       lay.[template],
       lay.[description],
       xtp.[gl_no] AS [default_gl_no],
       gldf.[gl_account_no] AS [default_gl_account],
       xtp.[gl_resale_no] AS [default_resale_gl_no],
       gldr.[gl_account_no] AS [default_resale_gl_account],
       ptp.[gl_no],
       glnm.[gl_account_no] AS [gl_account],
       ptp.[gl_resale_no],
       glrn.[gl_account_no] AS [resale_gl_account]
FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK)
     INNER JOIN [dbo].[T_PERF_PRICE] AS pri (NOLOCK) ON pri.[perf_no] = prf.[performance_no] AND pri.[zone_no] = prf.[performance_zone]
     INNER JOIN [dbo].[T_PERF_PRICE_TYPE] AS ptp (NOLOCK) ON ptp.[id] = pri.[perf_price_type]
     INNER JOIN [dbo].[T_PERF_PRICE_LAYER] AS lay (NOLOCK) ON lay.[id] = ptp.[perf_price_layer]
     INNER JOIN [dbo].[TX_TEMPLATE_PRICE_TYPE] AS xtp (NOLOCK) ON xtp.[template] = lay.[template] AND xtp.[price_type] = ptp.[price_type]
     INNER JOIN [dbo].[TR_DESIG_CODE] AS dsc (NOLOCK) ON dsc.[id] = ptp.[desig_code]
     INNER JOIN [dbo].[TR_PRICE_TYPE] AS typ (NOLOCK) ON typ.[id] = ptp.[price_type]
     INNER JOIN [dbo].[T_GL_ACCOUNT] AS gldf (NOLOCK) ON gldf.[id] = xtp.[gl_no]
     INNER JOIN [dbo].[T_GL_ACCOUNT] AS gldr (NOLOCK) ON gldr.[id] = xtp.[gl_resale_no]
     INNER JOIN [dbo].[T_GL_ACCOUNT] AS glnm (NOLOCK) ON glnm.[id] = ptp.[gl_no]
     INNER JOIN [dbo].[T_GL_ACCOUNT] AS glrn (NOLOCK) ON glrn.[id] = ptp.[gl_resale_no]
GO

GRANT SELECT ON [dbo].[LV_PERFORMANCE_PRICE_GL_NUMBERS] TO ImpUsers
GO

--SELECT * FROM [dbo].[LV_PERFORMANCE_PRICE_GL_NUMBERS] WHERE performance_dt = '6-5-2019'
--SELECT * FROM [dbo].[LV_PERFORMANCE_PRICE_GL_NUMBERS] WHERE performance_dt = '6-16-2019' --AND gl_account <> default_gl_account
      
      
