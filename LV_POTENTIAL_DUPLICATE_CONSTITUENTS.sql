USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_POTENTIAL_DUPLICATE_CONSTITUENTS]'))
    DROP VIEW [dbo].[LV_POTENTIAL_DUPLICATE_CONSTITUENTS]
GO

CREATE VIEW [dbo].[LV_POTENTIAL_DUPLICATE_CONSTITUENTS] AS
SELECT cus.[customer_no],
       cus.[cust_type],
       typ.[description] AS [cust_type_name],
       CASE WHEN typ.[description] LIKE '%School%' AND typ.[description] <> 'School District Official Rec' THEN 'School'
            ELSE typ.[description] END AS [customer_type],
       ISNULL(cus.[fname],'') AS [first_name],
       ISNULL(cus.[mname],'') AS [middle_name],
       ISNULL(cus.[lname],'') AS [last_name],
       nam.[display_name],
       nam.[display_name_short],
       nam.[display_name_tiny],
       nam.[sort_name],
       adr.[address_type],
       atp.[description] AS [address_type_name],
       ISNULL(adr.[street1],'') AS [street1],
       ISNULL(adr.[street2],'') AS [street2],
       ISNULL(adr.[street3],'') AS [street3],
       ISNULL(adr.[city],'') AS [city],
       ISNULL(adr.[state],'') AS [state],
       CASE WHEN LEN(ISNULL(adr.[postal_code],'')) = 9 THEN LEFT(ISNULL(adr.[postal_code],''),5)
            ELSE ISNULL(adr.[postal_code],'') END AS [postal_code],
       ISNULL(ead.[eaddress_type],'') AS [eaddress_type],
       ISNULL(etp.[description],0) AS [eaddress_type_name],
       ISNULL(ead.[address],'') [eaddress],
       CASE WHEN typ.[description] LIKE '%School%' AND typ.[description] <> 'School District Official Rec' THEN 'School'
            ELSE typ.[description] END + '_'
          + LEFT(nam.[sort_name],10) + '_' 
          + CASE WHEN LEN(ISNULL(adr.[postal_code],'')) = 9 THEN LEFT(ISNULL(adr.[postal_code],''),5)
                 ELSE ISNULL(adr.[postal_code],'') END + '_'
          + LEFT(ISNULL(adr.[street3],''),10) + '_'
          + LEFT(ISNULL(adr.[street1],''),10) AS [dupe_level_1]
FROM [dbo].[T_CUSTOMER] AS cus (NOLOCK)
     INNER JOIN [dbo].[TR_CUST_TYPE] AS typ (NOLOCK) ON typ.[id] = cus.[cust_type]
     INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = cus.[customer_no]
     INNER JOIN [dbo].[FT_GET_PRIMARY_ADDRESS]() AS adr ON adr.[customer_no]= cus.[customer_no]
     INNER JOIN [dbo].[TR_ADDRESS_TYPE] AS atp (NOLOCK) ON atp.[id] = adr.[address_type]
     LEFT OUTER JOIN [dbo].[FT_GET_PRIMARY_EADDRESS]() AS ead ON ead.[customer_no] = cus.[customer_no]
     LEFT OUTER JOIN [dbo].[TR_EADDRESS_TYPE] AS etp (NOLOCK) ON etp.[id] = ead.eaddress_type
WHERE cus.[cust_type] NOT IN (8, 19, 20)
GO

GRANT SELECT ON [LV_POTENTIAL_DUPLICATE_CONSTITUENTS] TO ImpUsers
GO

GRANT SELECT ON [LV_POTENTIAL_DUPLICATE_CONSTITUENTS] TO tessitura_app
GO

