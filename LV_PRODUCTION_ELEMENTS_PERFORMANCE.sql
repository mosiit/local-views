USE [impresario]
GO

/****** Object:  View [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]    Script Date: 2/1/2016 12:29:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
AS
      SELECT    inv.[inv_no] AS [performance_no],
                inv.[description] AS [performance_name],
                inv.[short_name] AS [performance_name_short],
                --sea.[production_name_long] AS 'performance_name_long',  --changed to use performance long title if it exists
                ISNULL(ttl.[value],sea.[production_name_long]) AS [performance_name_long],
                CASE WHEN sea.[generic_production_name] IS NULL THEN 'N'
                     WHEN sea.[production_name] = sea.[generic_production_name] THEN 'Y'
                     ELSE 'N'
                END AS [is_generic_title],
                inv.[type] AS [performance_inventory_type],
                prf.[perf_code] AS [performance_code],
                prf.[perf_dt] AS [performance_dt],
                CONVERT(CHAR(10), prf.[perf_dt], 111) AS [performance_date],
                zon.[zone_no] AS [performance_zone],
                zon.[description] AS [performance_zone_text],
                CASE WHEN isDate(zon.[description]) = 0 THEN LEFT(CONVERT(CHAR(8), prf.[perf_dt], 108), 5) ELSE zon.[description] END AS [performance_time],
                CASE WHEN isdate(zon.[description]) = 0 THEN [dbo].[LF_Format_Time](prf.[perf_dt], '12 Hour') ELSE zon.[short_desc] END AS [performance_time_display],
                CASE WHEN isdate(zon.[description]) = 0 THEN left(convert(char(8),dateadd(minute,sea.[production_duration],prf.[perf_dt]),108),5) 
                     ELSE left(convert(char(8),dateadd(minute,sea.[production_duration],zon.[description]),108),5) END as [performance_end_time],
                CASE WHEN isdate(zon.[description]) = 0 THEN [dbo].[LF_Format_Time] (dateadd(minute,sea.[production_duration],prf.[perf_dt]),'12 Hour') 
                     ELSE [dbo].[LF_Format_Time] (dateadd(minute,sea.[production_duration],zon.[description]),'12 Hour') END as [performance_end_time_display],
                prf.[doors_open] AS [performance_activity_start],
                prf.[doors_close] AS [performance_activity_end],
                sta.[inactive],
                prf.[perf_type] AS [performance_type],
                pty.[description] AS [performance_type_name],
                prf.[bsmap_no] AS [performance_best_seat_map],
                prf.[zmap_no] AS [performance_zone_map],
                map.[description] AS [performance_zone_map_name],
                prf.[facility_no] AS [performance_facility_no],
                fac.[description] AS [performance_facility_name],
                prf.[perf_status] AS [performance_status],
                pst.[description] AS [performance_status_name],
                prf.[def_start_sale_dt] AS [performance_default_start_sale_dt],
                prf.[def_end_sale_dt] AS [performance_default_end_sale_dt],
                prf.[avail_sale_ind] AS [performance_avail_sale_ind],
                prf.[create_dt] AS [performance_create_dt],
                prf.[created_by] AS [performance_created_by],
                prf.[create_loc] AS [performance_create_loc],
                prf.[last_update_dt] AS [performance_update_dt],
                prf.[last_updated_by] AS [performance_updated_by],
                ISNULL(loc.[value], sea.[season_location]) as [performance_location],
                sea.[production_season_no],
                sea.[production_season_name],
                sea.[production_season_long_title],
                sea.[production_season_inventory_type],
                sea.[production_season_short_name],
                sea.[season_no],
                sea.[season_name],
                sea.[season_start_dt],
                sea.[season_end_dt],
                sea.[season_fiscal_year],
                sea.[season_inactive],
                sea.[season_location],
                sea.[production_no],
                sea.[production_name],
                sea.[production_name_short],
                --sea.[production_name_long],  -changed to use performance long title if it exists
                ISNULL(ttl.[value],sea.[production_name_long]) AS [production_name_long],
                sea.[production_name_abbreviated],
                sea.[production_inventory_type],
                sea.[production_adjustment_hold_code_map],
                sea.[production_adjustment_hold_code_map_name],
                sea.[production_same_day_hold_code_map],
                sea.[production_same_day_hold_code_map_name],
                sea.[production_start_date],
                sea.[production_end_date],
                sea.[production_code],
                sea.[production_location],
                sea.[production_duration],
                sea.[production_auto_scan],
                sea.[production_gate_attendance],
                sea.[perf_code_suffix],
                sea.[visit_count_production],
                sea.[title_no],
                sea.[title_name],
                sea.[title_inventory_type],
                sea.[title_short_name],
                sea.[title_location],
                sea.[superscheduler_access],
                sea.[generic_production_name],
                sea.[default_facility],
                sea.[perf_code_prefix],
                sea.[quick_sale_venue],
                sea.[quick_sale_button_color],
                sea.[visit_count_title],
                sea.[capacity_reporting_title],
                sea.[recommendation_weight],
                sea.[recommended_events_public],
                sea.[recommended_events_member],
                sea.[recommended_events_kiosk],
                sea.[recommended_events_school]
      FROM      [dbo].[T_INVENTORY] AS inv
      LEFT OUTER JOIN [dbo].[T_PERF] AS prf ON prf.[perf_no] = inv.[inv_no]
      LEFT OUTER JOIN [dbo].[TR_PERF_TYPE] AS pty ON pty.[id] = prf.[perf_type]
      LEFT OUTER JOIN [dbo].[T_FACILITY] AS fac ON fac.[facil_no] = prf.[facility_no]
      LEFT OUTER JOIN [dbo].[TR_PERF_STATUS] AS pst ON pst.[id] = prf.[perf_status]
      LEFT OUTER JOIN [dbo].[T_PERF_ZONE_SUMMARY] AS pzs ON pzs.[perf_no] = prf.[perf_no] AND ISNULL(pzs.[zone_no],0) > 0
      LEFT OUTER JOIN [dbo].[T_ZMAP] AS map ON map.[zmap_no] = prf.[zmap_no]
      LEFT OUTER JOIN [dbo].[T_ZONE] AS zon ON zon.[zone_no] = pzs.[zone_no]
      LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_SEASON] AS sea ON sea.[production_season_no] = prf.[prod_season_no]
      LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_STATUS] AS sta ON sta.[performance_no] = inv.[inv_no] AND sta.[zone_no] = zon.[zone_no]
      LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] as loc ON loc.[inv_no] = inv.[inv_no] and loc.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Location')
      LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] as ttl ON ttl.[inv_no] = inv.[inv_no] and ttl.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Long Title')
      WHERE inv.[type] = 'R'
	        --and (pty.[description] in ('Live Presentation', 'Drop-In Activity') or sta.[inactive] = 'N')
            and sta.[inactive] = 'N'
            and isNull(zon.[description],'') not like '%disable%'
GO

GRANT SELECT ON  [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] to impusers, [tessitura_app]
GO
