USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_KEYWORDS]'))
    DROP VIEW [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_KEYWORDS]
GO

CREATE VIEW [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_KEYWORDS] AS
SELECT  prf.[title_no], 
        prf.[title_name], 
        prf.[production_no], 
        prf.[production_name], 
        prf.[performance_no], 
        prf.[performance_code], 
        prf.[performance_date], 
        prf.[performance_time], 
        prf.[performance_time_display],
        cat.[id] as 'keyword_category_no', 
        cat.[description] as 'keyword_category', 
        kwd.[id] as 'keyword_no', 
        kwd.[description] as 'keyword'
FROM LV_PRODUCTION_ELEMENTS_PERFORMANCE as prf
     INNER JOIN [dbo].[TX_INV_TKW] as txk ON txk.[inv_no] = prf.[performance_no]
     INNER JOIN [dbo].[TR_TKW] as kwd ON kwd.[id] = txk.[tkw]
     INNER JOIN [dbo].[TR_TKW_CATEGORY] as cat on cat.[id] = kwd.[category]
GO

GRANT SELECT ON [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_KEYWORDS] TO impusers
GO

SELECT * FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_KEYWORDS]