USE [impresario]
GO

/****** Object:  View [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_STATUS]    Script Date: 2/1/2016 1:47:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER VIEW [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_STATUS]
AS
       SELECT   prf.[perf_no] AS 'performance_no',
                zon.[zmap_no] AS 'zone_map_no',
                zon.[zone_no] AS 'zone_no',
                zon.[description] AS 'performance_time',
                zon.[short_desc] AS 'performance_time_display',
                zon.[zone_time] AS 'zone_time',
                zon.[rank] AS 'price_zone_rank',
                SUM(CASE WHEN pri.[start_enabled] = 'Y' THEN 1
                         ELSE 0
                    END) AS 'total_active_prices',
                CASE WHEN SUM(CASE WHEN pri.[start_enabled] = 'Y' THEN 1
                                   ELSE 0
                              END) = 0 THEN 'Y'
                     ELSE 'N'
                END AS 'inactive'
       FROM     [dbo].[T_PERF_ZONE_SUMMARY] AS prf
       LEFT OUTER JOIN [dbo].[T_ZONE] AS zon ON zon.[zone_no] = prf.[zone_no]
       LEFT OUTER JOIN [dbo].[T_PERF_PRICE] AS pri ON pri.[perf_no] = prf.[perf_no]
                                                      AND pri.[zone_no] = prf.[zone_no]
                                                      AND pri.[start_enabled] = 'Y'
       WHERE    ISNULL(zone_time, '') <> ''
       GROUP BY prf.[perf_no],
                zon.[zmap_no],
                zon.[zone_no],
                zon.[description],
                zon.[short_desc],
                zon.[zone_time],
                zon.[rank]



GO

GRANT SELECT ON  [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_STATUS] to impusers
GO

