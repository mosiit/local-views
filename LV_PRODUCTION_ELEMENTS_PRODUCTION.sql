USE [impresario];
GO

/****** Object:  View [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION]    Script Date: 2/1/2016 12:10:51 PM ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] AS 
SELECT  pro.[inv_no] AS 'production_no',
        pro.[description] AS 'production_name',
        ISNULL(psn.[value], '') AS 'production_name_short',
        ISNULL(pln.[value], '') AS 'production_name_long',
        ISNULL(abr.[value], '') AS 'production_name_abbreviated',
        pro.[type] AS 'production_inventory_type',
        ISNULL(CONVERT(INT, hc1.[value]), 0) AS 'production_adjustment_hold_code_map',
        ISNULL(mp1.[description], '') AS 'production_adjustment_hold_code_map_name',
        ISNULL(CONVERT(INT, hc2.[value]), 0) AS 'production_same_day_hold_code_map',
        ISNULL(mp2.[description], '') AS 'production_same_day_hold_code_map_name',
        ISNULL(sdt.[value], '') AS 'production_start_date',
        ISNULL(edt.[value], '') AS 'production_end_date',
        ISNULL(cod.[value], '') AS 'production_code',
        ISNULL(loc.[value], ttl.[title_location]) AS 'production_location',
        ISNULL(CONVERT(INT, dur.[value]), 0) AS 'production_duration',
        ISNULL(sca.[value], 'N') AS 'production_auto_scan',
        ISNULL(gat.[value], 'Y') AS 'production_gate_attendance',
        ISNULL(psu.[value], '') AS 'perf_code_suffix',
        ISNULL(vis.[value], 'N') AS 'visit_count_production',
        ISNULL(CONVERT(INT,wei.[value]), 0) AS 'Recommendation_weight',
        ISNULL(rcp.[value],'') AS 'recommended_events_public',
        ISNULL(rcm.[value],'') AS 'recommended_events_member',
        ISNULL(rck.[value],'') AS 'recommended_events_kiosk',
        ISNULL(rcs.[value],'') AS 'recommended_events_school',
        ttl.[title_no],
        ttl.[title_name],
        ttl.[title_inventory_type],
        ttl.[title_short_name],
        ttl.[superscheduler_access],
        ttl.[generic_production_name],
        ttl.[default_facility],
        ttl.[perf_code_prefix],
        ttl.[quick_sale_venue],
        ttl.[quick_sale_button_color],
        ttl.[title_location],
        ttl.[visit_count_title],
        ttl.[capacity_reporting_title]
FROM    [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] AS ttl (NOLOCK)
        LEFT OUTER JOIN [dbo].[T_PRODUCTION] AS txp (NOLOCK) ON txp.[title_no] = ttl.[title_no]
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS tgt (NOLOCK) ON tgt.[inv_no] = ttl.[title_no] AND tgt.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Generic Production')
        LEFT OUTER JOIN [dbo].[T_INVENTORY] AS pro (NOLOCK) ON pro.[inv_no] = txp.[prod_no] 
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS psn (NOLOCK) ON psn.[inv_no] = pro.[inv_no] AND psn.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Short Title')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS pln (NOLOCK) ON pln.[inv_no] = pro.[inv_no] AND pln.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Long Title')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS abr (NOLOCK) ON abr.[inv_no] = pro.[inv_no] AND abr.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Abbreviated Title')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS hc1 (NOLOCK) ON hc1.[inv_no] = pro.[inv_no] AND hc1.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'hc Map (adjust)')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS hc2 (NOLOCK) ON hc2.[inv_no] = pro.[inv_no] AND hc2.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'hc Map (same day)')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS sdt (NOLOCK) ON sdt.[inv_no] = pro.[inv_no] AND sdt.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Production Start Date')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS edt (NOLOCK) ON edt.[inv_no] = pro.[inv_no] AND edt.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Production End Date')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS cod (NOLOCK) ON cod.[inv_no] = pro.[inv_no] AND cod.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Production Code')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS psu (NOLOCK) ON psu.[inv_no] = pro.[inv_no] AND psu.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Perf Code Suffix')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS loc (NOLOCK) ON loc.[inv_no] = pro.[inv_no] AND loc.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Location')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS dur (NOLOCK) ON dur.[inv_no] = pro.[inv_no] AND dur.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Duration')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS sca (NOLOCK) ON sca.[inv_no] = pro.[inv_no] AND sca.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Production Auto Scan')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS gat (NOLOCK) ON gat.[inv_no] = pro.[inv_no] AND gat.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Production Gate Attendance')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS vis (NOLOCK) ON vis.[inv_no] = pro.[inv_no] AND vis.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Visit Count')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS wei (NOLOCK) ON wei.[inv_no] = pro.[inv_no] AND wei.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Recommendation Weight')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS rcp (NOLOCK) ON rcp.[inv_no] = pro.[inv_no] AND rcp.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Recommended Events (Public)')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS rcm (NOLOCK) ON rcm.[inv_no] = pro.[inv_no] AND rcm.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Recommended Events (Members)')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS rck (NOLOCK) ON rck.[inv_no] = pro.[inv_no] AND rck.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Recommended Events (Kiosk)')
        LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS rcs (NOLOCK) ON rcs.[inv_no] = pro.[inv_no] AND rcs.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Recommended Events (Schools)')
        LEFT OUTER JOIN [dbo].[T_HCMAP] AS mp1 (NOLOCK) ON mp1.[hcmap_no] = hc1.[value] 
        LEFT OUTER JOIN [dbo].[T_HCMAP] AS mp2 (NOLOCK) ON mp2.[hcmap_no] = hc2.[value]
WHERE   pro.[type] = 'P';
GO

GRANT SELECT ON  [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] to impusers
GO


--SELECT * FROM dbo.LV_PRODUCTION_ELEMENTS_PRODUCTION
