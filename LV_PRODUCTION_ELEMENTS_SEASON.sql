USE [impresario]
GO

/****** Object:  View [dbo].[LV_PRODUCTION_ELEMENTS_SEASON]    Script Date: 2/1/2016 12:12:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_PRODUCTION_ELEMENTS_SEASON] AS 
SELECT          pse.[inv_no] AS 'production_season_no',
                pse.[description] AS 'production_season_name',
                pse.[type] AS 'production_season_inventory_type',
                ISNULL(ttl.[value],pse.[description]) AS 'production_season_long_title',
                pse.[short_name] AS 'production_season_short_name',
                txs.[season] AS 'season_no',
                sea.[description] AS 'season_name',
                sea.[start_dt] AS 'season_start_dt',
                sea.[end_dt] AS 'season_end_dt',
                sea.[fyear] AS 'season_fiscal_year',
                sea.[inactive] AS 'season_inactive',
                ISNULL(loc.[value], pro.[production_location]) AS 'season_location',
                pro.[production_no],
                pro.[production_name],
                pro.[production_name_short],
                pro.[production_name_long],
                pro.[production_name_abbreviated],
                pro.[production_inventory_type],
                pro.[production_adjustment_hold_code_map],
                pro.[production_adjustment_hold_code_map_name],
                pro.[production_same_day_hold_code_map],
                pro.[production_same_day_hold_code_map_name],
                pro.[production_start_date],
                pro.[production_end_date],
                pro.[production_code],
                pro.[production_location],
                pro.[production_duration],
                pro.[production_auto_scan],
                pro.[production_gate_attendance],
                pro.[perf_code_suffix],
                pro.[visit_count_production],
                pro.[title_no],
                pro.[title_name],
                pro.[title_inventory_type],
                pro.[title_short_name],
                pro.[superscheduler_access],
                pro.[capacity_reporting_title],
                pro.[generic_production_name],
                pro.[default_facility],
                pro.[perf_code_prefix],
                pro.[quick_sale_venue],
                pro.[quick_sale_button_color],
                pro.[title_location],
                pro.[visit_count_title],
                pro.[recommendation_weight],
                pro.[recommended_events_public],
                pro.[recommended_events_member],
                pro.[recommended_events_kiosk],
                pro.[recommended_events_school]
       FROM     [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] AS pro
                LEFT OUTER JOIN [dbo].[T_PROD_SEASON] AS txs ON txs.[prod_no] = pro.[production_no]
                LEFT OUTER JOIN [dbo].[T_INVENTORY] AS pse ON pse.[inv_no] = txs.[prod_season_no]
                LEFT OUTER JOIN [dbo].[TR_SEASON] AS sea ON sea.[id] = txs.[season]
                LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] as loc ON loc.[inv_no] = pse.[inv_no] and loc.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Location')
                LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] as ttl ON ttl.[inv_no] = pse.[inv_no] and ttl.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Long Title')
       WHERE    pse.[type] = 'S'
GO


GRANT SELECT ON [dbo].[LV_PRODUCTION_ELEMENTS_SEASON] TO ImpUsers
GO 

--SELECT * FROM [LV_PRODUCTION_ELEMENTS_SEASON] --WHERE [production_season_name] <> [production_season_long_title]
