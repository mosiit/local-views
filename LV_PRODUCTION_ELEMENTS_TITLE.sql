USE [impresario]
GO

/****** Object:  View [dbo].[LV_PRODUCTION_ELEMENTS_TITLE]    Script Date: 2/1/2016 12:12:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[LV_PRODUCTION_ELEMENTS_TITLE]
AS
       SELECT   ttl.[inv_no] AS 'title_no',
                ttl.[description] AS 'title_name',
                ttl.[type] AS 'title_inventory_type',
                ttl.[short_name] AS 'title_short_name',
				vic.description AS 'content_type',
				tic.value AS 'content_value'
       FROM     [dbo].[T_INVENTORY] AS ttl
	   LEFT OUTER JOIN		[dbo].[TX_INV_CONTENT] tic
	   ON		tic.[inv_no] = ttl.[inv_no]
	   LEFT OUTER JOIN		[dbo].[TR_INV_CONTENT] vic
	   ON		vic.id = tic.content_type
       WHERE    ttl.[type] = 'T'

GO

GRANT SELECT ON  [dbo].[LV_PRODUCTION_ELEMENTS_TITLE] to impusers
GO