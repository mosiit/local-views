USE impresario
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_PRODUCTION_ELEMENTS_TREE]'))
    DROP VIEW [dbo].[LV_PRODUCTION_ELEMENTS_TREE]
GO

CREATE VIEW [dbo].[LV_PRODUCTION_ELEMENTS_TREE] AS
SELECT   ttl.[title_no]
        ,ivt.[description] as 'title_name'
        ,pro.[prod_no]
        ,ivp.[description] as 'production_name'
        ,sea.[prod_season_no]
        ,ivs.[description] as 'production_season_name'
        ,prf.[perf_no]
        ,prf.[perf_code]
        ,prf.[perf_dt]
        ,convert(char(10),prf.[perf_dt],111) as 'perf_date'
        ,prf.[perf_type]
        ,typ.[description] as 'perf_type_name'
        ,IsNull(prf.[sales_notes], '') as 'sales_notes'
FROM [dbo].[T_TITLE] as ttl (NOLOCK) 
     INNER JOIN [dbo].[T_INVENTORY] as ivt (NOLOCK) ON ivt.[inv_no] = ttl.[title_no]
     INNER JOIN [dbo].[T_PRODUCTION] as pro (NOLOCK) ON pro.[title_no] = ttl.[title_no]
     INNER JOIN [dbo].[T_INVENTORY] as ivp (NOLOCK) ON ivp.[inv_no] = pro.[prod_no]
     INNER JOIN [dbo].[T_PROD_SEASON] as sea (NOLOCK) ON sea.[prod_no] = pro.[prod_no]
     INNER JOIN [dbo].[T_INVENTORY] as ivs (NOLOCK) ON ivs.[inv_no] = sea.[prod_season_no]
     INNER JOIN [dbo].[T_PERF] as prf (NOLOCK) ON prf.[prod_season_no] = sea.[prod_season_no]
     INNER JOIN [dbo].[T_INVENTORY] as ivr (NOLOCK) ON ivr.[inv_no] = prf.[perf_no]
     INNER JOIN [dbo].[TR_PERF_TYPE] as typ (NOLOCK) ON typ.[id] = prf.[perf_type]
GO

GRANT SELECT ON [dbo].[LV_PRODUCTION_ELEMENTS_TREE] TO impusers
GO
