USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_QUARK_EVENT_INFO]'))
    DROP VIEW [dbo].[LV_QUARK_EVENT_INFO]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_QUARK_EVENT_INFO] AS
SELECT  m.ID
       ,convert(char(10),m.[ShowDate],111) + '_' + convert(char(8),m.[StartTime],108) + '_' + convert(varchar(10),m.[Location]) as 'unique_identifier'
       ,CASE WHEN t.[Type] IN ('CS&T','Programs') THEN 'X' WHEN t.[type] = 'Drop In Activities' THEN 'W' ELSE '' END
             + substring(convert(char(10),m.[ShowDate],111),6,2) + substring(convert(char(10),m.[ShowDate],111),9,2)
             + substring(convert(char(8),m.[StartTime],108),1,2) + substring(convert(char(8),m.[StartTime],108),4,2)
             + CASE WHEN m.[Location] = 18 THEN 'A' WHEN m.[Location] = 19 THEN 'B' WHEN m.[Location] = 46 THEN 'C'
                    WHEN m.[Location] = 68 THEN 'D' WHEN m.[Location] = 72 THEN 'E' WHEN m.[Location] = 96 THEN 'F' 
                    WHEN m.[Location] = 55 THEN 'G' WHEN m.[Location] = 59 THEN 'H' WHEN m.[Location] = 81 THEN 'I'
                    WHEN m.[Location] = 95 THEN 'J' WHEN m.[Location] = 100 THEN 'K' END as 'tessitura_performance_code'
       ,convert(char(10),m.[ShowDate],111) as 'show_date'
       ,m.[Type] as 'show_type_id'
       ,t.[Type] as 'show_type'
       ,CASE WHEN t.[Type] = 'Programs' THEN 'Live Presentations'
             WHEN t.[Type] = 'CS&T' THEN 'Live Presentations'
             WHEN t.[type] = 'Drop In Activities' THEN 'Drop-In Activities'
             ELSE '' END as 'tessitura_title'
       ,convert(char(8),m.[StartTime],108) as 'show_start_time'
       ,convert(char(8),m.[EndTime],108) as 'show_end_time'
       ,m.[ShowID]
       ,s.[AdmitsTitle] as 'tessitura_production_name'
       ,s.[Title] as 'tessitura_production_name_long'
       ,m.[Location] as 'location_id'
       ,l.[Location] as 'location_name'
       ,m.[Duration]
       ,CASE WHEN m.[Notes] is null THEN '' ELSE m.[Notes] END as 'Notes'
 FROM [TemporaryData].[dbo].[events_main] as m
      INNER JOIN [TemporaryData].[dbo].[events_showtypes] as t ON t.[ID] = m.[Type]
      INNER JOIN [TemporaryData].[dbo].[events_show] as s ON s.[ID] = m.[ShowID]
      INNER JOIN [TemporaryData].[dbo].[events_location] as l ON l.[ID] = m.[Location]
WHERE convert(char(10),m.[ShowDate],111) >= convert(char(10),getdate(),111) and t.[type] in ('CS&T', 'Programs','Drop In Activities')
      and s.[AdmitsTitle] <> ''
GO

GRANT SELECT ON [dbo].[LV_QUARK_EVENT_INFO] TO impusers
GO




--SELECT * FROM [dbo].[LV_QUARK_EVENT_INFO] ORDER BY unique_identifier
--SELECT unique_identifier FROM [dbo].[LV_QUARK_EVENT_INFO] GROUP BY unique_identifier HAVING count(*) > 1   --SHOULD RETURN *NO* RECORDS
--SELECT * FROM events.dbo.location WHERE id in (18, 19, 46, 68, 72, 96)
--SELECT DISTINCT tessitura_title, tessitura_production_name, tessitura_production_name_long   FROM [dbo].[LV_QUARK_EVENT_INFO] ORDER BY tessitura_title, tessitura_production_name
--SELECT DISTINCT location_id FROM LV_QUARK_EVENT_INFO ORDER BY location_id


