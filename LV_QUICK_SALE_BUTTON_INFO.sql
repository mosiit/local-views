USE impresario
GO

/* NOTE: Requires that all the Production Element views under Superscheduler be created first. */

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_QUICK_SALE_BUTTON_INFO]'))
    DROP VIEW [dbo].[LV_QUICK_SALE_BUTTON_INFO]
GO

CREATE VIEW [dbo].[LV_QUICK_SALE_BUTTON_INFO] AS
    SELECT   inv.[inv_no] as 'performance_no'
            ,CASE WHEN sea.[generic_production_name] is null THEN 'N' WHEN sea.[production_name] = sea.[generic_production_name] THEN 'Y' ELSE 'N' END as 'is_generic_title'
            ,prf.[perf_code] as 'performance_code'
            ,prf.[perf_dt] as 'performance_dt'
            ,convert(char(10),prf.[perf_dt],111) as 'performance_date'
            ,zon.[zone_no] as 'performance_zone'
            ,zon.[description] as 'performance_time'
            ,zon.[short_desc] as 'performance_time_display'
            ,prf.[perf_status] as 'performance_status'
            ,sea.[production_season_no]
            ,sea.[production_season_name]
            ,sea.[season_no]
            ,sea.[season_name]
            ,sea.[season_fiscal_year]
            ,sea.[season_inactive]
            ,sea.[production_no] 
            ,sea.[production_name]
            ,CASE WHEN sea.[production_name_short] = '' THEN upper(sea.[production_name]) ELSE sea.[production_name_short] END as 'production_name_short'
            ,sea.[production_name_abbreviated]
            ,sea.[production_name_long]
            ,sea.[perf_code_suffix]
            ,sea.[title_no]
            ,sea.[title_name]
            ,sea.[title_short_name]
            ,sea.[perf_code_prefix]
            ,sea.[quick_sale_venue]
            ,sea.[quick_sale_button_color]
FROM [dbo].[T_INVENTORY] as inv
     LEFT OUTER JOIN [dbo].[T_PERF] as prf (NOLOCK) ON prf.[perf_no] = inv.[inv_no]
     LEFT OUTER JOIN [dbo].[T_PERF_ZONE_SUMMARY] as pzs (NOLOCK) on pzs.[perf_no] = prf.[perf_no]
     LEFT OUTER JOIN [dbo].[T_ZONE] as zon (NOLOCK) ON zon.[zone_no] = pzs.[zone_no]
     LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_SEASON] as sea (NOLOCK) ON sea.[production_season_no] = prf.[prod_season_no]
     LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_STATUS] as sta (NOLOCK) on sta.[performance_no] = inv.[inv_no] and sta.[zone_no] = zon.[zone_no]
WHERE inv.[type] = 'R' and sea.[quick_sale_venue] <> '' and sta.[inactive] = 'N'
GO

GRANT SELECT ON [dbo].[LV_QUICK_SALE_BUTTON_INFO] TO impusers
GO

SELECT * FROM [dbo].[LV_QUICK_SALE_BUTTON_INFO]
GO


