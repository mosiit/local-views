USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_RECOMMENDATION_PRODUCTIONS]'))
    DROP VIEW [dbo].[LV_RECOMMENDATION_PRODUCTIONS]
GO

CREATE VIEW [dbo].[LV_RECOMMENDATION_PRODUCTIONS] AS 
    WITH [CTE_PRODUCTION] ([prod_no], [prod_name]) 
    AS  (
         SELECT DISTINCT sea.[prod_no], inv.[description]
         FROM [dbo].[T_PERF] AS prf
              INNER JOIN [dbo].[T_PROD_SEASON] AS sea ON sea.[prod_season_no] = prf.[prod_season_no]
              INNER JOIN [dbo].[T_INVENTORY] AS inv ON inv.[inv_no] = sea.[prod_no]
         WHERE prf.[perf_dt] > GETDATE()
            UNION SELECT rec.[production], inv.[description]
            FROM [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] AS rec
                 INNER JOIN [dbo].[T_INVENTORY] AS inv ON inv.[inv_no] = rec.[production]
        )
    SELECT pro.[title_no],
           pro.[title_name],
           pro.[production_no], 
           CASE WHEN pro.[title_no] = 1126 AND pro.[production_no] = 1127 THEN 'Household Membership'
                ELSE pro.[production_name] END AS [production_name]
    FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] AS pro
         INNER JOIN CTE_PRODUCTION AS cte ON cte.[prod_no] = pro.[production_no]
    WHERE pro.[title_no] IN (27,37,157,161,173,1113,1126,1132,1398,5542,7051,17828,84181)   --27=ExhibitHalls|37=Special Exhibitions|157=Butterfly Garden|161=Mugar Omni Theater
      AND pro.[production_name] NOT IN (pro.[generic_production_name],'Library','One Step') --173=4-D Theater|1126=Membership|1132=Hayden Planetarium|5542=Adult Offerings
      AND pro.[production_name] NOT LIKE '%Buyout%'                                         --7051=Member Events|17828=Audio Tour|1113=Summer Courses|1398=Live Presentations
                                                                                            --84181=Arctic Adventures
GO

GRANT SELECT ON [dbo].[LV_RECOMMENDATION_PRODUCTIONS] TO [ImpUsers], [tessitura_app]
GO

--SELECT * FROM [dbo].[LV_RECOMMENDATION_PRODUCTIONS] ORDER BY [title_no], [production_name]


