USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_RECOMMENDED_PRODUCTIONS]'))
    DROP VIEW [dbo].[LV_RECOMMENDED_PRODUCTIONS]
GO
CREATE VIEW [dbo].[LV_RECOMMENDED_PRODUCTIONS] AS
    WITH [CTE_CURRENT_RECOMMENDATIONS] ([prod_no], [rec_no])
    AS (
        SELECT DISTINCT [production], 
                        [rec_no]  
        FROM  (SELECT [Production], 
                      [rec_prod_01], 
                      [rec_prod_02], 
                      [rec_prod_03], 
                      [rec_prod_04], 
                      [rec_prod_05], 
                      [rec_prod_06], 
                      [rec_prod_07], 
                      [rec_prod_08], 
                      [rec_prod_09], 
                      [rec_prod_10]
                FROM [dbo].[LTR_PRODUCTION_RECOMMENDATIONS]) r  
                     UNPIVOT ([rec_no] FOR [recommendation] IN   
                             ([rec_prod_01], [rec_prod_02], [rec_prod_03], [rec_prod_04], [rec_prod_05], 
                              [rec_prod_06], [rec_prod_07], [rec_prod_08], [rec_prod_09], [rec_prod_10]))AS unpvt
       WHERE [rec_no] > 0
      )
       SELECT        0 AS [title_no],
                     '' AS [title_name],
                     0 AS [production_no],
                     '_none' AS [production_name],
                     '_none' AS [production_name_long]
        UNION SELECT [title_no],
                     [title_name],
                     [production_no],
                     [production_name],
                     [production_name_long]
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION]
        WHERE [recommendation_weight] > 0 
           OR [production_no] IN (SELECT [rec_no] FROM [CTE_CURRENT_RECOMMENDATIONS])
GO

GRANT SELECT ON [dbo].[LV_RECOMMENDED_PRODUCTIONS] TO ImpUsers
GO

--SELECT * FROM [dbo].[LV_RECOMMENDED_PRODUCTIONS] WHERE title_name like '%course%' ORDER BY title_name, production_name
