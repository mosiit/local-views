USE impresario
GO

    IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_REPORTS_RUN_SCHEDULED]'))
        DROP VIEW [dbo].[LV_REPORTS_RUN_SCHEDULED]
GO

    IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_REPORTS_RUN_ALL]'))
        DROP VIEW [dbo].[LV_REPORTS_RUN_ALL]
GO

    CREATE VIEW [dbo].[LV_REPORTS_RUN_ALL] AS
    SELECT  ISNULL(r.[report_id],'') AS [req_report_id],
            ISNULL(r.[type],'') AS [req_type],
            CASE ISNULL(r.[type],'')
                 WHEN 'f' THEN 'Foreground'
                 WHEN 'b' THEN 'Background'
                 WHEN 'h' THEN 'Metadata Row'
                 WHEN 's' THEN 'Scheduled Report'
                 ELSE ISNULL(r.type,'') END AS [req_type_name],
            r.[request_date_time] AS [req_date_time],
            r.[end_date_time] AS [req_end_date_time],
            ISNULL(DATEDIFF(SECOND,r.[request_date_time],r.[end_date_time]),0) AS [req_seconds],
            [dbo].[LFS_RUN_TIME_FROM_SECONDS] (DATEDIFF(SECOND,r.[request_date_time],r.[end_date_time])) AS [req_run_time],
            ISNULL(r.[printer_name],'') AS [req_printer_name],
            ISNULL(r.[public_flag],'') AS [req_public_flag],
            ISNULL(r.[result_code],'') AS [req_result_code],
            CASE ISNULL(r.result_code,'')
                 WHEN 'c' THEN 'Completed'
                 WHEN 's' THEN 'Save/Print Error'
                 WHEN 'd' THEN 'Database Error'
                 WHEN 't' THEN 'User Terminated'
                 WHEN 'r' THEN 'Data Window Error'
                 WHEN '' THEN 'Not Run Yet'
                 ELSE ISNULL(r.[result_code],'') END AS [req_result_code_name],
            ISNULL(r.[result_text],'') AS [req_result_text],
            ISNULL(s.[id],0) AS [sch_id],
            ISNULL(s.created_by,'') AS [sch_creator],
            ISNULL(u.[full_name],'') AS [sch_creator_name],
            ISNULL(u.[sort_name],'') AS [sch_creator_sort_name],
            ISNULL(u.[inactive],'Y') AS [sch_creator_inactive],
            s.create_dt AS [sch_create_dt],
            ISNULL(s.[name],'') AS [sch_name],
            ISNULL(s.[type],'') AS [sch_type],
            CASE ISNULL(s.[type],'')
                 WHEN 'd' THEN 'Daily'
                 WHEN 'h' THEN 'Hourly'
                 WHEN 'm' THEN 'Monthly/Weekday'
                 WHEN 'n' THEN 'Monthly/Date'
                 WHEN 'o' THEN 'One Time'
                 WHEN 'w' THEN 'Weekly'
                 ELSE ISNULL(s.[type],'') END AS [sch_type_name],
            ISNULL(s.[interval],0) AS [sch_interval],
            ISNULL(s.[day_of_week],0) AS [sch_day_of_week],
            ISNULL(s.[day_week_number],0) AS [sch_day_week_number],
            ISNULL(CONVERT(CHAR(10),s.[start_date],111),'') AS [sch_start_date],
            ISNULL(CONVERT(CHAR(10),s.[end_date],111),'') AS [sch_end_date],
            ISNULL(CONVERT(CHAR(8),s.[start_time],108),'') AS [sch_start_time],
            CASE WHEN s.[end_time] IS NULL THEN ''
                 WHEN s.[end_time] = '1900-01-01 00:00:00.000' THEN ''
                 ELSE ISNULL(CONVERT(CHAR(8),s.[end_time],108),'') END AS [sch_end_time],
            ISNULL(s.request_id,0) AS [sch_request_id],
            r.[user_id] AS [req_creator],
            ISNULL(u2.[full_name],'') AS [req_creator_name],
            ISNULL(u2.[sort_name],'') AS [req_creator_sort_name],
            ISNULL(u2.[inactive],'Y') AS [req_creator_inactive],
            ISNULL(r.[queue_status],'') AS [que_status],
            CASE ISNULL(r.[queue_status],'')
                 WHEN 's' THEN 'Scheduled'
                 WHEN 'h' THEN 'On Hold'
                 WHEN 'c' THEN 'Completed'
                 WHEN 'e' THEN 'Error'
                 ELSE ISNULL(r.[queue_status],'') END AS [que_status_name],
            r.[end_date_time] AS [que_computed_date],
            r.[created_by] AS [que_creator]
    FROM dbo.gooesoft_report_schedule AS s
         RIGHT OUTER JOIN dbo.gooesoft_request AS r ON r.[id] = s.[request_id]
         LEFT OUTER JOIN [dbo].[LV_MuseumUserInfo] AS u ON u.[userid] = s.[created_by]
         LEFT OUTER JOIN [dbo].[LV_MuseumUserInfo] AS u2 ON u2.[userid] = r.[user_id]
GO

    GRANT SELECT ON [dbo].[LV_REPORTS_RUN_ALL] TO [ImpUsers], [tessitura_app]
GO

    CREATE VIEW [dbo].[LV_REPORTS_RUN_SCHEDULED] AS
        SELECT  [sch_id],
                [sch_creator],
                [sch_creator_name],
                [sch_creator_sort_name],
                [sch_creator_inactive],
                [sch_create_dt],
                [sch_name],
                [sch_type],
                [sch_type_name],
                [sch_interval],
                [sch_day_of_week],
                [sch_day_week_number],
                [sch_start_date],
                [sch_end_date],
                [sch_start_time],
                [sch_end_time],
                [sch_request_id],
                [req_creator],
                [req_creator_name],
                [req_creator_sort_name],
                [req_creator_inactive],
                [que_status],
                [que_status_name],
                [que_computed_date],
                [req_report_id],
                [req_type],
                [req_type_name],
                [req_date_time],
                [req_end_date_time],
                [req_seconds],
                [req_run_time],
                [req_printer_name],
                [req_public_flag],
                [req_result_code],
                [req_result_code_name],
                [req_result_text],
                [que_creator]
        FROM [dbo].[LV_REPORTS_RUN_ALL]
        WHERE [sch_id] > 0 
          AND [que_status] IN ('s','h')
GO

    GRANT SELECT ON [dbo].[LV_REPORTS_RUN_SCHEDULED] TO  [ImpUsers], [tessitura_app]
GO
