--USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[LV_RPT_COLLECTED_PASSES] AS
       SELECT   cus.[customer_no] AS [show_and_go_no],
                cus.[cust_type] AS [customer_type],
                CASE WHEN att.[ticket_msg] = 'OK CityPass' AND LEN(LTRIM(RTRIM(att.[scan_str]))) = 14 THEN 'CityPass Booklet Scans'
                     WHEN att.[ticket_msg] = 'OK CityPass' AND LEN(LTRIM(RTRIM(att.[scan_str]))) <> 14 THEN 'CityPass Mobile Scans'
                     WHEN cus.[lname] like '%Library%' THEN 'Library Pass' 
                     ELSE cus.[lname] END AS [show_and_go_name],
                att.[update_dt] AS [scan_dt],
                prf.[performance_dt] AS [performance_dt],
                prf.[title_name] AS [scan_title],
                prf.[production_name] AS [scan_production_name],
                prf.[production_name_long] AS [scan_production_name_long],
                prf.[production_season_name] AS [scan_production_season_name],
                prf.[season_name] AS [scan_season],
                CONVERT(CHAR(10), att.[update_dt], 111) AS [scan_date],
                CONVERT(CHAR(8), att.[update_dt], 108) AS [scan_time],
                ISNULL(att.[device_name], '') AS [device_name],
                1 AS [scan_admission],
                att.[ticket_msg] AS [ticket_msg],
                att.[scan_str]
       FROM     [dbo].[T_NSCAN_EVENT_CONTROL] AS att (NOLOCK)
                LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = att.[customer_no]
                LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_NO_ZONES] AS prf (NOLOCK) ON prf.[performance_no] = att.[perf_no]
                LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] ctype ON cus.[cust_type] = ctype.[id]
       WHERE    ((att.[ticket_msg] = 'OK CityPass')
             OR (ctype.[description] = 'N-Scan Collected Pass' 
                 AND att.[ticket_msg] IN ('OK', 'OK - Ticket already recorded')))
GO

GRANT SELECT ON [dbo].[LV_RPT_COLLECTED_PASSES] TO [ImpUsers], [tessitura_app]
GO

SELECT * FROM  [dbo].[LV_RPT_COLLECTED_PASSES] WHERE scan_date = '2020/07/27' --AND [show_and_go_name] LIKE 'CityPass%' ORDER BY [show_and_go_name], [scan_date]

--SELECT [season_name] FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE [performance_no] = 82991

