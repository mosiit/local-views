USE [impresario]
GO

/****** Object:  View [dbo].[LV_RPT_LIBRARY_PASSES_ONLY]    Script Date: 7/1/2016 10:06:09 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP VIEW [dbo].[LV_RPT_LIBRARY_PASSES_ONLY]
GO

CREATE VIEW [dbo].[LV_RPT_LIBRARY_PASSES_ONLY]
AS
       SELECT   cus.[customer_no] AS 'show_and_go_no',
                cus.[cust_type] AS 'customer_type',
                cus.[lname] AS 'show_and_go_name',
                att.[update_dt] AS 'scan_dt',
                prf.[performance_dt] AS 'performance_dt',
                prf.[title_name] AS 'scan_title',
                prf.[production_name] AS 'scan_production_name',
                prf.[production_name_long] AS 'scan_production_name_long',
                prf.[production_season_name] AS 'scan_production_season_name',
                prf.[season_name] AS 'scan_season',
                CONVERT(CHAR(10), att.[update_dt], 111) AS 'scan_date',
                CONVERT(CHAR(8), att.[update_dt], 108) AS 'scan_time',
                ISNULL(att.[device_name], '') AS 'device_name',
                1 AS 'scan_admission',
                [ticket_msg] AS 'ticket_msg'
       FROM     [dbo].[T_NSCAN_EVENT_CONTROL] AS att (NOLOCK)
       INNER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = att.[customer_no]
       INNER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.[performance_no] = att.[perf_no]
       INNER JOIN [dbo].[TR_CUST_TYPE] ctype ON cus.[cust_type] = ctype.[id]
       WHERE    ctype.[description] = 'N-Scan Collected Pass'
                AND att.[ticket_msg] IN ('OK', 'OK - Ticket already recorded')
				AND cus.[lname] like '%Library%'

GO