

CREATE VIEW [dbo].[LV_SALES_LAYOUTS] AS
    SELECT lay.[id] AS [layout_id],
           lay.[description] [layout_name],
           lay.[primary_ind] AS [layout_is_primary],
           lay.[rows] AS [layout_rows],
           lay.[columns] AS [layout_columns],
           lay.[inactive] AS [layout_inactive],
           btn.[id] AS [button_id],
           btn.[ypos] AS [button_row],
           btn.[xpos] AS [button_column],
           btn.[caption1] AS [button_caption_1],
           btn.[caption2] AS [button_caption_2],
           btn.[caption3] AS [button_caption_3],
           btn.[caption4] AS [button_caption_4],
           btn.[background_color] AS [button_back_color],
           btn.[foreground_color] AS [button_text_color],
           ISNULL(btn.[itemid],0) AS [button_item_id],
           ISNULL(btn.[itemsubid],0) AS [button_item_subid],
           btn.[zone_mode] AS [button_zone_mode],

           btn.zone_sequence AS [button_zone_sequence],
           ISNULL(btn.[itemparentid],0) AS [season_no],
           ISNULL(sea.[description],'') AS [season_name]
    FROM [dbo].[T_SALES_LAYOUT] AS lay
         LEFT OUTER JOIN [dbo].[T_SALES_LAYOUT_BUTTON] AS btn ON btn.[layout_id] = lay.[id]
         LEFT OUTER JOIN [dbo].[TR_SEASON] AS sea ON sea.[id] = btn.[itemparentid]
GO

GRANT SELECT ON [dbo].[LV_SALES_LAYOUTS] TO ImpUsers
GO


SELECT * FROM dbo.LV_SALES_LAYOUTS


   