USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_SCHOLARSHIP_ALLOCATIONS]'))
    DROP VIEW [dbo].[LV_SCHOLARSHIP_ALLOCATIONS]
GO

CREATE VIEW [dbo].[LV_SCHOLARSHIP_ALLOCATIONS] AS
SELECT alo.[id_key]
     , alo.[customer_no] AS 'scholarship_no'
     , ISNULL(cus.[lname],'') AS 'scolarship_name'
     , alo.[allocation] AS 'allocation_no'
     , ISNULL(CONVERT(VARCHAR(30),cat.[description]),'') AS 'allocation_name'
     , ISNULL(cat.pmt_method,0) AS 'allocation_pmt_method'
     , ISNULL(mth.[description],'') AS 'pmt_method_name'
     , alo.[allocation_amt]
     , ISNULL(alo.[notes],'') AS 'notes'
     , alo.[anonymous]
     , alo.[created_by]
     , alo.[create_dt]
     , alo.[last_updated_by]
     , alo.[last_update_dt]
     , alo.[create_loc]
FROM [dbo].[LT_SCHOLARSHIP_ALLOCATION] AS alo (NOLOCK)
     LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.customer_no = alo.[customer_no]
     LEFT OUTER JOIN [dbo].[LTR_SCHOLARSHIP_ALLOCATION_CATEGORY] AS cat (NOLOCK) ON cat.[id] = alo.[allocation]
     LEFT OUTER JOIN [dbo].[TR_PAYMENT_METHOD] AS mth (NOLOCK) ON mth.[id] = cat.[pmt_method]
GO

GRANT SELECT ON [dbo].[LV_SCHOLARSHIP_ALLOCATIONS] TO ImpUsers
GO

--SELECT * FROM [dbo].[LV_SCHOLARSHIP_ALLOCATIONS] 
