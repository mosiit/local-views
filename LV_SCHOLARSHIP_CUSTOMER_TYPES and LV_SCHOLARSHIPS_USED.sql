USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_SCHOLARSHIP_CUSTOMER_TYPES]'))
    DROP VIEW [dbo].[LV_SCHOLARSHIP_CUSTOMER_TYPES]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_SCHOLARSHIP_CUSTOMER_TYPES] AS
      SELECT             0 AS 'customer_type_no'
                       , '_All Customer Types' AS 'customer_type'
UNION SELECT DISTINCT    [customer_type_no]
                       , [customer_type]
FROM  [dbo].[LV_SCHOLARSHIP_PAYMENTS] (NOLOCK)
GO

GRANT SELECT ON [dbo].[LV_SCHOLARSHIP_CUSTOMER_TYPES] TO ImpUsers
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_SCHOLARSHIPS_USED]'))
    DROP VIEW [dbo].[LV_SCHOLARSHIPS_USED]
GO


CREATE VIEW [dbo].[LV_SCHOLARSHIPS_USED] AS
      SELECT            0 AS 'scholarship_no'
                      , '_All Scholarships' AS 'scholarship_name'
UNION SELECT DISTINCT   [scholarship_no]
                      , [scholarship_name]
FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS]
WHERE [scholarship_name] <> '' and [scholarship_name] NOT LIKE 'Invalid Scholarship%' AND [scholarship_no] > 0
GO

GRANT SELECT ON [dbo].[LV_SCHOLARSHIPS_USED] TO ImpUsers
GO

