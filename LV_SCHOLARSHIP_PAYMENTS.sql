USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_SCHOLARSHIP_PAYMENTS]'))
    DROP VIEW [dbo].[LV_SCHOLARSHIP_PAYMENTS]
GO

CREATE VIEW [dbo].[LV_SCHOLARSHIP_PAYMENTS] AS
SELECT    trx.[sequence_no] AS [transaction_Sequence_no]
        , trx.[transaction_no] AS [transaction_no]
        , trx.[trn_dt] AS [transaction_dt]
        , CONVERT(CHAR(10),trx.[trn_dt],111) AS [transaction_date]
        , trx.[create_dt] AS [transaction_create_dt]
        , CONVERT(CHAR(10),trx.[create_dt],111) AS [transaction_create_date]
        , trx.[created_by] AS [transaction_created_by]
        , trx.[trn_type] AS [transaction_type_no]
        , tty.[description] AS [transaction_type]
        , trx.[trn_amt] AS [transaction_amount]
        , trx.[posted_status]
        , pay.[payment_no]
        , pay.[pmt_dt] AS [payment_dt]
        , CONVERT(CHAR(10),pay.[pmt_dt],111) AS [payment_date]
        , pay.[create_dt] AS [payment_create_dt]
        , CONVERT(CHAR(10),pay.[create_dt],111) AS [payment_create_date]
        , pay.[created_by] AS [payment_created_by]
        , trx.[order_no]
        , trx.[perf_no] AS [performance_no]
        , prf.[perf_dt] AS [performance_dt]
        , CONVERT(CHAR(10),prf.[perf_dt],111) AS [performance_date]
        , trx.[batch_no]
        , trx.[campaign_no]
        , trx.[fee_no]
        , trx.[tckt_tran_flag]
        , pay.[pmt_method] AS [payment_method_no]
        , mth.[description] AS [payment_method]
        , pay.[pmt_amt] AS [payment_amount]
        , CASE WHEN ISNULL(pay.[check_no],'0') = '' THEN '0' ELSE ISNULL(pay.[check_no],'0') END AS [scholarship_no]
        , CASE WHEN ISNULL(pay.[pmt_method],0) = 32 THEN 'Scholarship On Account' 
               WHEN ISNULL(sch.[cust_type],0) NOT IN (16,17) THEN 'Invalid Scholarship ID (' + CONVERT(VARCHAR(25),ISNULL(pay.[check_no],'')) + ')'
               ELSE ISNULL(sch.[lname],'') END AS [scholarship_name]
        , ISNULL(sty.[description],'') AS [scholarship_type]
        , pay.[customer_no]
        , cus.[cust_type] AS [customer_type_no]
        , cty.[description] AS [customer_type]
        , ISNULL(cus.[fname], '') AS [customer_first_name]
        , ISNULL(cus.[mName], '') AS [customer_middle_name]
        , ISNULL(cus.[lname], '') AS [customer_last_name]
        , CASE WHEN ISNULL(cus.[fname], '') = '' THEN '' ELSE cus.[fname] END 
          + CASE WHEN ISNULL(cus.[mname], '') = '' THEN '' ELSE cus.[mname] END 
          + CASE WHEN ISNULL(cus.[lname], '') = '' THEN '' ELSE cus.[lname] END AS [customer_name]
        , ISNULL(adr.[address_type],0) AS [address_type_no]
        , ISNULL(aty.[description],'') AS [address_type]
        , ISNULL(adr.[street1],'') AS [address_street1]
        , ISNULL(adr.[street2],'') AS [address_street2]
        , ISNULL(adr.[city],'') AS [address_city]
        , ISNULL(adr.[state],'') AS [address_state]
        , ISNULL(adr.[postal_code],'') AS [address_zip_code]
        , ISNULL(pay.[notes],'') AS [payment_notes]
FROM [dbo].[T_PAYMENT] AS pay (NOLOCK)
     LEFT OUTER JOIN [dbo].[T_TRANSACTION] AS trx (NOLOCK) ON trx.[sequence_no] = pay.[sequence_no]
     LEFT OUTER JOIN [dbo].[TR_TRANSACTION_TYPE] AS tty (NOLOCK) ON tty.[id] = trx.[trn_type]
     LEFT OUTER JOIN [dbo].[TR_PAYMENT_METHOD] AS mth (NOLOCK) ON mth.[id] = pay.[pmt_method]
     LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS sch (NOLOCK) ON CAST(sch.[customer_no] AS VARCHAR(30)) = pay.[check_no]
     LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] AS sty (NOLOCK) ON sty.[id] = sch.[cust_type]
     LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = pay.[customer_no]
     LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr (NOLOCK) ON adr.[customer_no] = cus.[customer_no] AND adr.[inactive] = 'N' AND adr.[primary_ind] = 'Y'
     LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] AS cty (NOLOCK) ON cty.[id] = cus.[cust_type]
     LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS aty (NOLOCK) ON aty.[id] = adr.[address_type]
     LEFT OUTER JOIN [dbo].[T_PERF] AS prf (NOLOCK) ON prf.[perf_no] = trx.[perf_no]
WHERE (
            mth.[description] like '%Scholarship%'  
        OR (mth.[description] = 'Interdepartmental Transfer' AND trx.[trn_type] IN (32,33))  --32 = Ticket Purchase / 33 = Ticket Refund
       )                                                                                     --7/13/2018: Added Ticket Refunds
GO

GRANT SELECT ON [dbo].[LV_SCHOLARSHIP_PAYMENTS] TO ImpUsers
GO

--SELECT * FROM  [dbo].[LV_SCHOLARSHIP_PAYMENTS] WHERE CONVERT(DATE,payment_dt) = '3-22-2018' AND [payment_method] = 'Interdepartmental Transfer'

