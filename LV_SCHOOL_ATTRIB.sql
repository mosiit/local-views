USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[LV_SCHOOL_ATTRIB]
AS

SELECT t483.keyword_no AS keyword_483, t404.keyword_no AS keyword_404, t483.customer_no, t483.key_value AS key_value_483, t404.key_value AS key_value_404
FROM dbo.TX_CUST_KEYWORD t483
	LEFT JOIN dbo.TX_CUST_KEYWORD t404
	ON t404.customer_no = t483.customer_no
		AND t404.keyword_no = 404
WHERE t483.keyword_no = 483 

UNION 

SELECT t483.keyword_no AS keyword_483, t404.keyword_no AS keyword_404, t404.customer_no, t483.key_value AS key_value_483, t404.key_value AS key_value_404
FROM dbo.TX_CUST_KEYWORD t404
	LEFT JOIN dbo.TX_CUST_KEYWORD t483
	ON t404.customer_no = t483.customer_no
		AND t483.keyword_no = 483
WHERE t404.keyword_no = 404 

GO

GRANT SELECT ON  [dbo].[LV_SCHOOL_ATTRIB] to impusers
GO 
