USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_SCHOOL_EXHIBIT_ORDER_PRICING]'))
    DROP VIEW [dbo].[LV_SCHOOL_EXHIBIT_ORDER_PRICING]
GO

CREATE VIEW [dbo].[LV_SCHOOL_EXHIBIT_ORDER_PRICING] AS
    WITH perf_cache (perf_dt, perf_no)
    AS (SELECT [performance_dt], [performance_no] FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE (NOLOCK)
        WHERE  [title_name] = 'Exhibit Halls' AND [production_name] = 'Exhibit Halls School')
    SELECT ord.[order_no], 
           ord.[customer_no],
           prf.[title_name],
           prf.[production_name] + CASE WHEN prf.[production_name] LIKE '%school%' AND prf.[performance_time] = '13:00' THEN ' PM'
                                        WHEN prf.[production_name] LIKE '%school%' AND prf.[performance_time] <> '13:00' THEN ' AM'
                                        ELSE '' END AS 'production_name', 
           prf.[performance_dt],
           prf.[performance_date],
           prf.[performance_time],
           sli.price_type,
           typ.[description] AS 'price_type_name',
           sli.[paid_amt],
           ISNULL(sli.[rule_ind], '') AS 'rule_ind',
           ISNULL(sli.[rule_id], 0) AS 'rule_id',
           ISNULL(rul.[description], '') AS 'rule_name',
           count(*) as 'ticket_count',
           sum (sli.[paid_amt]) AS 'paid_amount'
    FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
         INNER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
         LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) 
                               ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
         LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] AS typ (NOLOCK) ON typ.[id] = sli.[price_type]
         LEFT OUTER JOIN [dbo].[T_PRICING_RULE] AS rul (NOLOCK) ON rul.[id] = sli.[rule_id]
    WHERE sli.[perf_no] IN (SELECT [perf_no] FROM [perf_cache])
      AND sli.[sli_status] IN (3, 12)
    GROUP BY ord.[order_no], ord.[customer_no], prf.[title_name], prf.[production_name], prf.[performance_dt], prf.[performance_date], 
             prf.[performance_time], sli.price_type, typ.[description], sli.[paid_amt], sli.[rule_ind], sli.[rule_id], rul.[description]
GO

GRANT SELECT ON [dbo].[LV_SCHOOL_EXHIBIT_ORDER_PRICING] TO ImpUsers
GO

