
DROP VIEW [dbo].[LV_SEAT_HISTORY]
GO

CREATE VIEW [dbo].[LV_SEAT_HISTORY] AS
    SELECT his.[perf_no], 
           prf.[title_name],
           prf.[production_name],
           his.[seat_no],
           his.[event_date],
           his.[event_code],
           cod.[description] AS [event_name],
           his.[user_id],
           his.[user_location],           
           ISNULL(his.[order_no],0) AS [order_no],
           ISNULL(his.[customer_no],0) AS [customer_no],
           ISNULL(his.[hc_no],0) AS [hold_code_no],
           ISNULL(hlc.[description],'') AS [hold_code],
           ISNULL(his.[sli_paid_amt],0.0) AS [paid_amount]
    FROM [dbo].[T_ORDER_SEAT_HIST] AS his 
         LEFT OUTER JOIN [dbo].[TR_EVENT_CODE] AS cod ON cod.[id] = his.[event_code]
         LEFT OUTER JOIN [T_HC] AS hlc ON hlc.[hc_no] = his.[hc_no]
         LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_NO_ZONES] AS prf ON prf.[performance_no] = his.[perf_no]
GO

GRANT SELECT ON [dbo].[LV_SEAT_HISTORY] TO [ImpUsers], [tessitura_app]
GO

