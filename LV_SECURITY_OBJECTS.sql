USE impresario
GO

    IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_SECURITY_OBJECTS]'))
        DROP VIEW [dbo].[LV_SECURITY_OBJECTS]
    GO


    CREATE VIEW [dbo].[LV_SECURITY_OBJECTS] AS
        SELECT obj.[name] AS 'object_name'
              ,obj.[object_type]
              ,CASE WHEN ISNULL(obj.[const_based],'N') = 'Y' THEN 'Yes' ELSE 'No' END AS 'constituent_based'
              ,obj.[description] AS 'object_description'
        FROM  [dbo].[T_APP_OBJECTS] as obj (NOLOCK)
    GO
    
    GRANT SELECT ON [dbo].[LV_SECURITY_OBJECTS] TO ImpUsers
    GO


    
