

    CREATE VIEW [dbo].[LV_SECURITY_RIGHTS] AS
        SELECT ugr.[UG_id],
               ugr.[UG_name],
               ISNULL(obj.[object_id],-1) AS [object_id],
               ISNULL(obj.[name],'') AS [object_name],
               ISNULL(obj.[object_type],'') AS [object_type],
               ISNULL(con.[description], '') as [constituency],
               ISNULL(usr.[adding],'') AS [adding],
               ISNULL(usr.[deleting],'') AS [deleting],
               ISNULL(usr.[editing],'') AS [editing],
               ISNULL(usr.[viewing],'') AS [viewing],
               ISNULL(usr.[adding],'') + ISNULL(usr.[deleting],'')
                                       + ISNULL(usr.[editing],'') 
                                       + ISNULL(usr.[viewing],'') AS [permission_str],
               ISNULL(obj.[const_based],'') AS [const_based],
               ISNULL(obj.[description],'No Security Rights Found') AS [description]
        FROM [dbo].[T_METUSERGROUP] as ugr (NOLOCK)
             LEFT OUTER JOIN [dbo].[TX_SECURITY_RIGHTS] as usr (NOLOCK) ON usr.[UG_id] = ugr.[UG_id]
             LEFT OUTER JOIN [dbo].[T_APP_OBJECTS] as obj (NOLOCK) ON obj.[object_id] = usr.[object_id]
             LEFT OUTER JOIN [dbo].[TR_CONSTITUENCY] as con (NOLOCK) ON con.[id] = usr.[constituency]
GO


    GRANT SELECT ON [dbo].[LV_SECURITY_RIGHTS] TO [ImpUsers], [tessitura_app]
GO

    