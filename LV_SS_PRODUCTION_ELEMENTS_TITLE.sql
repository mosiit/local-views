USE [impresario]
GO

/****** Object:  View [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE]    Script Date: 3/19/2021 10:50:07 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] AS
SELECT ttl.[inv_no] AS 'title_no'
      ,ttl.[description] AS 'title_name'
      ,ttl.[type] AS 'title_inventory_type'
      ,ttl.[short_name] AS 'title_short_name'
      ,ISNULL(ssa.[Value], 'N') AS 'superscheduler_access'
      ,ISNULL(tgt.[value], 'Unknown') AS 'generic_production_name'
      ,ISNULL(fac.[value], '') AS 'default_facility'
      ,ISNULL(pcd.[value], '') AS 'perf_code_prefix'
      ,ISNULL(qsv.[value], '') AS 'quick_sale_venue'
      ,CASE WHEN qsc.[value] IS NULL THEN 0 WHEN ISNUMERIC(qsc.[value]) = 0 THEN 0 ELSE CONVERT(INT,qsc.[value]) END AS 'quick_sale_button_color'
      ,ISNULL(loc.[value], '') AS 'title_location'
      ,CONVERT(CHAR(1),ISNULL(vis.[value], 'Y')) AS 'visit_count_title'
      ,CONVERT(CHAR(1),ISNULL(cap.[value], 'Y')) AS 'capacity_reporting_title'
FROM [dbo].[T_INVENTORY] AS ttl
     LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS ssa ON ssa.[inv_no] = ttl.[inv_no] AND ssa.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Superscheduler Access')
     LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS tgt ON tgt.[inv_no] = ttl.[inv_no] AND tgt.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Generic Production')
     LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS pcd ON pcd.[inv_no] = ttl.[inv_no] AND pcd.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Perf Code Prefix')
     LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS qsv ON qsv.[inv_no] = ttl.[inv_no] AND qsv.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Quick Sale Venue Name')
     LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS qsc ON qsc.[inv_no] = ttl.[inv_no] AND qsc.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Quick Sale Button Color')
     LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS fac ON fac.[inv_no] = ttl.[inv_no] AND fac.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Default Facility')
     LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS loc ON loc.[inv_no] = ttl.[inv_no] AND loc.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Location')
     LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS vis ON vis.[inv_no] = ttl.[inv_no] AND vis.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Visit Count')
     LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS cap ON cap.[inv_no] = ttl.[inv_no] AND cap.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Capacity Reporting')
WHERE ttl.[type] = 'T'
GO


