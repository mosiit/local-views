SET QUOTED_IDENTIFIER ON;
SET ANSI_NULLS ON;
GO

ALTER VIEW [dbo].[LV_SUM_BY_PERF]
AS
SELECT DISTINCT
    a.order_no,
    a.perf_no,
    a.perf_time,
    perf_type = CASE
                    WHEN c.facility_no = 16 THEN 'EX'
                    WHEN c.facility_no = 12 THEN 'PL'
                    WHEN c.facility_no = 14 THEN 'OM'
                    WHEN c.facility_no = 17 THEN '4D'
                    WHEN c.facility_no = 19 THEN 'BG'
					WHEN c.facility_no = 36 THEN 'NI'
                END,
    SUM(a.count_tix) OVER (PARTITION BY a.perf_no, a.order_no, a.perf_time) AS count_by_perf
FROM lv_order_li_pt_count a
JOIN T_PERF c
    ON a.perf_no = c.perf_no
WHERE c.facility_no IN (12, 14, 16, 17, 19, 36);

GO


