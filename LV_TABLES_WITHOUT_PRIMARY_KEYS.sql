USE [impresario]
GO

/****** Object:  View [dbo].[LV_TABLES_WITHOUT_PRIMARY_KEYS]    Script Date: 10/27/2016 10:45:31 AM ******/
DROP VIEW [dbo].[LV_TABLES_WITHOUT_PRIMARY_KEYS]
GO

/****** Object:  View [dbo].[LV_TABLES_WITHOUT_PRIMARY_KEYS]    Script Date: 10/27/2016 10:45:31 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_TABLES_WITHOUT_PRIMARY_KEYS]
AS
       SELECT   s.name AS SchemaName,
                t.name AS TableName,
				t.object_id ObjectID
       FROM     sys.objects t
       INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
       WHERE    t.type = 'U'
                AND NOT EXISTS ( SELECT 1
                                 FROM   sys.objects pk
                                 WHERE  pk.type = 'PK'
                                        AND t.object_id = pk.parent_object_id )

GO


