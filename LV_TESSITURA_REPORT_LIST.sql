USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_TESSITURA_REPORT_LIST]'))
    DROP VIEW [dbo].[LV_TESSITURA_REPORT_LIST]
GO

CREATE VIEW [dbo].[LV_TESSITURA_REPORT_LIST] AS
SELECT rpt.[category],
       cat.[description] AS [category_name],
       cat.[description_text] AS [category_description],
       rpt.[id] AS [report_id],
       rpt.[name] AS [report_name],
       rpt.[window] AS [report_window_type],
       rpt.[application_id],
       rpt.[allow_query],
       rpt.[allow_schedule],
       ISNULL(rpt.[imp_win_ind],'N') AS [imp_win_ind],
       ISNULL(rpt.[imp_win_name],'') AS [imp_win_name],
       rpt.[public_flag],
       ISNULL(rpt.[psr_file],'') AS [psr_file],
       rpt.[report_type],
       typ.[Description] AS [report_type_name],
       rpt.[warning_ind],
       rpt.[utility_ind],
       ISNULL(CAST(rpt.[created_by] AS VARCHAR(30)),'(unknown)') AS [report_created_by],
       ISNULL(rpt.[create_dt],'1900-01-01 00:00:00') AS [report_create_dt],
       ISNULL(rpt.[create_loc],'(unknown)') AS [report_create_loc],
       ISNULL(CAST(rpt.[last_updated_by] AS VARCHAR(30)),'(unknown)') AS [report_last_updated_by],
       ISNULL(rpt.[last_update_dt],'1900-01-01 00:00:00') AS [report_last_update_dt],
       ISNULL(rpt.[querystring_append],'') AS [querystring_append],
       rpt.[inactive] as [report_inactive],
       REPLACE(REPLACE(ISNULL(rpt.[description],''),char(13),''),char(10),' ** ') AS [report_description]
FROM [dbo].[gooesoft_report] AS rpt
     INNER JOIN [dbo].[gooesoft_report_category] AS cat ON cat.[id] = rpt.[category]
     INNER JOIN [dbo].[TR_REPORT_TYPE] AS typ ON typ.[id] = rpt.[report_type]
GO

GRANT SELECT ON [dbo].[LV_TESSITURA_REPORT_LIST] TO [ImpUsers], [tessitura_app]
GO

--SELECT * FROM [dbo].[LV_TESSITURA_REPORT_LIST]
