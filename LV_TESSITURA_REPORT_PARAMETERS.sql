USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_TESSITURA_REPORT_PARAMETERS]'))
    DROP VIEW [dbo].[LV_TESSITURA_REPORT_PARAMETERS]
GO

CREATE VIEW [dbo].[LV_TESSITURA_REPORT_PARAMETERS] AS
SELECT rpt.[category],
       cat.[description] AS [category_name],
       rpt.[id] AS [report_id],
       rpt.[name] AS [report_name],
       ISNULL(rpt.[psr_file],'') AS [psr_file],
       rpt.[report_type],
       typ.[Description] AS [report_type_name],
       rpt.[utility_ind],
       rpt.[inactive] AS [report_inactive],
       prm.[id] AS [parameter_id],
       prm.[sequence_number] AS [parameter_sequence],
       prm.[description] AS [parameter_name],
       prm.[data_type],
       CASE prm.[data_type]
            WHEN 50 THEN 'Money'
            WHEN 40 THEN 'DateTime'
            WHEN 30 THEN 'Time'
            WHEN 20 THEN 'Date'
            WHEN 10 THEN 'Number'
            ELSE 'String' END AS [data_type_name],
        ISNULL(prm.[length],0) AS [length],
        ISNULL(prm.[editstyle],'') AS [editstyle],
        ISNULL(prm.[editstyle_flag],'N') AS [editstyle_flag],
        ISNULL(prm.[end_of_day],'N') AS [end_of_day],
        ISNULL(prm.[validation],'') AS [validation],
        ISNULL(prm.[required],'N') AS [required],
        ISNULL(prm.[multi_select],'N') AS [multi_select],
        ISNULL(prm.[d_object],'') AS [d_object],
        ISNULL(prm.[table_name],'') AS [table_name],
        ISNULL(prm.[display_col],'') AS [display_col],
        ISNULL(prm.[data_col],'') AS [data_col],
        ISNULL(prm.[where_clause],'') AS [where_clause],
        ISNULL(prm.[order_clause],'') AS [order_clause],
        CASE WHEN ISNULL(prm.[table_name],'') <> '' AND ISNULL([prm].[data_col],'') <> '' AND ISNULL([prm].[display_col],'') <> ''
             THEN 'SELECT ' + prm.[data_col] + ', ' + prm.[display_col] + ' FROM ' + prm.[table_name]
                + CASE WHEN ISNULL(prm.[where_clause],'') = '' THEN '' ELSE ' WHERE ' + prm.[where_clause] END
                + CASE WHEN ISNULL(prm.[order_clause],'') = '' THEN '' ELSE ' ORDER BY ' + prm.[order_clause] END
             ELSE '' END AS [parameter_query],
        ISNULL(prm.[disable_clause],'') AS [disable_clause],
        ISNULL(prm.[default_val],'') AS [default_value],
        ISNULL(prm.[param_name],'') AS [SSRS_name],
        ISNULL(CAST(prm.[created_by] AS VARCHAR(30)),'(unknown)') AS [parameter_created_by],
        ISNULL(prm.[create_dt],'1900-01-01 00:00:00') AS [parameter_create_dt],
        ISNULL(prm.[create_loc],'(unknown)') AS [parameter_create_loc],
        ISNULL(CAST(prm.[last_updated_by] AS VARCHAR(30)),'(unknown)') AS [parameter_last_updated_by],
        ISNULL(prm.[last_update_dt],'1900-01-01 00:00:00') AS [paramter_last_update_dt]
FROM [dbo].[gooesoft_report] AS rpt
     INNER JOIN [dbo].[gooesoft_report_category] AS cat ON cat.[id] = rpt.[category]
     INNER JOIN [dbo].[TR_REPORT_TYPE] AS typ ON typ.[id] = rpt.[report_type]
     LEFT OUTER JOIN [dbo].[gooesoft_report_parameter] AS prm ON prm.[report_id] = rpt.[id]
GO

GRANT SELECT ON [dbo].[LV_TESSITURA_REPORT_PARAMETERS] TO [ImpUsers], [tessitura_app]
GO

--SELECT * FROM [dbo].[LV_TESSITURA_REPORT_PARAMETERS]
