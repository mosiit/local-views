USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_TESSITURA_REPORT_SCHEDULE]'))
    DROP VIEW [dbo].[LV_TESSITURA_REPORT_SCHEDULE]
GO

CREATE VIEW [dbo].[LV_TESSITURA_REPORT_SCHEDULE] AS
SELECT rpt.[category],
       cat.[description] AS [category_name],
       rpt.[id] AS [report_id],
       rpt.[name] AS [report_name],
       ISNULL(rpt.[psr_file],'') AS [psr_file],
       rpt.[report_type],
       typ.[Description] AS [report_type_name],
       rpt.[utility_ind],
       rpt.[inactive] AS [report_inactive],
       ISNULL(req.[type],'') AS [request_type],
       CASE ISNULL(req.[type],'')
            WHEN 'f' THEN 'Foreground'
            WHEN 'b' THEN 'Background'
            WHEN 'h' THEN 'Metadata Row'
            WHEN 's' THEN 'Scheduled Report'
            ELSE ISNULL(req.type,'') END AS [request_type_name],
       req.[id] AS [request_id],
       req.[request_date_time] AS [request_date_time],
       CONVERT(CHAR(10),req.[request_date_time],111) AS [request_date],
       CONVERT(CHAR(8),req.[request_date_time],108) AS [request_time],
       req.[end_date_time] AS [request_end_date_time],
       CONVERT(CHAR(10),req.[end_date_time],111) AS [request_end_date],
       CONVERT(CHAR(8),req.[end_date_time],108) AS [request_end_time],
       ISNULL(DATEDIFF(SECOND,req.[request_date_time],req.[end_date_time]),0) AS [request_seconds],
       [dbo].[LFS_RUN_TIME_FROM_SECONDS] (DATEDIFF(SECOND,req.[request_date_time],req.[end_date_time])) AS [request_run_time],
       ISNULL(req.[printer_name],'') AS [request_printer_name],
       ISNULL(req.[public_flag],'') AS [request_public_flag],
       ISNULL(req.[result_code],'') AS [request_result_code],
       CASE ISNULL(req.result_code,'')
            WHEN 'c' THEN 'Completed'
            WHEN 's' THEN 'Save/Print Error'
            WHEN 'd' THEN 'Database Error'
            WHEN 't' THEN 'User Terminated'
            WHEN 'r' THEN 'Data Window Error'
            WHEN '' THEN 'Not Run Yet'
            ELSE ISNULL(req.[result_code],'') END AS [request_result_code_name],
       ISNULL(req.[result_text],'') AS [request_result_text],
       ISNULL(sch.[id],0) AS [schedule_id],
       ISNULL(sch.created_by,'') AS [schedule_creator],
       ISNULL(uss.[full_name],'') AS [schedule_creator_name],
       ISNULL(uss.[sort_name],'') AS [schedule_creator_sort_name],
       ISNULL(uss.[inactive],'Y') AS [schedule_creator_inactive],
       sch.create_dt AS [schedule_create_dt],
       ISNULL(req.[queue_status],'') AS [queue_status],
       CASE ISNULL(req.[queue_status],'')
            WHEN 'e' THEN 'Error'
            WHEN 'c' THEN 'Completed'
            WHEN 'h' THEN 'On Hold'
            WHEN 's' THEN 'Scheduled'
            ELSE '(unknown)' END AS [request_queue_status],
       ISNULL(sch.[name],'') AS [schedule_name],
       ISNULL(sch.[type],'') AS [schedule_type],
       CASE ISNULL(sch.[type],'')
            WHEN 'd' THEN 'Daily'
            WHEN 'h' THEN 'Hourly'
            WHEN 'm' THEN 'Monthly/Weekday'
            WHEN 'n' THEN 'Monthly/Date'
            WHEN 'o' THEN 'One Time'
            WHEN 'w' THEN 'Weekly'
            ELSE ISNULL(sch.[type],'') END AS [schedule_type_name],
       ISNULL(sch.[interval],0) AS [schedule_interval],
       ISNULL(sch.[day_of_week],0) AS [schedule_day_of_week],
       ISNULL(sch.[day_week_number],0) AS [schedule_day_week_number],
       ISNULL(CONVERT(CHAR(10),sch.[start_date],111),'') AS [schedule_start_date],
       ISNULL(CONVERT(CHAR(10),sch.[end_date],111),'') AS [schedule_end_date],
       ISNULL(CONVERT(CHAR(8),sch.[start_time],108),'') AS [schedule_start_time],
       CASE WHEN sch.[end_time] IS NULL THEN ''
            WHEN sch.[end_time] = '1900-01-01 00:00:00.000' THEN ''
            ELSE ISNULL(CONVERT(CHAR(8),sch.[end_time],108),'') END AS [schedule_end_time],
      sch.[deactivate_flag]
FROM [dbo].[gooesoft_report] AS rpt
     INNER JOIN [dbo].[gooesoft_report_category] AS cat ON cat.[id] = rpt.[category]
     INNER JOIN [dbo].[TR_REPORT_TYPE] AS typ ON typ.[id] = rpt.[report_type]
     INNER JOIN [dbo].[gooesoft_request] AS req ON req.[report_id] = rpt.[id]
     LEFT OUTER JOIN [dbo].[gooesoft_report_schedule] AS sch ON sch.[id] = req.[schedule_id]
     LEFT OUTER JOIN [dbo].[LV_MuseumUserInfo] AS uss ON uss.[userid] = sch.[created_by]
WHERE ISNULL(req.[type],'') = 's'
GO

GRANT SELECT ON [dbo].[LV_TESSITURA_REPORT_SCHEDULE] TO [ImpUsers], [tessitura_app]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_TESSITURA_REPORT_SCHEDULE_FUTURE]'))
    DROP VIEW [dbo].[LV_TESSITURA_REPORT_SCHEDULE_FUTURE]
GO

CREATE VIEW [dbo].[LV_TESSITURA_REPORT_SCHEDULE_FUTURE]
AS SELECT * FROM [dbo].[LV_TESSITURA_REPORT_SCHEDULE] WHERE [queue_status] IN ('h','s')

GO

GRANT SELECT ON [dbo].[LV_TESSITURA_REPORT_SCHEDULE_FUTURE] TO [ImpUsers], [tessitura_app]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_TESSITURA_REPORT_SCHEDULE_COMPLETED]'))
    DROP VIEW [dbo].[LV_TESSITURA_REPORT_SCHEDULE_COMPLETED]
GO

CREATE VIEW [dbo].[LV_TESSITURA_REPORT_SCHEDULE_COMPLETED]
AS SELECT * FROM [dbo].[LV_TESSITURA_REPORT_SCHEDULE] WHERE [queue_status] NOT IN ('','h','s')
GO

GRANT SELECT ON [dbo].[LV_TESSITURA_REPORT_SCHEDULE_COMPLETED] TO [ImpUsers], [tessitura_app]
GO




--SELECT * FROM [dbo].[LV_TESSITURA_REPORT_SCHEDULE] WHERE queue_status IN ('h','s')--


