USE impresario
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_US_CANADA_STATE_LIST_WITH_ALL]'))
    DROP VIEW [dbo].[LV_US_CANADA_STATE_LIST_WITH_ALL]
GO

CREATE VIEW [dbo].[LV_US_CANADA_STATE_LIST_WITH_ALL] AS
SELECT   'All' AS 'id'
       , '_All States' AS 'description'
       , '_All States' AS 'extended_description'
       , 1 AS 'country'
       , 'USA' AS 'country_name'
       , 'N' AS [inactive]
       , NULL AS 'create_loc'
       , 'dbo' AS 'created_by'
       , GETDATE() AS 'create_dt'
       , 'dbo' AS 'last_updated_by'
       , GETDATE() AS 'last_update_dt'
       , 0 AS 'state_no'
UNION SELECT   [st].[id]
       , [st].[description]
       , [st].[description] + CASE WHEN st.[country] = 1 THEN '' ELSE ', ' + co.[description] END AS 'extended_description'
       , [st].[country]
       , [co].[description] AS 'country_name'
       , [st].[inactive]
       , [st].[create_loc]
       , [st].[created_by]
       , [st].[create_dt]
       , [st].[last_updated_by]
       , [st].[last_update_dt]
       , [st].[state_no]
FROM [dbo].[TR_STATE] AS st (NOLOCK)
     INNER JOIN [dbo].[TR_COUNTRY] AS co (NOLOCK) ON co.[id] = st.[country]
WHERE [st].[country] IN (1,32) AND st.[id] <> 'XX'
GO

GRANT SELECT ON [dbo].[LV_US_CANADA_STATE_LIST_WITH_ALL] TO ImpUsers
GO

--SELECT [description], [extended_description], [id] FROM [dbo].[LV_US_CANADA_STATE_LIST_WITH_ALL] ORDER BY [country], [description]

