USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_UserGroupUserList]'))
    DROP VIEW [dbo].[LV_UserGroupUserList]
GO

CREATE VIEW [dbo].[LV_UserGroupUserList] AS
SELECT    txg.[user_group_no]
        , txg.[userid]
        , usr.[fname]
        , usr.[lname]
        , usr.[full_name]
        , usr.[sort_name]
        , usr.[location]
        , usr.[email_address]
        , usr.[last_login_dt]
        , usr.[days_since_last_login]
        , usr.[locked_ind]
        , usr.[num_logins_allowed]
        , usr.[inactive]
        , txg.[UG_id]
        , txg.[default_ind]
        , grp.[UG_name]
        , grp.[description]
        , grp.[post_ldg]
        , grp.[admin_group]
        , grp.[organization]
        , org.[description] AS 'organization_name'
        , grp.[allow_app]
        , grp.[allow_tablet]
        , grp.[allow_onthego]
FROM [dbo].[TX_USER_GROUP] AS txg (NOLOCK)
     LEFT OUTER JOIN [dbo].[LV_MuseumUserInfo] AS usr (NOLOCK) ON usr.[userid] = txg.[userid]
     LEFT OUTER JOIN [dbo].[T_METUSERGROUP] AS grp (NOLOCK) ON grp.[UG_id] = txg.[UG_id]
     LEFT OUTER JOIN [dbo].[TR_ORGANIZATION] AS org (NOLOCK) ON org.[id] = grp.[organization]
GO

GRANT SELECT ON [dbo].[LV_UserGroupUserList] TO ImpUsers
GO




--SELECT * FROM [LV_UserGroupUserList] WHERE UG_id = 'admin'

