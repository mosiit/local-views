USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_WHO_WORKED_TODAY]'))
    DROP VIEW [dbo].[LV_WHO_WORKED_TODAY]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_WHO_WORKED_TODAY] AS
    SELECT DISTINCT  CONVERT(DATE,sli.[create_dt]) AS 'work_date'
                   , sli.[created_by] AS 'user_id'
                   , mus.[userid] 
                   , (mus.[fname] + ' ' + mus.[lname]) AS 'user_name'
                   , (mus.[lname] + ', ' + mus.[fname]) AS 'user_name_sort'
                   , [location] AS 'user_location' 
    FROM [dbo].[T_SUB_LINEITEM] AS sli
         LEFT OUTER JOIN [dbo].[T_METUSER] AS mus (NOLOCK) ON mus.[userid] = sli.[created_by]
GO

GRANT SELECT ON [dbo].[LV_WHO_WORKED_TODAY] TO ImpUsers
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_WHO_WORKED_TODAY_WITH_ALL]'))
    DROP VIEW [dbo].[LV_WHO_WORKED_TODAY_WITH_ALL]
GO

CREATE VIEW [dbo].[LV_WHO_WORKED_TODAY_WITH_ALL] AS
          SELECT DISTINCT  CONVERT(DATE,sli.[create_dt]) AS 'work_date'
                         , 'All' AS 'user_id'
                         , 'All Users' AS 'user_name'
                         , '_All Users' AS 'user_name_sort'
                         , 'All Locations' AS 'user_location' 
          FROM [dbo].[T_SUB_LINEITEM] AS sli
    UNION SELECT DISTINCT  CONVERT(DATE,sli.[create_dt]) AS 'work_date'
                   , sli.[created_by] AS 'user_id'
                   , (mus.[fname] + ' ' + mus.[lname]) AS 'user_name'
                   , (mus.[lname] + ', ' + mus.[fname]) AS 'user_name_sort'
                   , [location] AS 'user_location' 
    FROM [dbo].[T_SUB_LINEITEM] AS sli
         LEFT OUTER JOIN [dbo].[T_METUSER] AS mus (NOLOCK) ON mus.[userid] = sli.[created_by]
GO

GRANT SELECT ON [dbo].[LV_WHO_WORKED_TODAY_WITH_ALL] TO ImpUsers
GO


  
  
  --SELECT * FROM [dbo].[LV_WHO_WORKED_TODAY_WITH_ALL] WHERE work_date = '11-14-2016' AND [user_location] IN ('Box Office', 'All Locations') ORDER BY user_name_sort
  
 
--WHERE [userid] in (SELECT [created_by] FROM  WHERE convert(char(10),[create_dt],111) = convert(char(10),@report_dt,111))
--ORDER BY [lname], [fname]